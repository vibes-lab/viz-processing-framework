# Basic Commands
Commands used when working from the command line in Linux or Windows Git Bash

## Windows Bash/ Linux 
- Workshop Info: http://swcarpentry.github.io/shell-novice/
- **Navigating**
    - Commands
        - `ls` - list directory
            - `ls -laS` sort by size
            - `ls -l` displays details about files (size, owner, read/write priviledges)
        - `pwd` - print working directory
        - `cd` - change directory
            - `cd ~` change directory to user home directory
            - `cd .` will get to current directory
            - `cd ..` goes up one level
        - `--help` gives list and descriptions of built-in commands
            - `help commandname` gives information on command and flags that can be used (for items in --help list)
            - `man commandname` gives information and flags for commands not included in --help list
        - `clear` - clear screen
        - `ctrl:C` - break (end operation if taking too long)
        - `wc` - gives word count
    - General notes
        - Absolute paths start with /
        - Use up arrow to cycle through previous commands
        - Use tab to autofill to complete command
        - Use forward slashes
        - Anything with spaces must be in quotes
        - https://swcarpentry.github.io/shell-novice/02-filedir/index.html
- **Creating**
    - `mkdir directoryname` (make a directory)
    - avoid spaces in directories or put in quotes, use dashes/ underscores
    - nano is simple text editor
    - use extensions to easily set correct type
- **Editing**
    - Rename 
        - `mv originalname.txt newname.txt`
    - Move
        - `mv name.txt .` (moves to current directory)
        - use `mv name.txt ..` as appropriate to move to directory one level up
    - Copy
        - `cp originalfilename.txt copyfilename.txt`
        - -r can be powerful and confusing. The recursive nature is helpful to make sure things do not get deleted or not moved, but it can duplicate easily.
    - Delete (forever)
        - `rm filename`
        - `rm -i filename` (prompts before removal)
        - to delete directory use `rm -r -i dirname`  (and -i to prevent deleting extra stuff)
        - use `rm -r dirname` if you are sure you want to delete everything in directory
    - Wildcard
        - `*` selects everything that starts with the entered name 
        - Can use wildcard to run command for everything in directory as well
- **Pipes/ filters**
    - Use `|` to separate commands on same line
    - Can use `head` and `tail` to select first or last items
- **Batches**
    - Loops (example)  
                `for filename in *[AB].txt `  
                    `do`  
                    `head -n 2 $filename | tail -n 1`  
- **Text Editors**
    - nano
        - Editing
            - Saving: `Ctrl`+`o`
            - Exiting: `Ctrl`+`x`
            - Cancel (when prompted for filename): `Ctrl`+`C`
        - Formatting
            - ` `` `  will box commands
            - `#` big bold text
            - `-` bulleted list
    - vim
        - `i` go to insert mode
        - `Esc` go to command/normal mode
        - `:wq` save and quit
        - `:q!` quit without saving
        - `:w` save




