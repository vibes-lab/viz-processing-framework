# Git (File Management)
## Git (VT SWC Session 8-17-20)
Additional resources
- https://github.com/chendaniely/2020-08-17-git-dan
- Dan's reference guide for git (but uses branches): https://chendaniely.github.io/training_ds_r/help-faq.html
- swc git lesson: https://swcarpentry.github.io/git-novice/
- If you want to know more about branches and forks: https://www.youtube.com/watch?v=uvWhSYBkZJ0
- Setting up ssh links: https://bi-sdal.github.io/training/ssh-keys.html  

General information
- https://swcarpentry.github.io/git-novice/
- About
    - Type of file organization system that gives most recent file while maintaining old files
    - Version control
- Commits
    - saves a snapshot of folder
    - intermediate staging area between commits
- Use `Ctrl+C` if something is running and its frozen
- Local
    - `git init`: create git repository in current folder
    - `git config --global user.name "Tasha Vipond"`
    - `git config --global user.email "vipondn@vt.edu"`
    - Email and username MUST match the credentials from the remote site you are using (GitLab for instance)
    - `git status`: tells you what is going on in your repository
    - `git add <FILE>` : places into the staging area -git add README.md
    - `git commit`: commits files in the staging area
        -  Must add message about what was changed when committing
    - `git config --global core.editor "nano -w"`: assign nano as default editor
    - `git commit -m "MESSAGE"`: allows you to type a one line commit message in 1 step
    - `git log`: shows you your history
    - `git log --oneline`: shows you your 1-line version of history
    - `HEAD`: tells you where you are looking at in history
    - `git diff`: shows you current state with last known state differences
        - `git diff --staged`: shows difference from staging area with last known
        - you can use `git log --oneline` to specify different versions in history
- Remotes
    - `git remote add origin <URL>` : adds with the name origin
    - `git push origin master`: pushes the master branch to the origin remote
    - `git pull origin master`: pulls the master branch from origin to local computer
    - You make changes to different parts of a file and it will be combined automatically
    - Look for the `>>> === <<<` and fix those lines to what you actually want -delete other lines and create new section instead
    - `git clone <URL>`: to download a git repo on to your local computer
    - `git config --get remote.origin.url` : Check remote url
    - `git remote set-url origin new-repo-ssh-or-https.git` : Set new remote
    - Example: `git remote set-url origin git@code.vt.edu:vipondn/physical-visualization-server.git`
Branches
- `master` is default branch and moves forward with every commit
- Other branches can be made and you can switch between branches if you're not yet ready to apply changes to master
- `git branch testing` : Create a new branch
- `git checkout testing` : Switch to new branch
- `git checkout master` : Switch back to master
- `git checkout -b newbranchname` : Create branch and switch at same time
- Note if you commit on two seperate branches, the history will diverge and will need to be merged
- `git checkout -` : Return to your previously checked out branch
- `git merge branch-name` merges branch with master

