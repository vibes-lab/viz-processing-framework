# Javascript Basics

The following information is a summarized form of the tutorial offered here: https://javascript.info/first-steps 

# Contents 

[External Scripts](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#external-scripts)   
[Syntax](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#syntax)   
[Comments](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#comments)   
[Variables](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#variables)   
[Data Types](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#data-types)   
[Strings](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#strings)   
[Interactions](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#interactions)   
[Conversions](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#conversions)   
[Basic Operators](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#basic-operators)   
[Comparisons](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#comparisons)   
[Conditionals](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#conditional-branching)     
[Logical Operators](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#logical-operators)   
[Loops](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#loops)    
[Functions](https://code.vt.edu/vipondn/goodwin-data-visualization/-/blob/master/basic-instructions/javascript_basics.md#functions) 

## External scripts

Script files are attached to HTML with the src attribute:
`<script src="/path/to/script.js"></script>`
 
Here, `/path/to/script.js` is an absolute path to the script from the site root. 
 
One can also provide a relative path from the current page. For instance, `src="script.js"` would mean a file `"script.js"` in the current folder.
 
We can give a full URL as well. For instance:
`<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.js"></script>`

# Syntax

Semicolons are technically optional when a line break exists (implicit semi colon), but are always recommended

# Comments

One line comments: `//`

Examples:  
`// This comment occupies a line of its own`  
`alert('World'); // This comment follows the statement`

Multi-line comments: `/* … */`

Example:
```
/* An example with two messages.
This is a multiline comment.
*/
```

# Variables

**Variable principles:**
- Declare using let or var
    - `let message;`
    - `var message;`
- Assign value to variable using equals sign
    - `message = 'Hello';`
- Can declare and assign on same line
    - `let message = 'Hello'`
    - `var message = 'Hello';`
- Do not declare more than once
- Can reassign variable value
    - `let message = 'Goodbye';`

**Variable naming limitations:**
- The name must contain only letters, digits, or the symbols $ and _.
- The first character must not be a digit.
- Case matters 
- English is conventional language
- Cannot use reserved words (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#keywords)

**Constants (Cannot be reassigned):**
- To declare a constant (unchanging) variable, use const instead of let:
    - `const birthday = '11.06.1997';`
- Can use constants as aliases for difficult to remember values (use upper-case)
    - `const COLOR_ORANGE = "#FF7F00";`

**Good rules to follow:**
- Use human-readable names like `userName` or `shoppingCart`.
- Stay away from abbreviations or short names like `a`, `b`, `c`, unless you really know what you’re doing.
- Make names maximally descriptive and concise. Examples of bad names are `data` and `value`. Such names say nothing. It’s only okay to use them if the context of the code makes it exceptionally obvious which data or value the variable is referencing.
- Agree on terms within your team and in your own mind. If a site visitor is called a “user” then we should name related variables `currentUser` or `newUser` instead of `currentVisitor` or `newManInTown`.

# Data Types
 
**8 basic data types:**
- `number` for numbers of any kind: integer or floating-point, integers are limited by ±(253-1).
- `bigint` is for integer numbers of arbitrary length.
- `string` for strings. A string may have zero or more characters, there’s no separate single-character type.
- `boolean` for true/false.
- `null` for unknown values – a standalone type that has a single value null.
- `undefined` for unassigned values – a standalone type that has a single value undefined.
- `object` for more complex data structures.
- `symbol` for unique identifiers.

The `typeof` operator allows us to see which type is stored in a variable.
- Two forms: `typeof x` or `typeof(x)`.
- Returns a string with the name of the type, like "string".
- For null returns "object" – this is an error in the language, it’s not actually an object.

# Strings
 
**3 types of quotes**
- Double quotes: `"Hello"`.  
- Single quotes: `'Hello'`.  
- Backticks: ``` `Hello` ```.  

Double and single quotes are “simple” quotes. There’s practically no difference between them in JavaScript.

Backticks are “extended functionality” quotes. They allow us to embed variables and expressions into a string by wrapping them in ${…}, for example:

```
let name = "John";
alert( `Hello, ${name}!` ); // Hello, John!
```

# Interactions

**Functions:**
- alert: shows a message.
    - `alert("Hello");`
- prompt: shows a message asking the user to input text. It returns the text or, if Cancel button or Esc is clicked, null.
    - `result = prompt(title, [default]);`
    - title: The text to show the visitor.
    - default: An optional second parameter, the initial value for the input field.
- confirm: shows a message and waits for the user to press “OK” or “Cancel”. It returns true for OK and false for Cancel/Esc.
    - `result = confirm(question);`

All these methods are modal: they pause script execution and don’t allow the visitor to interact with the rest of the page until the window has been dismissed.

There are two limitations shared by all the methods above:
1. The exact location of the modal window is determined by the browser. Usually, it’s in the center.
2. The exact look of the window also depends on the browser. We can’t modify it.

# Conversions

**The three most widely used type conversions:**
- String Conversion – Occurs when we output something. Can be performed with `String(value)`. The conversion to string is usually obvious for primitive values.  
- Numeric Conversion – Occurs in math operations. Can be performed with `Number(value)`
- Boolean Conversion – Occurs in logical operations. Can be performed with `Boolean(value)`.

Most of these rules are easy to understand and memorize. The notable exceptions where people usually make mistakes are:
- undefined is NaN as a number, not 0.
- "0" and space-only strings like " " are true as a boolean.

# Basic Operators

**Definitons**
- An operand – is what operators are applied to. For instance, in the multiplication of 5 * 2 there are two operands: the left operand is 5 and the right operand is 2. Sometimes, people call these “arguments” instead of “operands”.
- An operator is unary if it has a single operand
- An operator is binary if it has two operands

**The following math operations are supported:**
- Addition `+`,
    - Concatenation
        - if the binary + is applied to strings, it merges (concatenates) them
        - Note that if any of the operands is a string, then the other one is converted to a string too.
        - Goes left to right 
            - `2 + 2 + '1'; // "41"`
            - `'1' + 2 + 2; // "122"`
    - Numeric conversion, unary +
        ```
        let apples = "2";`
        let oranges = "3";
        - apples + oranges; // "23"
        - +apples + +oranges; // 5
        ```
- Subtraction `-`
- Multiplication `*`
- Division `/`
- Remainder `%`
    - The result of `a % b` is the remainder of the integer division of a by b.
- Exponentiation `**`
    - The exponentiation operator `a ** b` raises a to the power of b.
    - In school maths, we write that as a^b.

**Operator Precedence**
- Unary precedence is above binary - normal rules apply otherwise (PEMDAS)

**Modify-in-place**
```
n = n + 5; // Same as
n += 5;

n = n * 2; // Same as
n *= 2; 
```

**Increment/ decrement**
- Increment ++ increases a variable by 1
- Decrement -- decreases a variable by 1

**The operators ++ and -- can be placed either before or after a variable**
- postfix form
    - Operator goes after the variable `counter++`
    - Returned value is the old value
- prefix form
    - Operator goes before the variable: `++counter`
    - Returned value is the new value
- If not using a returned value (ie a=counter++) then it doesn’t matter where you put ++

# Comparisons

**Comparison operators: **
- Greater/less than: `a > b`, `a < b`.
- Greater/less than or equals: `a >= b`, `a <= b`.
- Equals: `a == b`, please note the double equality sign == means the equality test, while a single one `a = b` means an assignment.
- Strict equality check: `a === b` returns false for differing data types between a and b
- Not equals: In mathematics the notation is ≠, but in JavaScript it’s written as `a != b`.

**Notes:**
- Comparison operators return a boolean value.
- Strings are compared letter-by-letter in the “dictionary” order.
- When values of different types are compared, they get converted to numbers (with the exclusion of a strict equality check).
- The values `null` and `undefined` equal `==` each other and do not equal any other value.
- Be careful when using comparisons like ``> or `<` with variables that can occasionally be `null/undefined`. Checking for `null/undefined` separately is a good idea.

# Conditional Branching
 
The if(...) statement evaluates a condition in parentheses and, if the result is true, executes a block of code.
Sometimes, we’d like to test several variants of a condition. The else if clause lets us do that.
The if statement may contain an optional “else” block. It executes when the condition is falsy.

```
let year = prompt('In which year was the ECMAScript-2015 specification published?', '');
 
if (year < 2015) {
  alert( 'Too early...' );
} else if (year > 2015) {
  alert( 'Too late' );
} else {
  alert( 'Exactly!' );
}
```

The “conditional” or “question mark” operator lets us assign a variable depending on a condition in a shorter and simpler way.
`let result = condition ? value1 : value2;`

It is not recommended to use ? in place of if statements because if statements have better readability

Th switch statement
- A switch statement can replace multiple if checks.
- It gives a more descriptive way to compare a value with multiple variants.
- The switch has one or more case blocks and an optional default.

Example:
```
switch(x) {
  case 'value1':  // if (x === 'value1')
    ...
    [break]

  case 'value2':  // if (x === 'value2')
    ...
    [break]

  default:
    ...
    [break]
}
```

Notes:
- The value of x is checked for a strict equality to the value from the first case (that is, value1) then to the second (value2) and so on.
- If the equality is found, switch starts to execute the code starting from the corresponding case, until the nearest break (or until the end of switch). 
- If no case is matched then the default code is executed (if it exists).
- Must include breaks if you do not want all the cases run following the equality
- Type matters: Equality check is always strict. The values must be of the same type to match.

# Logical Operators

There are four logical operators in JavaScript: `||` (OR), `&&` (AND), `!` (NOT), `??` (Nullish Coalescing). 

**The “OR” `||` operator:**
- `result = a || b`;
- In classical programming, the logical OR is meant to manipulate boolean values only. If any of its arguments are true, it returns true, otherwise it returns false.
- In JavaScript OR `||` finds the first truthy value (or last value)
    - Given multiple OR’ed values:
    - `result = value1 || value2 || value3;`
    - The OR || operator does the following:
        - Evaluates operands from left to right.
        - For each operand, converts it to boolean. If the result is true, stops and returns the original value of that operand.
        - If all operands have been evaluated (i.e. all were false), returns the last operand.

**The AND `&&` operator:**
- `result = a && b;`
- In classical programming, AND returns true if both operands are truthy, and false otherwise:
- In JavaScript AND `&&` finds the first truthy value (or last value)
    - Given multiple AND’ed values:
    - `result = value1 && value2 && value3;`
    - The AND && operator does the following:
        - Evaluates operands from left to right.
        - For each operand, converts it to a boolean. If the result is false, stops and returns the original value of that operand.
        - If all operands have been evaluated (i.e. all were truthy), returns the last operand.

**The NOT `!` operator:**
- The boolean NOT operator is represented with an exclamation sign !.
- The syntax is pretty simple:
    - `result = !value;`
    - The operator accepts a single argument and does the following:
        - Converts the operand to boolean type: true/false.
        - Returns the inverse value.

**The nullish coalescing `??` operator:**
- Provides a short way to choose the first “defined” value from a list.
- It’s used to assign default values to variables:
- `height = height ?? 100; // set height=100, if height is null or undefined`
- It’s forbidden to use it with || or && without explicit parentheses.
- The operator ?? has a very low precedence, only a bit higher than ? and =, so consider adding parentheses when using it in an expression.
- The important difference between ?? amd || is that:
    - || returns the first truthy value.
    - ?? returns the first defined value.

**Precedence**
- All basic operators
- NOT ! (highest of all logical operators, so it always executes first)
- AND &
- OR ||
- Nullish coalescing operator ??
- Evaluators (? and =)

# Loops

**3 types of loops:**
- `while` – The condition is checked before each iteration.
    ```
    while (condition) {
        // code
        // so-called "loop body"
    }
    ```
- `do..while` – The condition is checked after each iteration.
    ```
    do {
    // loop body
    } while (condition);
    ```
- `for (;;)` – The condition is checked before each iteration, additional settings available.
    ```
    for (begin; condition; step) {
    // ... loop body ...
    }
     ```

**Advanced features:**
- To make an “infinite” loop, usually the while(true) construct is used. Such a loop, just like any other, can be stopped with the break directive.
- If we don’t want to do anything in the current iteration and would like to forward to the next one, we can use the `continue` directive.
- The `break` directive is activated at the line (*) if the user enters an empty line or cancels the input. It stops the loop immediately, passing control to the first line after the loop. 
- A label is the only way for break/continue to escape a nested loop to go to an outer one.
    - Example:
        ```
        outer: for (let i = 0; i < 3; i++) {
        
            for (let j = 0; j < 3; j++) {
            
                let input = prompt(`Value at coords (${i},${j})`, '');
            
                // if an empty string or canceled, then break out of both loops
                if (!input) break outer; // (*)
            
                // do something with the value...
            }
        }
        alert('Done!');
        ```

# Functions

**Function declaration:**
```
function name(parameters, delimited, by, comma) {
  /* code */
}
```
- Values passed to a function as parameters are copied to its local variables.
- A function may access outer variables. But it works only from inside out. The code outside of the function doesn’t see its local variables.
- A function can return a value. If it doesn’t, then its result is undefined.
- To make the code clean and easy to understand, it’s recommended to use mainly local variables and parameters in the function, not outer variables.
- It is always easier to understand a function which gets parameters, works with them and returns a result than a function which gets no parameters, but modifies outer variables as a side-effect.
A function should only perform one action, you may need to create a second function to perform multiple functions

**Function naming:**
- A name should clearly describe what the function does. When we see a function call in the code, a good name instantly gives us an understanding what it does and returns.
- A function is an action, so function names are usually verbal.
- There exist many well-known function prefixes like create…, show…, get…, check… and so on. Use them to hint what a function does.

**Function expression:**
```
let sayHi = function() {
  alert( "Hello" );
};
```

**Notes:**
- Functions are values. They can be assigned, copied or declared in any place of the code.
- If the function is declared as a separate statement in the main code flow, that’s called a “Function Declaration”.
- If the function is created as a part of an expression, it’s called a “Function Expression”.
- Function Declarations are processed before the code block is executed. They are visible everywhere in the block.
- Function Expressions are created when the execution flow reaches them.
- In most cases when we need to declare a function, a Function Declaration is preferable, because it is visible prior to the declaration itself. That gives us more flexibility in code organization, and is usually more readable.
- We should use a Function Expression only when a Function Declaration is not fit for the task.

**Arrow functions:**
Arrow functions are handy for one-liners. They come in two flavors:
- Without curly braces: `(...args) => expression` – the right side is an expression: the function evaluates it and returns the result.
- With curly braces: `(...args) => { body }` – brackets allow us to write multiple statements inside the function, but we need an explicit return to return something.

Example: `let sum = (a, b) => a + b;`

**Immediately Invoked Function Expressions (IIFE)**

Executes functions immediately, as soon as they are created. IIFEs are very useful because they don't pollute the global object, and they are a simple way to isolate variables declarations

Syntax (must include extra parenthesis):
```
;(function() {
  /* */
})()
```
Do not require a name because they run immediately and then are never run again.

Semi-colon is optional and just prevents syntax errors if combined with a file that does not use semi-colons

If the IIFE is set as a variable, the IIFE returns an object that contains the methods included

```
var calculator = (function () {
    function add(a, b) {
        return a + b;
    }

    function multiply(a, b) {
        return a * b;
    }
    return {
        add: add,
        multiply: multiply
    }
})();
```

To access this object, you must load the .js script it in to HTML file. In this case, in the case above, you'd access the functions using calculator.add() in any scripts following it in the HTML file

# Objects

Objects are associative arrays with several special features.

They store properties (key-value pairs), where:
- Property keys must be strings or symbols (usually strings).
- Values can be of any type.

There are two ways to create objects:
- object constructor syntax: `let user = new Object();`
- object literal syntax: `let user = {};`
- object literal is more common

```
let user = {     // an object
  name: "John",  // by key "name" store value "John"
  age: 30,        // by key "age" store value 30
};
```

The last property in the list may end with a comma to make it easier to add/remove/move around properties, because all lines become alike.

Computed properties: We can use square brackets in an object literal, when creating an object. 

```
let fruit = prompt("Which fruit to buy?", "apple");
let bag = {
  [fruit]: 5, // the name of the property is taken from the variable fruit
};
```

To access a property, we can use:
- The dot notation: obj.property.
        `user.age`
- Square brackets notation obj["property"]. Square brackets allow to take the key from a variable, like obj[varWithKey].
        `user[age]`
- Square brackets are much more powerful than the dot notation. They allow any property names and variables. But they are also more cumbersome to write.
- Most of the time, when property names are known and simple, the dot is used. And if we need something more complex, then we switch to square brackets.

We often use existing variables as values for property names. Instead of name:name we can just write name, like this:
```
function makeUser(name, age) {
  return {
    name, // same as name: name
    age,  // same as age: age
    // ...
  };
}
```

Additional operators:
To delete a property: delete obj.prop.
        `delete user.age`
To check if a property with the given key exists: "key" in object
```
let user = { name: "John", age: 30 };
"age" in user; // true, user.age exists
```
To iterate over an object: for (let key in obj) loop.
```
for (key in object) {
  // executes the body for each key among object properties
}
```

There are many other kinds of objects in JavaScript:
- Array to store ordered data collections,
- Date to store the information about the date and time,
- Error to store the information about an error.
- They have their special features, but formally they are not types of their own, but belong to a single “object” data type. And they extend it in various ways.

Objects are assigned and copied by reference. In other words, a variable stores not the “object value”, but a “reference” (address in memory) for the value. So copying such a variable or passing it as a function argument copies that reference, not the object itself.

All operations via copied references (like adding/removing properties) are performed on the same single object.

To make a “real copy” (a clone) we can use Object.assign for the so-called “shallow copy” (nested objects are copied by reference) or a “deep cloning” function, such as _.cloneDeep(obj).

The Object.keys() method returns an array of a given object's own enumerable property names, iterated in the same order that a normal loop would.

# Notes from Abhinav’s Code
 
Format for JavaScript: Functions can be combined using .
Example:  `(d.getMonth()+1).toString().padStart(2, '0')`
 
Functions used:
- `getFullYear()`: Gets 4 digit year YYYY according to local time
- `getMonth()`: returns the month in the specified date according to local time, as a zero-based value (where zero indicates the first month of the year) 
    - Must add 1 to getMonth result to start with January as 1
- `getDate()`: returns the day of the month (from 1 to 31) for the specified date.
- `getHours()`: hour (from 0 to 23) of the specified date and time
- `getMinutes()`: returns the minutes (from 0 to 59) of the specified date and time
- `getSeconds()`: returns the seconds (from 0 to 59) of the specified date and time
- `padStart(targetLength, padString)` - pads the current string with another string (multiple times, if needed) until the resulting string reaches the target length. The padding is applied from the start of the current string. padString is the string used to pad (default is space  “ “ if left blank)
- `toString()`: Converts value to a string
- `new Date()`: creates a new date object with the current date and time
- `setMinutes()`: sets the minutes of a date object
- `setSeconds()`: sets the seconds of a date object
- `setDate()`: Set the day of the month to a specified date using an integer representing the day of a month.
    - Expected values are 1-31, but other values are allowed:
    - 0 will result in the last day of the previous month
    - -1 will result in the day before the last day of the previous month
    - 32: If the month has 31 days, the first day of the next month. If the month has 30 days, the second day of the next month
    - Can specify other options using these principles
- `push()`: Adds new items to the end of an array, and returns the new length.
- `unshift()`: Adds new items to the beginning of an array, and returns the new length






















