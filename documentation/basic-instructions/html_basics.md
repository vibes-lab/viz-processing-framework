## HTML Basics

Content summarized from : https://www.w3schools.com/html/default.asp 

# Intro
- HTML stands for Hyper Text Markup Language
- HTML is the standard markup language for creating Web pages
- HTML describes the structure of a Web page
- HTML consists of a series of elements  
- HTML elements tell the browser how to display the content  
- HTML elements label pieces of content such as "this is a heading", "this is a paragraph", "this is a link", etc.

# Example format

```
<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
</head>
<body>

<h1>This is a Heading</h1>
<p>This is a paragraph.</p>

</body>
</html>
```

# HTML Elements

- An HTML element is defined by a start tag, some content, and an end tag:
- `<tagname>Content goes here...</tagname>`
- The visible part of the HTML document is between `<body>` and `</body>`.
- HTML tags are not case sensitive: `<P>` means the same as `<p>`.
- With HTML, you cannot change the display by adding extra spaces or extra lines in your HTML code. The browser will automatically remove any extra spaces and lines when the page is displayed:


**Types of tags:**

**Headings:**
- HTML headings are defined with the `<h1>` to `<h6>` tags.
- `<h1>` defines the most important heading. `<h6>` defines the least important heading
- `<h1>This is heading 1</h1>`
- Each HTML heading has a default size. However, you can specify the size for any heading with the style attribute, using the CSS font-size property
- `<h1 style="font-size:60px;">Heading 1</h1>`

**Links:**
- HTML links are defined with the `<a>` tag:
- `<a href="https://www.w3schools.com">This is a link</a>`

**Images:**
- HTML images are defined with the `<img>` tag.
- The source file (src), alternative text (alt), width, and height are provided as attributes:
- `<img src="w3schools.jpg" alt="W3Schools.com" width="104" height="142">`

**Line break:**
- The `<br>` tag defines a line break, and is an empty element without a closing tag
- `<p>This is a <br> paragraph with a line break.</p>`

**Thematic break:**
- The `<hr>` tag defines a thematic break in an HTML page, and is most often displayed as a horizontal rule.
- The `<hr>` tag is an empty tag, which means that it has no end tag.

**Pre-formatted Text:**
- The HTML `<pre>` element defines preformatted text.
- The text inside a `<pre>` element is displayed in a fixed-width font (usually Courier), and it preserves both spaces and line breaks
```
<pre>
  My Bonnie lies over the ocean.
  My Bonnie lies over the sea.
  My Bonnie lies over the ocean.
  Oh, bring back my Bonnie to me.
</pre>
```

[Complete list of tags](https://www.w3schools.com/tags/default.asp)

# Attributes

- All HTML elements can have attributes
- The `href` attribute of `<a>` specifies the URL of the page the link goes to
- The `src` attribute of `<img>` specifies the path to the image to be displayed
- The `width` and `height` attributes of `<img>` provide size information for images
- The `alt` attribute of `<img>` provides an alternate text for an image
- The `style` attribute is used to add styles to an element, such as color, font, size, and more
- The `lang` attribute of the `<html>` tag declares the language of the Web page
- The `title` attribute defines some extra information about an element (in the tooltip)
- It is recommended (but not required) to use lowercase attribute names and double quotes for the contents
    `<a href="https://www.w3schools.com/html/">Visit our HTML tutorial</a>`

# Formatting Elements

Formatting elements were designed to display special types of text:
- `<b>` - Bold text
- `<strong>` - Important text (bold)
- `<i>` - Italic text
- `<em>` - Emphasized text (italic)
- `<mark>` - Marked text (highlight)
- `<small>` - Smaller text
- `<del>` - Deleted text (strike through)
- `<ins>` - Inserted text (underline)
- `<sub>` - Subscript text
- `<sup>` - Superscript text

Example: `<em>This text is emphasized</em>`

# Comments
- HTML comments are not displayed in the browser, but they can help document your HTML source code.
- Comments are also great for debugging HTML, because you can comment out HTML lines of code, one at a time, to search for errors.

`<!-- Write your comments here -->`

# JavaScript Interaction

- The HTML `<script>` tag is used to define a client-side script (JavaScript).
- The `<script>` element either contains script statements, or it points to an external script file through the src attribute.
- Common uses for JavaScript are image manipulation, form validation, and dynamic changes of content.
- To select an HTML element, JavaScript most often uses the `document.getElementById()` method.
- The HTML `<noscript>` tag defines an alternate content to be displayed to users that have disabled scripts in their browser or have a browser that doesnt support scripts

  ```
  <!DOCTYPE html>
  <html>
  <body>

  <button type="button" onclick="myFunction()">Click Me!</button>

  <p id="demo">This is a demonstration.</p>

  <script>
  function myFunction() { 
    document.getElementById("demo").innerHTML = "Hello JavaScript!";
  }
  </script>

  </body>
  </html>
  ```

# Basic Terms

**DOM (Document Object Model):**
- DOM is an API (Application Programming Interface) that represents and interacts with any HTML or XML document
- DOM is a document model loaded in the browser and representing the document as a node tree, where each node represents part of the document (e.g. an element, text string, or comment)
- DOM is one of the most-used APIs on the Web because it allows code running in a browser to access and interact with every node in the document
- Nodes can be created, moved and changed. Event listeners can be added to nodes and triggered on occurrence of a given event

**Document Interface:**
- Represents any web page loaded in the browser and serves as an entry point into the web page's content
- It provides functionality globally to the document, like how to obtain the page's URL and create new elements in the document

Constructor:   
- `Document()` creates a new Document object

Properties/ Methods/ Event Handlers:
- [Full list of properties](https://developer.mozilla.org/en-US/docs/Web/API/Document#properties)
- Access using `Document.propertyName`
- Have standard properties, specific HTMLDocuments properties, methods, and event handlers
- Some event handlers use the following format: `GlobalEventHandlers.onselectionchange`

Events:
- Listen to these events using `addEventListener()` or by assigning an event listener to the oneventname property of this interface.
- These include animation, clipboard, drag and drop, fullscreen, keyboard, load and unload, pointer, selection, touch, and transition events

**Window Interface:**
- Represents a window containing a DOM document
- The document property points to the DOM document loaded in that window
- A window for a given document can be obtained using the `document.defaultView` property.
- A global variable, window, representing the window in which the script is running, is exposed to JavaScript code.
- In a tabbed browser, each tab is represented by its own Window object; the global window seen by JavaScript code running within a given tab always represents the tab in which the code is running. 
-See [Full list of methods, properties, etc.](https://developer.mozilla.org/en-US/docs/Web/API/Window#constructors)

You can use window and document in JavaScript functions to assign variables globally

# CSS

**Basics:**
- Use the HTML style attribute for inline styling
- Use the HTML `<style>` element to define internal CSS
- Use the HTML `<link>` element to refer to an external CSS file
    `<link rel="stylesheet" href="/html/styles.css">`
    `<link rel="stylesheet" href="https://www.w3schools.com/html/styles.css">`
- Use the HTML `<head>` element to store `<style>` and `<link>` elements
```
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="styles.css">
</head>
<body>

<h1>This is a heading</h1>
<p>This is a paragraph.</p>

</body>
</html>
```

**Properties:**
- `color` defines the text color to be used
- `font-family` defines the font to be used
- `font-size` defines the text size to be used
- `border` defines a border around an HTML element.
- `padding` defines a padding (space) between the text and the border
- `margin` defines a margin (space) outside the border.


