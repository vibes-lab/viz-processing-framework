## Information about Python Syntax used in Application

**map()**
The map() function in Python takes in a function and a list as an argument. The function is called with a lambda function and a list and a new list is returned which contains all the lambda modified items returned by that function for each item

**Indexing**
One of the neat features of Python lists is that you can index from the end of the list. You can do this by passing a negative number to []. It essentially treats len(array) as the 0th index. So, if you wanted the last element in array, you would call array[-1].

**json**
json.dumps() function converts a Python object into a json string

**.format()**
“text {0} {1}”.format(x,y) adds x and y for the indices 0 and 1

**Try Statements**
Since the try block raises an error, the except block will be executed.
Without the try block, the program will crash and raise an error:
