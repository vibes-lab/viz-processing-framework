# Work Flow

1. Make sure all services (Zookeeper, Kafka, Cassandra, web application) are running properly
2. Check that FTP server is accessible
3. Ensure that data acquisition settings are as desired
4. Update .lastread file to desired processing start time 
5. Run initialization script to start processing using screen
6. Begin using the web application

# 1 Checking/ restarting services
- Check status `sudo systemctl status <service name>`
- If not running properly, use ansible playbook to restart services on all nodes `ansible-playbook ~/housekeeping/ansible_playbooks/start_services.yml --ask-pass --ask-become-pass`
- Use sudo password twice
- See [installation/ configuration docs](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/server-documentation/setup/2_software-installation-configuration.md) for playbook details

# 2 Checking FTP server
- Make sure data acquisition server has the FTP firewall rules enabled
- Make sure config file has the correct FTP server IP address and file path `sudo vim /home/vtsil/vtsil-data-viz-api/datavizapi/config.yml`
- See [ftp server docs](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/server-documentation/setup/3_ftp_server_configuration.md) for details 

# 3 Data acquisition
- See [data acquisition docs](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/server-documentation/maintenance/data-acquisition.md)

# 4 Updating .lastread file
- As soon as a new files is saved that exceeds the .lastread time, a new file will sync and processing will begin
    - Format: `YYYY-MM-DD_HH_mm_ss`, ex: `2022-05-08_19_41_24`
- If you choose a .lastread time in the past, every file saved since that time will sync immediately, and the server will have to "catch up" to the current time
- If you accidently choose a time that is too far in the past and don't want to wait for all the old data to process, you can reset the topics
- Use the ansible playbook here: `/home/vtsil/housekeeping/ansible_playbooks/reset_kafka.yml` or manually execute the same commands
    ```
    ---
    - hosts: localhost
    tasks:
        - name: List kafka topics in inputs/kafka-topics.txt
        shell: "/opt/kafka/bin/kafka-topics.sh --zookeeper node0 --list | awk 'FNR>1{print $0}' > inputs/kafka-topics.txt"
        - name: Delete kafka topics listed in inputs/kafka-topics.txt
        shell: "while read t; do /opt/kafka/bin/kafka-topics.sh --zookeeper node0 --delete --topic $t; done < inputs/kafka-topics.txt"

    - hosts: all
    become: true
    become_method: sudo
    tasks:
        - name: Restart Kafka
        shell: "systemctl restart kafka"
    ```

# 5 Processing initialization script
- Work in virtual environment `workon flask-app`
- Start script `python /home/vtsil/housekeeping/screen_kafka_pubsub.py`
- Input the desired number of script instances to run
- Check that processing has begun by attaching to screens and checking outputs
    - List active screens: `screen -ls`
    - Choose a processing task: `screen -r <screen number or name>`

**Script**
```
import os
import sys

if __name__ == '__main__':
    os.system('killall screen')
    n1=input("Number of script instances: ")

    for i in range(n1):
        os.system("screen -d -S fft_sec -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python psd_fft.py;'")
        os.system("screen -d -S consumer_raw -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python consumer_raw.py;'")
        os.system("screen -d -S hdfs_to_kafka -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python put_file_in_kafka.py;'")

    os.system("screen -d -S file_sync -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python file_sync_stable.py;'")
    os.system("screen -d -S fft_hour -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python fft_by_hour.py;'")
    os.system("screen -d -S fft_day -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python fft_by_day.py;'")
```

**Details on screen**
- Screen installation `sudo apt-get install screen`  
- Check location of program files `which screen`
- Create new window `screen` optional, can add name `screen -S <name of screen>`
- Anytime you use the `screen` command, you enter a new screen and must detach or terminate the screen to exit
- Check screen version: `ctrl`+`a` then `v`
- Detach from screen `ctrl`+`a` then `d`
- See detached screen sessions: `screen –ls`
- Reattach screen: `screen –r`
- Kill current window: `ctrl`+`a` then `k`
- Terminate session: `ctrl` + `c`
- Quit screen and kill all windows: `ctrl`+`a` then `\`
- Details on screen commands: [Here](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-screen-on-an-ubuntu-cloud-server)

# 6 Begin using web application
- Troubleshooting
- If web application does not load there is an error in the code
- If page loads but is not working properly, right click and go to inspect to check console for errors
- If there is a JS error, investigate the specific error
- If there is a 500 error, go to `vim /var/logs/uwsgi/vtsil-web-server.log` to see the error
- Most common cause for 500 error: An invalid query (requested data that does not exist in Cassandra)
    - Error handling for invalid inputs is not currently in place, although the framework to add it exists (just modify code to send "error" as the response)
    - Check whether data exists in Cassandra using cqlsh (see data processing) to confirm whether the query is valid

