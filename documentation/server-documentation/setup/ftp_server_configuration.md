## Setting up FTP Server

Instuctions for how to set up an FTP server the transfer data between the data acquisition server and the processing/ visualization server

- Install FileZilla
    - http://filezilla-project.org/download.php?type=server
- Open `FileZilla Server Interface`
- Click `OK` to connect to server
- Share folder
    - `Edit > Users > Shared Folders > Add >Users > vtsil`
    - Specify path to existing folder or create a new folder for FTP 
    - `Add > /path/to/ftp/folder/ > OK`
    - Assign user permissions as appropriate
- Secure FTP Server
    - Create password for user 
        - `Edit > Users > vtsil > check password box > type password`
        - Same password as virtual server (hint: tmwiP)
    - Specify available ports 
    - `Edit > Settings > General Settings > Listen on these ports > Change to desired port`
        - Use secure ports per documentation
        - Used Port `54316` initially
    - Specifiy allowable IP range
        - `Edit > Settings > General Settings > IP Filter` 
        - Use asterisk in first box to prevent all IP's
        - Allow only VT VPN `172.27.0.0/16` and server head node `128.173.25.223`
	- Specify passive ports
	- `Edit > Settings > General Settings > Passive mode settings`
	- `Use custom port range: 50000-50100`
    - Use as few ports as possible (50 minimum for performance)
- Change welcome message to be descriptive
    - `Edit > Settings > General Settings > Welcome message`
    - ` VTSIL FTP Server`
Windows Firewall Exception
- To create:
    - Open `Windows Firewall with Advanced Security`
    - `Inbound Rules > New Rule`
    - Rule type: `Port`
    - Rule applies to `TCP`
    - Specific Local Ports: `54316, 50000-50100`
    - `Allow the connection`
    - When does this rule apply: Check all (domain, private, public)
    - Create informative name and description
- Edit existing rule:
    - Edit name and description
        - `Properties > General`
    - Specify ports to make available
        - `Properties > Protocols and Ports`
        - Portocol type: `TCP`
        - Local port: `Specific Ports` `54316, 50000-50100`
    - Change rules scope 
        - `Properties > Advanced > Check all boxes (domain, private, public)`
- Restrict IP addresses that can access:
    - `Properties > Scope > These IP addresses > Add`
    - `172.27.0.0/16` `128.173.25.223`
- Enable firewall exception only when using FTP server and disable at all other times

## Connecting to FTP Folder
For Linux terminal:
- `ftp -p 198.82.17.8 54316`
- Enter username and password to connect
- Works like command line
- `exit` to leave
- Use `ls` to ensure that you can see files, can use `sudo vim` to test read/ write permissions as well

