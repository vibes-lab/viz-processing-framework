# Virtual Server Set-up Instructions

Instructions for setting up a virtual server to test the framework prior to implementation on physical machines (optional)

** **Note**: This step in set-up is only required if a virtual environment is being configured for testing. If only physical servers will be used, these instructions can be skipped 

## VirtualBox Set-up Instructions


**Download Instructions:**
- Download VirtualBox 6.1.16 platform packages (Windows host) at https://www.virtualbox.org/wiki/Downloads  
- Download VirtualBox 6.1.16 Oracle VM VirtualBox Extension Pack at https://www.virtualbox.org/wiki/Downloads  
- Download Ubuntu 18.04.5 LTS VM Image at https://www.linuxvmimages.com/images/ubuntu-1804/  


**Ubuntu 18.04 Image**  

- Import image:
	- Open VirtualBox
	- File -> Import Appliance
	- Browse and find image that was downloaded
	- Choose desired settings  
- Image details:
	- Ubuntu 18.04.5 LTS 
	- Default login
		- Username: ubuntu 
		- Password : ubuntu

**Network Set-up**
- Use GUI for network set-up or look-up associated commands
- NAT Network
	- File -> Preferences (Ctrl+G) -> Network Tab -> Add new NAT Network
	- Edit Selected NAT Network
		- Rename Network
		- Check enable network
		- Check Supports DHCP
		- Don't need Port Forwarding in our case
- Host-only Network
	- Tools -> "Virtual Box Host-Only Ethernet Adapter"
		- Check "Configgure Adapter Manually"
		- Use default settings
		- Ensure DHCP Server is enabled (Check box)
- Set Networks for individual VM's
	- Selct Virtual Machine -> Settings -> Network Tab
	- All nodes
		- Enable Adapter 1
		- NAT Network
		- Select NAT Network created above
		- Promiscuouss Mode: Allow VMs
		- Check cable connected
	- Head node
		- Enable Adpater 2
		- Host-only Adpater
		- Select Network created above
		- Promiscuous Mode: Deny 
		- Check cable connected
- Configure machine to use network settings
	- Head node
		- `sudo vim /etc/network/interfaces`
		- Enter following text
			```
			# This file describes the network interfaces available on your system and how to activate them. 
			# For more information, see interfaces(5).

			# The loopback network interface
			auto lo
			iface lo inet loopback

			# The primary network interface
			auto eth0
			iface eth0 inet dhcp

			# The secondary network interface
			auto eth1
			iface eth1 inet static
				address 192.168.56.101
				netmask 255.255.255.0
			```
		- Restart the networking services by restarting guest VM
- Rename Virtual Machine to something descriptive
	- Use GUI to rename
	- Settings -> Details -> Edit from ubuntu1804
		- Node 0: "vs-node0"
		- Node 1: "vs-node1"
		- Node 2: "vs-node2"
		- vs = virtual server


**Setting up users**
- Set up user and give ability to run commands in privileged mode
    - Add user `sudo useradd vtsil -m`
    - Set bash as default shell `sudo usermod --shell /bin/bash vtsil`
    - Create password `sudo passwd vtsil`
    - Add to sudo group `sudo usermod -aG sudo vtsil`
    - Restart to see user updates `sudo systemctl restart sshd`
- Check that user is set up correctly
    - Switch to new user `su -l vtsil`
    - Check groups for new user `groups $USER`
    - Should say vtsil:vtsil sudo
-  Reset passwords for extra accounts
	- ubuntu
	- linuxnmimages
	- `sudo passwd username`
- Remove extra accounts from login menu
	- `cd /var/lib/AccountsService/users/`
	- `sudo vim username`
	- Change the following line from `SystemAccount=false` to `SystemAccount=true`


**User Passwords**
- ubuntu: gohokies2020
- linuxvmimage: gohokies2020
- vtsil: themagicwordisPL3@$E


**Navigating Network**
- Find Head node IP Address (192.168.56.101)
	- `ip addr | grep 'inet[^6]'`
- Use `ifconfig` to see IP address for each Node
- Internal IP Addresses:
	- Host: 10.0.2.2
	- Node 0: 10.0.2.4
	- Node 1: 10.0.2.5
	- Node 2: 10.0.2.6
- Navigate between nodes using `ssh username@IPAddress`
	- Use `exit` to go back to previous host
	- Will need to enter user password each time until SSH keys are added
- vtsil user is used for our servers

**Set up SSH for convenience**
- Generating SSH keys
	- On host machine
		- Log in as desired user: `su -l username`
		- `ssh-keygen -t RSA -b 4096`
		- Confirm save path (use default)
		- Add password or leave blank
- Adding authorized keys
	- On host machine
		- `cat ~/.ssh/id_rsa.pub`
		- Copy public key
	- On guest machine
		- Confirm you are logged in as vtsil user
			- If not `su -l vtsil`
		- `sudo vim ~/.ssh/authorized_keys`
		- Paste public key
- Creating shortcuts
	- On host machine
		- `sudo vim ~/.ssh/config`
		```
		# Shortcut to login to Node 1
		Host node1
			Hostname 10.0.2.4
			User vtsil

		# Automatically use vtsil user always
		Host *
			User vtsil
		```
		- Add all appropriate shortcuts for each VM
- Enable agent to prevent reentering password for certain time period
	- Enable agent: `eval $(ssh-agent)`
	- Add key `ssh-add -t1h ~/.ssh/id_rsa` - change time from 1h as desired


**Editing Banner Upon Login**
- Change Message of the Day
	- `sudo rm /etc/motd`
	- `sudo vim /etc/motd`
	- Create desired message "Welcome to the VTSIL server - Node 1"
- Disable other messages from executing
	- `sudo chmod -x /etc/update-motd.d/*`
- Edit configuration for sshd
	- `sudo vim /etc/ssh/sshd_config`
	- Uncomment banner or motd
- Restart sshd to update
	- `sudo systemctl restart sshd`

**Snapshots**
- Use to save progress at a certain point so you can restore that version later in needed
- In VirtualBox program
- Click button that looks like a bulleted list next to the VM name
- Choose Shanshots
- View and restore old snapshots or take a new snapshots
- Delete old snapshots to clear space (this will take some time because it combines information)

**Dynamic Allocation of Memory**
- Will expand dynamically but will not shrink dynamically
- To clear space on host machine:
	- Clear space on guest machine
		- Zero-filling on Linux 
		- Deleting a file however does not zero out the blocks, it only removes the file from the directory. In order to reclaim the disk space you need to zero out the data first. Which is what is described above using the zero.fill file.
		- Create giant empty file of zeros: `sudo dd if=/dev/zero | pv | sudo dd of=/bigemptyfile bs=4096k`
		- Remove the empty file: `sudo rm /bigemptyfile`
	- Compact the .vdi
		- Open Command Prompt on Windows
		- `"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" modifyhd "C:\Users\vipondn\VirtualBox VMs\server_vs-node1-head\Ubuntu_18.04.5_VB_LinuxVMImages.com-disk001.vdi" --compact`
		- OR `VBoxManage.exe modifyhd "C:\Users\vipondn\VirtualBox VMs\server_vs-node1-head\Ubuntu_18.04.5_VB_LinuxVMImages.com-disk001.vdi" --compact`
	- Remove unnecessary snapshots periodically as well


