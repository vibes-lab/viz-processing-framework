## Software Set-up

Installation and configuration steps for the required services for the data processing/ visualization server

**General Note:**
- Use files in housekeeping folder to assist with set-up
- Verify the housekeeping files against the instructions below, the instructions below are more updated

**Basic software set-up:**
- `sudo apt upgrade` (y for any confirmations needed)
- `sudo apt-get update` (to check that everything updated)
- `sudo apt-add-repository universe`
- `sudo apt update -y`
- `sudo apt install -y software-properties-common python-pip librdkafka-dev python-dev build-essential libssl-dev libffi-dev python-setuptools`
- `sudo apt install -y hdf5-tools ruby ruby-dev build-essential ipython`
- `sudo gem install kafkat`
- `sudo apt install librdkafka-dev python-dev`
- `pip install --user virtualenv`
- `pip install --user virtualenvwrapper`
- `pip install --user scipy`
- `pip install --user cassandra-driver`
- `pip install --user pytz`
- `sudo apt -y install xserver-xorg`

**Java set-up:**
- `sudo apt install default-jdk`
- `sudo apt update`
- `sudo apt install openjdk-8-jre`
- `sudo update-alternatives --config java` (note Java path)
- `sudo vim /etc/environment`
    -  add line with `JAVA_HOME="<Path to java from install>"`
    - `JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre/"`
    - Be sure to remove extra bin/java/ from the end of the path if needed

**Zookeeper Set-up**
- See getting started guide for details: https://zookeeper.apache.org/doc/current/zookeeperStarted.html
- Go to proper directory `cd /opt`
- Install `sudo wget https://downloads.apache.org/zookeeper/stable/apache-zookeeper-3.5.8-bin.tar.gz`
- Extract `sudo tar -zxf apache-zookeeper-3.5.8-bin.tar.gz`
- Rename `sudo mv apache-zookeeper-3.5.8-bin zookeeper`
- Remove tar.gz file `sudo rm -R apache-zookeeper-3.5.8-bin.tar.gz`
- Create directories and set appropriate ownership
    - `sudo mkdir /opt/zookeeper/data`
    - `sudo mkdir /opt/zookeeper/logs`
    - `sudo mkdir /var/log/zookeeper`
    - `sudo chown vtsil:vtsil -R /var/log/zookeeper`
- Assign server id to associated virtual machine
    - `cd /opt/zookeeper/data`
    - `sudo bash -c 'echo "1" > /opt/zookeeper/data/myid'`
        - Will be "2" and "3" for zoo2 and zoo3 servers
        - Assign server id to each virtual machine
        - Will need to set-up 3 virtual machines
- Rename/ add host names `sudo vim /etc/hosts`
    ```
    #127.0.0.1       localhost localhost.localdomain
    #127.0.1.1      ubuntu1804.linuxvmimages.local  ubuntu1804

    10.0.2.4       localhost localhost.localdomain #Node0
    10.0.2.5       localhost #Node1
    10.0.2.6       localhost #Node2
    #Put only proper ip for each node as localhost

    10.0.2.4 node0
    10.0.2.5 node1
    10.0.2.6 node2

    # The following lines are desirable for IPv6 capable hosts
    ::1     ip6-localhost ip6-loopback
    fe00::0 ip6-localnet
    ff00::0 ip6-mcastprefix
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters
    ```
- Change the configuration file
    - `cd /opt/zookeeper/conf`
    - Copy sample file `sudo cp zoo_sample.cfg zoo.cfg`
    - Edit configuration file `sudo vim /opt/zookeeper/conf/zoo.cfg`  
    ```
    tickTime=2000  
    initLimit=10  
    syncLimit=5  
    dataDir=/opt/zookeeper/data  
    dataLogDir=/opt/zookeeper/logs  
    clientPort=2181  
    server.1=node0:2888:3888  
    server.2=node1:2888:3888  
    server.3=node2:2888:3888  
    autopurge.snapRetainCount=3
    autopurge.purgeInterval=1
    # For whichever server is the localhost, change node# to 0.0.0.0 (ex. server.1=0.0.0.0:2888:3888 on server with myid=1)
    ```
- Make sure all files related to Zookeeper have correct owner
    - `sudo chown vtsil:vtsil -R /opt/zookeeper`
- Create service file
    - Edit file to match text below `sudo vim /lib/systemd/system/zookeeper.service`  
    ```            
    [Unit]  
    Description=Zookeeper Daemon  
    Documentation=http://zookeeper.apache.org  
    Requires=network.target  
    After=network.target  

    [Service]
    Type=forking
    WorkingDirectory=/opt/zookeeper
    User=vtsil
    Group=vtsil
    ExecStart=/opt/zookeeper/bin/zkServer.sh start /opt/zookeeper/conf/zoo.cfg
    ExecStop=/opt/zookeeper/bin/zkServer.sh stop /opt/zookeeper/conf/zoo.cfg
    ExecReload=/opt/zookeeper/bin/zkServer.sh restart /opt/zookeeper/conf/zoo.cfg
    TimeoutSec=30
    Restart=on-failure

    [Install]
    WantedBy=default.target
    ```
- `sudo chown vtsil:vtsil /lib/systemd/system/zookeeper.service`
- Create symbolic link for service file
    - `cd /etc/systemd/system/`
    - `sudo ln -s /lib/systemd/system/zookeeper.service /etc/systemd/system/zookeeper.service`
    - `sudo chown -h vtsil:vtsil /etc/systemd/system/zookeeper.service`
- Check if program runs in general 
    - `cd opt/zookeeper`
    - `sudo bin/zkServer.sh start`
    - `sudo bin/zkServer.sh stop`
    - `sudo bin/zkServer.sh status`
- Run using service file
    - `sudo systemctl enable zookeeper`
    - `sudo systemctl start zookeeper`
    - `sudo systemctl status zookeeper`
- Check leaders and followers `echo srvr | nc <nodeName or IP address> 2181 | grep Mode`

**Kafka Installation**

- Set-up directories
    - `sudo mkdir /opt/kafka`
    - `sudo chown vtsil:vtsil -R /opt/kafka/`
    - `sudo mkdir /mnt/sdc1/kafka`
    - `sudo chown -R vtsil:vtsil /mnt/sdc1/kafka`
    - `sudo ln -s /mnt/sdc1/kafka/logs /opt/kafka/data`

- Install
    - `cd /opt/kafka`
    - `sudo wget https://downloads.apache.org/kafka/2.4.0/kafka_2.11-2.4.0.tgz`
    - `sudo mv kafka_2.11-2.4.0.tgz kafka.tgz`
    - Check file type `file kafka.tgz`
    - `sudo tar -zxf kafka.tgz --strip 1`
- Configure
    - Edit following lines in properties file `sudo vim /opt/kafka/config/server.properties`
        ```
        broker.id=0 # 0 for node0, 1 for node1, 2 for node2
        listeners=PLAINTEXT://node0:9092 # Change node to match server where config file is located, this is for node0
        num.network.threads=4
        log.dirs=/opt/kafka/data
        offsets.topic.replication.factor=2
        transaction.state.log.replication.factor=2
        log.retention.hours=8
        log.retention.check.interval.ms=1000000
        zookeeper.connect=node0:2181,node1:2181,node2:2181
        zookeeper.connection.timeout.ms=30000
        group.initial.rebalance.delay.ms=3000
        ```
	- Edit following lines `sudo vim /opt/kafka/config/consumer.properties`
		```
		bootstrap.servers=node0:9092,node1:9092,node2:9092
		group.id=default-consumer-group
		auto.offset.reset=latest
		```
	- Edit following lines `sudo vim /opt/kafka/config/producer.properties`
		```
		bootstrap.servers=node0:9092,node1:9092,node2:9092
		```
    - Change owner `sudo chown vtsil:vtsil -R /opt/kafka`
    - Create service file `sudo vim /lib/systemd/system/kafka.service`
    - Enter following text:
    ```
        [Unit]
        Requires=zookeeper.service
        After=zookeeper.service

        [Service]
        Type=simple
        User=vtsil
        ExecStart=/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties
	ExecStart=/bin/sh -c '/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties > /opt/kafka/kafka.log 2>&1'
        ExecStop=/opt/kafka/bin/kafka-server-stop.sh
        Restart=on-abnormal

        [Install]
        WantedBy=multi-user.target
        ```
    - `sudo chown vtsil:vtsil /lib/systemd/system/kafka.service`
    - Create symbolic link for service file `sudo ln -s /lib/systemd/system/kafka.service /etc/systemd/system/kafka.service`
    - `sudo chown -h vtsil:vtsil /etc/systemd/system/kafka.service`
- Try starting Kafka
    - Make sure zookeeper is started first
    - `sudo systemctl enable kafka`
    - `sudo systemctl start kafka`
    - `sudo systemctl stop kafka`

**Cassandra Set-up**
- Install Cassandra
    - `echo deb http://dl.bintray.com/apache/cassandra 39x main | sudo  tee /etc/apt/sources.list.d/cassandra.list`
    - `gpg --keyserver keyserver.ubuntu.com --recv-keys 749D6EEC0353B12C`
    - `gpg --export --armor 749D6EEC0353B12C | sudo apt-key add -`
    - `gpg --keyserver keyserver.ubuntu.com --recv-keys A278B781FE4B2BDA`
    -` gpg --export --armor A278B781FE4B2BDA | sudo apt-key add -`
    - `sudo apt upgrade && sudo apt update -y`
    - `sudo apt install -y cassandra`
- Set-up Cassandra Directories
    - Make
        - `sudo mkdir /mnt/sdc1/cassandra`
        - `sudo mkdir /mnt/sdb1/cassandra`
        - `sudo mkdir /opt/cassandra`
    - Link 
        - `sudo ln -s /mnt/sdc1/cassandra/hints /opt/cassandra/hints`
        - `sudo ln -s /mnt/sdc1/cassandra/commitlog /opt/cassandra/commitlog`
        - `sudo ln -s /mnt/sdb1/cassandra/data /opt/cassandra/data`
        - `sudo ln -s /etc/cassandra/cassandra.yaml /var/lib/cassandra/cassandra.yaml`
    - Set proper owners
        - `sudo chown -R cassandra:cassandra /opt/cassandra`
        - `sudo chown -R cassandra:cassandra /mnt/sdb1/cassandra`
        - `sudo chown -R cassandra:cassandra /mnt/sdc1/cassandra`
- Before changing configuration file, wipe out old info to prevent name errors
    - `sudo systemctl start cassandra`
    - `nodetool decommission`
    - `nodetool flush system`
    - `sudo systemctl stop cassandra`
    - `sudo rm -rf /var/lib/cassandra/*`
    - `sudo rm -rf /var/log/cassandra/*`
    - `sudo rm -rf /mnt/sdb1/cassandra/data/system/*`
    - `sudo rm -rf /mnt/sdc1/cassandra/commitlog/*` 
    - `sudo rm -rf /mnt/sdc1/cassandra/hints/*`
- Edit yaml file `sudo vim /etc/cassandra/cassandra.yaml`
    ```
    # Edit following lines
    cluster_name: VTSIL Cluster
    data_file_directories: /opt/cassandra/data
    commitlog_directory: /opt/cassandra/commitlog
    seeds: “10.0.2.4,10.0.2.5,10.0.2.6”

    listen_address: 10.0.2.4 #match ip address of each individual VM
    rpc_address: 10.0.2.4 #match ip address of each individual VM
    endpoint_snitch: GossipingPropertyFileSnitch

    # Add line to end 
    auto_bootstrap: false

    # Leave all other lines unchanged
    ```
- Edit configuration file `sudo vim /opt/spark/conf/spark-env.sh`
    - Uncomment/add the following lines
    ```
    export SPARK_DIST_CLASSPATH=$(/usr/local/hadoop/bin/hadoop classpath)
    export HADOOP_PREFIX=/usr/local/hadoop
    export HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop
    export SPARK_MASTER_HOST=10.0.2.4
    ```
- Must make these changes on ALL nodes
- Check if services run correctly `sudo systemctl restart cassandra`
- Check logs `sudo vim /var/log/cassandra/system.log` `/var/log/cassandra/debug.log`

**Hadoop Installation**
- Install hadoop
    - `sudo mkdir /opt/hadoop`
    - `sudo chown vtsil:vtsil -R /opt/hadoop`
    - `sudo wget https://www-eu.apache.org/dist/hadoop/common/hadoop-3.1.4/hadoop-3.1.4.tar.gz -O /opt/hadoop/hadoop-3.1.4.tar.gz`
- Set-up directories
    - `sudo mkdir /usr/local/hadoop`
    - `sudo chown vtsil:vtsil /usr/local/hadoop`
    - `sudo tar -zxf /opt/hadoop/hadoop-3.1.4.tar.gz -C /usr/local/hadoop --strip 1`
    - `sudo mkdir -p /mnt/sdb1/data/hdfsnamenode`
    - `sudo mkdir -p /mnt/sdc1/data/hdfsdatanode`
    - `sudo mkdir -p /mnt/sdb1/logs`
- Create symbolic links for directories (to opt)
    - `sudo ln -s /mnt/sdb1/data/hdfsnamenode /opt/hadoop/namenode`
    - `sudo ln -s /mnt/sdc1/data/hdfsdatanode /opt/hadoop/datanode`
    - `sudo ln -s /mnt/sdb1/logs /opt/hadoop/logs`

**Spark Installation**
- Install Spark
    - `sudo apt install scala`
    - `cd ~/Downloads`
    - `wget https://downloads.apache.org/spark/spark-2.4.7/spark-2.4.7-bin-without-hadoop.tgz`
    - `tar xvf spark-2.4.7-bin-without-hadoop.tgz`
    - `sudo mv spark-2.4.7-bin-without-hadoop /opt/spark`
- Configure
    - `sudo cp /opt/spark/conf/spark-env.sh.template /opt/spark/conf/spark-env.sh`
    - `sudo vim /opt/spark/conf/spark-env.sh`
    - Add the following line: `export SPARK_DIST_CLASSPATH=$(/usr/local/hadoop/bin/hadoop classpath)`
- Try running Spark
- Run spark `start-all.sh` use full path if needed `
- To stop `stop-all.sh`
- Can alternatively use `start-master.sh` `stop-master.sh`
- To view the Spark Web user interface, open a web browser and enter the localhost IP address on port 8081. `http://192.168.56.101:8081/`
- Port may be 8080 or 8082 if 8081 does not work

**Install YARN**
- `sudo apt install npm`
- `sudo npm install --global yarn`

**Matlab Runtime Installation**
- `sudo mkdir /opt/matlab`
- `sudo chown vtsil:vtsil -R /opt/matlab`
- Downloaded at: https://ssd.mathworks.com/supportfiles/downloads/R2020b/Release/2/deployment_files/installer/complete/glnxa64/MATLAB_Runtime_R2020b_Update_2_glnxa64.zip via Firefox
- `cd ~/Downloads`
- `unzip MATLAB_Runtime_R2020b_Update_2_glnxa64.zip`
- `sudo mkdir matlab`
- Move all contents into matlab folder `sudo mv filename`
- `./install -mode silent -agreeToLicense yes`

**Docker Installation**
- `sudo apt install apt-transport-https ca-certificates curl software-properties-common`
- `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`
- `sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"`
- `sudo apt update -y`
- `apt-cache policy docker-ce`
- `sudo apt install -y docker-ce`
- `sudo usermod -aG docker ${USER}`
- `sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose`
- `sudo chmod +x /usr/local/bin/docker-compose`

**Ansible Set-up**
- Install ansible 
    - `python -m pip install --user ansible`
    - OR
    - `sudo apt update`
    - `sudo apt install software-properties-common`
    - `sudo apt-add-repository --yes --update ppa:ansible/ansible`
    - `sudo apt install ansible`
- Set-up ansible hosts
    - Set up groups with appropriate ip addresses
    - Set auto_legacy_silent to let ansible run older Python version without constant depreciation warnings
    - `sudo vim /etc/ansible/hosts`
        ```
        [webservers]
        10.0.2.4
        10.0.2.5
        10.0.2.6

        [webservers:vars]
        ansible_python_interpreter=auto_legacy_silent

        [local]
        10.0.2.4

        [local:vars]
        ansible_python_interpreter=auto_legacy_silent
        ```
    - Be sure each host (including node you are running from) has ssh key added to node you are running from
        - `cat ~/.ssh/id_rsa.pub`
        - `sudo vim ~/.ssh/authorized_keys`
- Copy ansible files from previous server to new server
    - `scp -r user@ssh.example.com:/path/to/remote/source /path/to/local/destination`
    - Can do with entire directory at once
- To run playboook: `ansible-playbook /path/to/playbookname.yml`
- Start services playbooks: `sudo vim ~/housekeeping/ansible_playbooks/start_services.yml`
    ```
    ---
    - hosts: webservers
    become: true
    become_method: sudo
    tasks:
        - name: Start Zookeeper
            shell: systemctl restart zookeeper
        - name: Start Cassandra
            shell: systemctl restart cassandra
        - name: Start Kafka
            shell: systemctl restart kafka

    - hosts: local
    become_method: sudo
    tasks:
        - name: Start webserver
            become: true
            shell: systemctl restart vtsilwebserver
    ```
- Stop services playbook: `sudo vim ~/housekeeping/ansible_playbooks/stop_services.yml`
    ```
    ---
    - hosts: local
    become_method: sudo
    tasks:
            - name: Stop webserver
                become: true
                shell: systemctl stop vtsilwebserver

    - hosts: webservers
    become: true
    become_method: sudo
    tasks:
        - name: Stop Cassandra
            shell: systemctl stop cassandra
        - name: Stop Kafka
            shell: systemctl stop kafka
        - name: Stop Zookeeper
            shell: systemctl stop zookeeper
    ```
**Set-up virtual environment**
- Allows you to manage python installations seperately for each package
- Install packages
    - `mkvirtualenv flask-app`
    - `pip install wheel`
    - `pip install uwsgi flask`
    - `pip install pytz`
    - `pip install pyyaml`
    - `pip install virtualenv`
    - `pip install virtualenvwrapper`
    - `pip install scipy`
    - `pip install cassandra-driver`
    - `sudo apt -y install xserver-xorg`
    - `pip install confluent_kafka`
    - `pip install h5py`
    - `pip install numpy`

- Activate/ deactivate environment
    - Once you make an environment it will activate
    - To deactivate `deactivate`
    - To reactivate `workon flask-app`
    - To see list of available environments `~/.virtualenvs`
    - You can confirm you’re in the virtual environment by checking the location of your Python interpreter, it should point to the env directory. `which python`

**Set up vtsilwebserver service**
- Copy all files over that run the service
- `git clone urltocode` OR `scp -r user@ssh.example.com:/path/to/remote/source /path/to/local/destination`
- `sudo vim /etc/systemd/system/vtsilwebserver.service`
- Used by a systemd utility called systemctl to start and stop our web application. 
- This file contains the location of the root folder and instructions on how to start the application
    ```
    [Unit]
    Description=uWSGI instance to serve vtsil web server
    After=network.target

    [Service]
    User=vtsil
    Group=www-data
    WorkingDirectory=/home/vtsil/vtsil-data-viz-api
    Environment="PATH=/home/vtsil/.virtualenvs/flask-app/bin"
    ExecStart=/home/vtsil/.virtualenvs/flask-app/bin/uwsgi --ini vtsil-web-server.ini

    [Install]
    WantedBy=multi-user.target
    ```
- Get uWSGI to log errors
    - sudo mkdir /var/log/uwsgi
    - Edit .ini file `sudo vim vtsil-web-server.ini`
    ```
    [uwsgi]
    module = wsgi:application

    master = true
    lazy-apps = true
    processes = 5
    enable-threads = true

    socket = vtsilwebserver.sock
    chmod-socket = 660
    vaccum = true
    logto = /var/log/uwsgi/%n.log

    die-on-term = true
    env = APP_ENV=production
    ```

**NGINX Set-up**
- `sudo apt install -y nginx`
-  Add site configuration file 
    - `sudo vim /etc/nginx/sites-available/vtsilwebserver`
        ```
        server {
        listen 80;
        server_name localhost;

        location / {
            include uwsgi_params;
            proxy_read_timeout 30s;
            proxy_send_timeout 30s;
            uwsgi_pass unix:/home/vtsil/vtsil-data-viz-api/vtsilwebserver.sock;
            uwsgi_buffering off;
            proxy_buffering off;
            sendfile        on;
            client_max_body_size 20M;
            keepalive_timeout  0;
        }

        }
        ```
- Add symbolic link to site `sudo ln -s /etc/nginx/sites-available/vtsilwebserver /etc/nginx/sites-enabled`
- Comment out default sites file so it won't supercede vstil site `sudo vim /etc/nginx/sites-available/default`
- `sudo chown vtsil:vtsil -R /etc/nginx`
- Reload nginx to apply changes `nginx -s reload` `sudo systemctl restart nginx`
- Leave regular configuration file with default settings `/etc/nginx/nginx.conf`
- To troubleshoot view logs `sudo vim /var/log/nginx/access.log`

**Edit .bashrc to reflect all settings changes**
- Apphend the following to the bottom `sudo vim ~/.bashrc`
    ```
    export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre/
    export PATH=$PATH:$JAVA_HOME/bin
    export WORKON_HOME=$HOME/.virtualenvs
    export PROJECT_HOME=$HOME
    export ANSIBLE_LIBRARY=/usr/lib/python2.7/dist-packages/ansible/modules
    export ANSIBLE_MODULE_UTILS=/usr/lib/python2.7/dist-packages/ansible/module_utils
    export SPARK_HOME=/opt/spark
    export PATH=$PATH:$SPARK_HOME/sbin
    export PYSPARK_PYTHON=usr/bin/python
    export HADOOP_HOME=/usr/local/hadoop/
    export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
    source $HOME/.local/bin/virtualenvwrapper.sh
    export PYTHONPATH=/home/vtsil/vtsil-data-viz-api
    export APP_ENV=production
    ```
- Reload file `source ~/.bashrc`
- Adjust paths as necessary to lead to proper directories

**Edit hosts file**
- `sudo vim /etc/hosts`
    ```
    #127.0.0.1       localhost localhost.localdomain
    #127.0.1.1      ubuntu1804.linuxvmimages.local  ubuntu1804

    10.0.2.4       localhost # Only on node0
    10.0.2.5       localhost # Only on node1
    10.0.2.6       localhost # Only on node2
    # Put only proper ip for each node as localhost

    10.0.2.4 node0
    10.0.2.5 node1
    10.0.2.6 node2

    # The following lines are desirable for IPv6 capable hosts
    ::1     ip6-localhost ip6-loopback
    fe00::0 ip6-localnet
    ff00::0 ip6-mcastprefix
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters
    ```
