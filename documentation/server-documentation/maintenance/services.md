# Using Services

## Service versions

Physical server:
- Kafka: 2.1.0
- Scala: 2.11
- Cassandra: 3.4.2



## Kafka

**Basic Terminology**
- https://kafka.apache.org/081/documentation.html#introduction 
- Commands: https://docs.confluent.io/platform/current/clients/confluent-kafka-python/html/
- Topics: Categories in which Kafka feeds messages
- Producers: Processes that publish messages to a Kafka topic.
- Consumers: Processes that subscribe to topics and process the feed of published messages.
- Broker: Each server comprising the cluster Kafka is run on

**Commands**
- Check status of a Kafka consumer group `/opt/kafka/bin/kafka-consumer-groups.sh --bootstrap-server node0:9092 --describe --group psd`
- Describe topics `/opt/kafka/bin/kafka-topics.sh --zookeeper node0:2181 --describe --topic incomingFiles`
- Change topic partition `/opt/kafka/bin/kafka-topics.sh --zookeeper node0:2181 --alter --topic incomingFiles --partitions 3`
- Describe consumers with lag `/opt/kafka/bin/kafka-consumer-groups.sh --bootstrap-server node2:9092 --describe --group psd | awk  '$5 != 0 { print $1}'`
- See list of topics `/opt/kafka/bin/kafka-topics.sh --list --zookeeper localhost:2181`
- Create kafka topics `cat topics.txt | xargs -I % -L1 /opt/kafka/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic`
- See list of files being consumed `/opt/kafka/bin/kafka-console-consumer.sh --topic topicName --from-beginning --bootstrap-server localhost:9092`
- `kafka-consumer-groups.sh --bootstrap-server kafka-host:9092 --group my-group --reset-offsets --to-earliest --all-topics --execute`
- Delete topic `/opt/kafka/bin/kafka-topics.sh --zookeeper localhost:2181 --delete --topic 'rawData.*'`

## Cassandra
**Setting up tables**
- Locations of files
	- `/home/vtsil/housekeeping/cass/create_tables_final.cql`
-  `cqlsh 10.0.2.4 -f /home/vtsil/housekeeping/cass/create_tables_final.cql`
- Populate sensor info into sensor info table by creating a script that uses `sensor_info.json` with `insertSensorInfo(fname)` function from `/home/vtsil/vtsil-data-viz-api/datavizapi/cassandra_operations.py` as shown below
    - Open .json file `python -m json.tool sensor_info.json` (not a required step, just allows you to view contents)
    - `sudo vim load_sensor_info.py`
		```
		import datavizapi.cassandra_operations as db_op
	
		db_op.insertSensorInfo('/home/vtsil/vtsil-data-viz-api/datavizapi/scripts/sensor_info.json')
		```
- Must initiate all tables before trying to populate them with data or query them
- You can only query tables using primary keys used when initializing tables
	- ex. PRIMARY KEY (date, ts, id)
	- ex. `SELECT id,ts,total_power,power_dist FROM vtsil.psd_by_minute where date=2021-05-04 13:00:00+00 and ts=2021-05-04 13:00:07+00`

**Using cqlsh**
- Connect: `cqlsh 10.0.2.4` (use IP address of any node)
- Can’t use cqlsh while vtsil server is running (because server uses cassandra)
- Note: `enter` goes to new line, must finish for semi-colon to run
- Check for keyspace `describe keyspaces`
- Use keyspace `use vtsil`
- Check for tables `describe tables`
- Show full table
	- General format: `SELECT * FROM table_name;`
	- `SELECT * FROM vtsil.psd_by_hour;` 
	- `SELECT * FROM vtsil.sensor_data_by_second`
- Clear table contents
	- Format: `TRUNCATE keyspace.table_name;`
	- Example: `TRUNCATE vtsil.sensor_data_by_second;`
- Delete table
	- `use vtsil;`
	- `drop table if exists table_name;`
- Copy data from one table to another (ex. to change keys)  
	- `COPY vtsil.fft_by_hour(date, hour, id, fft) TO '/home/vtsil/vtsil-data-viz-api/datavizapi/scripts/fft_day2.csv' WITH PAGETIMEOUT=10000;`  
	- `COPY vtsil.fft_by_hour_sort(date, hour, id, fft) FROM '/home/vtsil/vtsil-data-viz-api/datavizapi/scripts/fft_day2.csv' WITH PAGETIMEOUT=10000;`

## Reset tables and data processing
- Delete kafka topics `/opt/kafka/bin/kafka-topics.sh --zookeeper localhost:2181 --delete --topic 'rawData.*'`
- Reset kafka `sudo systemctl restart kafka`
- Remove all files
	- `sudo rm /home/vtsil/vtsil-data-viz-api/datavizapi/scripts/*.h5`
	- `sudo rm /home/vtsil/vtsil-data-viz-api/datavizapi/scripts/*.txt`
	- `rm /home/vtsil/vtsil-data-viz-api/datavizapi/scripts/.lastread`
	- `sudo rm /home/vtsil/testfiles/*.h5`
- Reset Cassandra
	- `cqlsh 10.0.2.4`
	- `TRUNCATE vtsil.sensor_data_by_second;`
	- `TRUNCATE vtsil.psd_by_minute;`
- To start data collection from a specific point
	- `sudo vim /home/vtsil/vtsil-data-viz-api/datavizapi/scripts/.lastread`
	- `2021-05-04_11_01_07`
	- `YYYY-MM-DD_HH_mm_ss`
