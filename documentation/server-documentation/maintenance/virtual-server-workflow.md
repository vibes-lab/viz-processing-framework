# Standard Workflow for Virtual Server

Using Git Bash on Windows 10

## Simplified Steps
**Starting Webserver**
- Start all VMS `startnodes`
- Login to cluster `ssh vs`
- Start virtual environment `workon flask-app`
- Start services via ansible `ansible-playbook -K ~/housekeeping/ansible_playbooks/start_services.yml`
    - Use `-K` when password is required for sudo on any servers
- Start server `sudo systemctl restart vtsilwebserver`
- Enter the IP address of the head node in a browser on the same machine where the virtual server is running `http://<IP Address>/`

**Stopping Webserver**
- Stop server `sudo systemctl stop vtsilwebserver` 
- Stop services `ansible-playbook ~/housekeeping/ansible_playbooks/stop_services.yml`
- Get out of virtual environment `deactivate`
- Leave virtual server `exit`
- Stop virtual server `stopnodes`

## Detailed Steps

**Starting Machines**
- Manual 
    - Start`"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" startvm <VM-name> --type headless`
    - Stop `"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" controlvm <VM-name> acpipowerbutton`
    - Other commands `"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" controlvm <VM-name> <command>`
    - Commands:
        - `savestate`
        - `resume`
        - `poweroff` (pulling cord)
        - `acpipowerbutton` (power button)
        - `pause`
- Using alias
    - `vim ~/.bashrc`
        ```
        alias startnode1='"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" startvm server_vs-node1-head --type headless'
        alias startnode2='"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" startvm server_vs-node2 --type headless'
        alias startnode3='"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" startvm server_vs-node3 --type headless'

        alias stopnode1='"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" controlvm server_vs-node1-head acpipowerbutton'
        alias stopnode2='"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" controlvm server_vs-node2 acpipowerbutton'
        alias stopnode3='"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" controlvm server_vs-node3 acpipowerbutton'

        alias startnodes='"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" startvm server_vs-node1-head --type headless | "C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" startvm server_vs-node2 --type headless | "C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" startvm server_vs-node3 --type headless'
        alias stopnodes='"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" controlvm server_vs-node1-head acpipowerbutton | "C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" controlvm server_vs-node2 acpipowerbutton | "C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" controlvm server_vs-node3 acpipowerbutton'
        ```
**Logging into server**
- `ssh vtsil@<IP Address>`
- Reccomended to use shortcuts
    - `vim ~/.ssh/config`
        ```
        Host vs
            Hostname <IP Address>
            User vtsil
        ```
    - `ssh vs`
