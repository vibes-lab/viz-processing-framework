# Data Acquisition

Reference the [Goodwin Analysis](https://code.vt.edu/vibes-lab/goodwin-analysis) repository for additional documentation on data collection.

## Preventing/ solving acquisition issues
**Hardware issues:**
- Preventative: Keep firmware updated
- If DAQ stops working
    - Reboot using LXI
    - Manual power cycle by going to the building
    - LAN reset
    - Submit RMA

**Creating new folders:**
- It is best to create a new folder anytime the sampling rate is changed for a new acqusiiton
    - Streaming code uses sampling rate from the most recent configuration file in the folder where data is being saved
        - The data will not be added into Cassandra correctly if an incorrect sampling rate is used
- Number of files in the folder is very influential when doing second level saves
    - Create a new folder daily

**Changing acquisition settings:**
- For record lengths of 10 seconds or less, use `C:\Users\vtsil\Documents\Code\VTSIL_Data_Collection_Revised\Data_Acquisition_Code\x64\Release`
- For greater than 10 seconds use `C:\Users\vtsil\Documents\Code\VTSIL_Data_Collection\Data_Acquisition_Code\x64\Release`
- Must adjust the correct configuration file (located in the same folder) based on which acquisition file is being used

**Issues encountered:**
- Files not being written fast enough
    - Issue: Previous file saves further into next acquisition with each save until eventually a file is skipped
    - Solution: Reduce sleep time in `DAQ_Interface.cpp` `writeDataToFile()` function. Time was previously as long/ longer than the desired acquisition period
- HDF5 saving error
    - Issue: HDF5 file saving issue `H5D_chuck_construct()`
    - Solution: Change `CHUNK_SIZE` specified in `library.cpp`. 
        - The following inequaltity MUST be satisfied: 
        Sample Rate (Hz) x Acqusiition Length (s) >= CHUNK_SIZE x CHUNK_SIZE. 
- Incorrect timestamp
    - Issue: Timestamp in filename does not match up with data acquisiiton start
    - Solution: 
        - Timestamp time comes from the IEEE-1588 clock which counts the number of seconds since the Unix Epoch (Jan 1 1970). 
        - When setting the time using time.gov, one of the clocks considers leap seconds while the other does not, causing an offset equal to the current number of leap seconds (37-38). 
        - You must set the IEEE 1588 clock to be correct rather than making the Local Time correct
    - Note: How to check file creation time in seconds in Windows
        - Open powershell
        ```
        cd D:\OMA\20210722_256Hz
        $file = Get-Item 2021-07-22_08_56_18.h5
        $file.CreationTime
        ```
- Save times increase over time
    - Issue: As data acqusition runs for long periods the time taken to save a file increases significantly
    - Solution: Restart acquisition automatically each day. See automated data collection taks
- Directory becomes too full with files
    - Issue: Performance gets worse when a single directory becomes too full, and files are continuously being saved to the same folder.
    - Solution: Create new folder automatically each week. See automated data collection tasks.

**The following considerations should be made when selecting a CHUNK_SIZE:**
- Number of chunks MUST be equal or greater than 1  
    - Number of chunks = (Sample Rate (Hz) x Acqusiition Length (s))/(CHUNK_SIZE x CHUNK_SIZE)
- Chunks should not be too small
    - "There is a certain amount of overhead associated with finding chunks. When chunks are made smaller, there are more of them in the dataset. When performing I/O on a dataset, if there are many chunks in the selection, it will take extra time to look up each chunk. In addition, since the chunks are stored independently, more chunks results in more I/O operations, further compounding the issue. The extra metadata needed to locate the chunks also causes the file size to increase as chunks are made smaller. Making chunks larger results in fewer chunk lookups, smaller file size, and fewer I/O operations in most cases."
- Chunks should not be too large
    - "It may be tempting to simply set the chunk size to be the same as the dataset size in order to enable compression on a contiguous dataset. However, this can have unintended consequences. Because the entire chunk must be read from disk and decompressed before performing any operations, this will impose a great performance penalty when operating on a small subset of the dataset if the cache is not large enough to hold the one-chunk dataset. In addition, if the dataset is large enough, since the entire chunk must be held in memory while compressing and decompressing, the operation could cause the operating system to page memory to disk, slowing down the entire system.”
    - If your selections are always the same size (or multiples of the same size), and start at multiples of that size, then the chunk size should be set to the selection size, or an integer divisor of it.

## Automated Data Collection tasks

**Code to restart acqusiition manually:**
- Run at 4am every day by Windows Task Scheduler
- `restartDataAcquisition.bat`
    ```
    ECHO off

    ::Batch file for building data acquisition. Double click to run. Edit VT_SIL_DAQ_Interface.exe line inputs to change DAQs. 
    ::If DAQ initialization takes a long time, run the LXI Discovery tool (Desktop) before running this file.

    :: Specify which version of the data acquisition code to use by uncommenting proper line

    ::cd C:\Users\vtsil\Documents\Code\VTSIL_Data_Collection\Data_Acquisition_Code\x64\Release
    cd C:\Users\vtsil\Documents\Code\VTSIL_Data_Collection_Revised\Data_Acquisition_Code\x64\Release

    :: Remove output files that are over 7 days old

    ECHO off
    ECHO Removing following output files that are over 7 days old:
    forfiles /p "C:\Users\vtsil\Documents\Code\Output" /s /m *.* /D -7 /c "cmd /c echo @file"
    ECHO off 

    forfiles /p "C:\Users\vtsil\Documents\Code\Output" /s /m *.* /D -7 /c "cmd /c del @file"

    :: Create filename corresponding to acquisition start time/ date

    For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set mydate=%%c-%%a-%%b)
    For /f "tokens=1-2 delims=/:" %%a in ("%TIME%") do (set mytime=%%a%%b)

    set save_name=C:\Users\vtsil\Documents\Code\Output\%mydate%_%mytime%_out.txt

    :: Kill any acqusiition currently running

    ECHO off
    ECHO.
    ECHO Stopping current data acquisition
    ECHO off

    taskkill /F /IM VT_SIL_DAQ_Interface.exe

    :: Start the new acquisition

    ECHO off
    ECHO Starting new acquisition. 
    ECHO Reference %save_name% to see acquisition progress.
    ECHO off

    :: Remove > %save_name% to output to cmd window instead of file
    :: Use the lines below to run daqs individually (uncomment only one at a time)

    ::VT_SIL_DAQ_Interface.exe "DAQ-4W.local" > %save_name%

    ::VT_SIL_DAQ_Interface.exe "DAQ-4E.local" > %save_name%

    ::VT_SIL_DAQ_Interface.exe "DAQ-1W.local" > %save_name%

    ::VT_SIL_DAQ_Interface.exe "DAQ-3E.local" > %save_name%

    ::VT_SIL_DAQ_Interface.exe "DAQ-1E.local" > %save_name%

    :: Use the code below to run all/multiple daqs

    VT_SIL_DAQ_Interface.exe "DAQ-3E.local|DAQ-1W.local|DAQ-1E.local" > %save_name%

    ::VT_SIL_DAQ_Interface.exe "DAQ-4W.local|DAQ-4E.local|DAQ-3E.local|DAQ-1W.local|DAQ-1E.local" > %save_name%

    ::VT_SIL_DAQ_Interface.exe "DAQ-1W.local|DAQ-3E.local|DAQ-4E.local|DAQ-4W.local" > %save_name%

    ::VT_SIL_DAQ_Interface.exe "DAQ-1W.local|DAQ-4E.local|DAQ-4W.local" > %save_name%

    ::VT_SIL_DAQ_Interface.exe "DAQ-1W.local|DAQ-1E.local" > %save_name%
    ```

Code to create new move directory:
- Run once a week at 3:59am
- `editConfig.bat`
    ```
    ECHO OFF

    ::Batch file for editing configuration file. Double click to run.

    :: File will copy the current daq_config.txt file except for changing the move directory to a file with the current date and the sampling rate of the current configuration (ex. 20210818_256Hz)

    :: Uncomment the appropriate line of code below to edit the configuration file for the desired code version

    cd C:\Users\vtsil\Documents\Code\VTSIL_Data_Collection_Revised\Data_Acquisition_Code\x64\Release

    ::cd C:\Users\vtsil\Documents\Code\VTSIL_Data_Collection\Data_Acquisition_Code\x64\Release

    :: Extract previous directory and sample rate from daq_config.txt file

    set file_in=daq_config.txt

    setlocal enabledelayedexpansion

    set count=0

    for /f "tokens=*" %%x in (%file_in%) do (
        set /a count+=1
        set var[!count!]=%%x
    )

    set prev_dir=%var[15]%
    set sample_rate=%var[12]:~2%

    :: Set new directory name

    set YYYYMMDD=%DATE:~10,4%%DATE:~4,2%%DATE:~7,2%

    set save_dir=D:\\OMA\\%YYYYMMDD%_%sample_rate%Hz\\

    :: Replace the directory name in the config file

    setlocal enableextensions disabledelayedexpansion

    for /f "tokens=*" %%l in ('type "%file_in%"^&cd.^>"%file_in%"'
        ) do for /f "tokens=1 delims== " %%a in ("%%~l"
        ) do if /i "%%~a"=="%prev_dir%" (
            >>"%file_in%" echo(4;%save_dir%
        ) else (
        >>"%file_in%" echo(%%l
        )

    type "%file_in%"

    endlocal

    :: Create new folder to save into

    mkdir %save_dir%
    ```

## Code Overview

Original code written by Chreston Miller

** Note this is my interpretation of code that I did not write

- Use e to elegantly close the program (write out anything in the buffer before closing)

**Location of code**
- Copy: `C:\Users\vtsil\Documents\Code\VTSIL_Data_Collection_ Revised\Data_Acquisition_Code\VT_SIL_DAQ_Interface`
- Original: `C:\Users\vtsil\Documents\Code\VTSIL_Data_Collection\Data_Acquisition_Code\VT_SIL_DAQ_Interface`
- Open copy - not original code

**General format**
- .h (header files) - explain general purpose of file
- .cpp - full code to execute purpose


**config_parser**
- Defines the reading of the daq_config file and assignment of config variables
- parseConfigFileStr()
    - Reads daq_config and assign variables
        - sampling rate
        - seconds per file
        - block size (samples per channel)
        - save path
        - store all channels (t/f)
        - check records on daq (t/f) 
    - Sets default values if config file cannot be read
    - setters and getters
    - Define functions that return values for each of the variables in the config file

**CSV_Output**
- Outdated (no longer used)
- Would store the data in CSV format instead of HDF5

**DAQ**
- Defines the data collection operations (anything involving the DAQs)
- DAQ()
    - Create new instance of VTEXDsa driver
    - Initialize variables
- close()
    - Cleanly close driver instance and clear up memory
- initialize()
    - Initialize the driver
    - Uses System as ReferenceOscillator and TimestampSource
    - Performs LAN Synchronization
    - Set daq driver parameters based on what is in the config file 
    - get the number of physical channels
    - Enable streaming
    - Begin getting measurements
- getActiveChannels()
    - Get the active channels
    - Check for data
    - Convert channel data from BSTR to string to vector
    - Check which channels have sensors
- continuousRawData()
    - Collect raw, unprocessed data from the DAQ
    - Option to exit gracefully
        - Use “e” key
        - Writes out all data in buffer before closing
    - Reads out data from buffer
        - Timeout of 1000ms
        - Process 1 record at a time
    - Check for available records on buffer
    - Counts up for each record received before write to disk
        - Will be the number of seconds specified bc one record is collected per second
        - Prints progress
    - Check if buffer has filled up
        - Write to disk when full
        - If buffer is filling up print that
    - Print errors if exceptions occur
- appendToData()
    - Stores the new sensor data
    - Trips flag if bufferSize is reached
- writeDataToGlobal()
    - Iterate through each sensor and pull out the data
    - Uses safe arrays to access data
    - grab timestamp only from first sensor
        - ::SafeArrayGetElement(currentSensorData->getTimeSeconds(), (LONG *)&timeIndex, timeStamp);
        - Get from first sensor (index=0)
    - Can iterate over all channels or over only those with sensor
        - User specifies in daq_config file
    - Iterate over all available records
        - Use record size (time*sample rate) to determine how many records to read
    - stores all data in totalSensorData
- clearBuffer()
    - clears the buffer for each channel
    - clean up memory for objects and vectors
    - clears sensor data

**DAQ_Interface**
- Runs the DAQ operations and HDF5 operations
- DAQ_Interface()
    - Constructor
        - Initializes the DAQ interface
    - Deconstructor
        - Closes the session and releases memory
- loadConfigureFile()
    - Runs functions from the config_parser to set the appropriate variables with the info from daq_config.txt
    - Gets timestamp for config file
    - Adds info to config file for acquisition and saves
- daqBootStrap()
    - Loads data from acquisition config file
    - Initialize all the daqs
    - Allocate memory size
- initialize()
    - Creates and initializes new DAQ object
    - Gets active channels
- collectDAQData()
    - Collect data
    - Write data to global variable
    - Output that the data collection is finished
    - Clear the buffer
- writeDataToFile()
    - Writes DAQ data to file
    - Outputs that Write Data is Finished
- close()
    - Clears memory and variables

**HDF5_Output**
- Defines the operations for writing the data to HDF5 format
- getMoveDir()
    - return the move directory specified in config file
- setMoveDir()
    - Assign the move directory specified in config file
- writeDaqDataToFile()
    - Get start time of write
    - Output current time in seconds since Unix epoch (Jan 1 1970)
    - Uses timestamp from DAQ (IEEE-1588 time)
    - Set current timestamp as filename for file
    - Uses timestamp from above and converts to format
    - Creates file name using timestamp
    - Create new HDF5 file
        - Uses default property lists
        - Creates /Data group
        - Uses H5F_ACC_TRUNC
    - Sets dimensions
        - Row dimension = Square root of total data collected
        - Column dimension = total data collected/ row dimension
    - Sets chunk size as specified chunk size x specified chunk size
    - Sets up dataset
    - Writes data to dataset
        - Sets DAQ name for each set of data
    - Outputs time taken to save
- Converts raw data into HDF5 files
- Currently used over CSV

**library**
- Defines basic operations used by other functions and defines some constants
- currentTimeSeconds()
    - const double
    - Measures seconds since base time
        - Jan 1, 2015
    - Base time
        - y2k.tm_hour (hours in UTC)
        - y2k.tm_min (minutes)
        - y2k.tm_sec (seconds)
        - y2k.tm_year (Years starting from 1900)
            - ex. y2k.tm_year = 121 // 2021
        - y2k.tm_mon (month with January=0)
        - y2k.tm_mday (day)
- currentDateTime()
    - const string
    - Get current date/time, format is YYYY-MM-DD.HH:mm:ss
- startTimer()
    - start global timer for timing how long operations take
- endTimer()
    - end global timer for timing how long operations take
- to_bool(string const& s)
    - Converts string to boolean value

**main**
- Runs the other scripts (this is basically what is called by the .exe file)
- Runs toStarts new DAQ interface
- Runs daqBootStrap
- Format for input: "DAQ-4W.local|DAQ-4E.local|DAQ-3E.local|DAQ-1W.local|DAQ-1E.local"

**sensor_data**
- Defines data operations to make them easier to use in the other scripts
- Purpose of this class is to bundle the data returned from the DAQ for easy organization and management
- Defines parameters for SAFEARRAY
- Implements setters and getters for SAFEARRAY parameters

## Code graphical output:

config_parser.cpp parseConfigFileStr():
```
Opening configuration file: <fileNameStr>

File stream for config file is good --> opened successfully. Continuing with loading parameters from config file...
```
DAQ_Interface loadConfigureFile():
```
Parameters from the config file:
Clock Frequency: <clockFrequency>
Prescaler: <prescaler>
Sample Rate: <sampleRate>
Length of file (in seconds): < bufferSize>
Move Path:  <movePath>
Store all sensor data: < ALL_CHANNEL_DATA>
Check records on DAQs: < CHECK_RECORDS_ON_DAQ>
```
DAQ.cpp initialize():
```
LAN Synchronization Enabled

 ========================================================================
System set-up for DAQ: <daqID>
Clock Frequency: <clockFreq>
Prescaler: <preScaler>
Actual sampling rate:  <sampleRate> samples a second
Record size: <recordSize>
Number of physical channels: <numPhysicalChannels>
Sampling every <recordSize/sampleRate> of a second ==>
Record size / sampling rate = <recordSize>/<sampleRate> = <recordSize/sampleRate> of a second
Hence, recording Sample rate * number of channels = <sampleRate*numPhysicalChannels> samples every second

========================================================================
```
DAQ.cpp getActiveChannels():
```
<daqID>:
Number of channels without sensors: <numChannelWithNoSensor>

========================================================================

Collection for <bufferSize> seconds:
Progress (in seconds):

<numCollectedRecords>, ...

<daqID>: Current number of records on DAQ to pull: <driver->StreamingData->NumMemoryRecords>
```
DAQ_Interface.cpp collectDAQData():
```
Collect Data Finished
```
HDF5_Output writeDaqDataToFile():
```
Time to iterate over data and write sensor data into HDF5 file: <duration> s
```
DAQ_Interface.cpp writeDataToFile():
```
Write Data Finished
```
DAQ_Interface.cpp DAQ_Interface() deconstructor:
```
Closing collection…
```
