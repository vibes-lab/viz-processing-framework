# General Notes
- Make sure to be in virtualenv when running python/ starting webserver
    - `workon flask-app`
- Always try restarting services before investigating an error with services

# Troubleshooting Tips
- Read/ annotate the code when trying to identify a problem
- Read documentation before attempting to use services, do brief tutorials
- Add print statements to python code at each important step (console.log in js code)
- Make sure anything being referenced in the code actually exists
    - Go through code line-by-line and locate what it is referencing
- Search for text/ files that you are looking for
    - To search for type of file (extension): `find . -type f -name "*.type"`
    - To search for file that contains certain text `sudo grep -Rl "texttolookfor" /directory/to/start/with`

# FTP Troubleshooting
- Don't run FTP operations without configuration file (from data acquisition) present in the FTP folder
- Make sure to enable connections to FTP server through Firewall before attempting file_sync and disable when not using
- Be sure date in .lastread file is earlier than files you are trying to sync
- Error: `ValueError: time data 'te' does not match format '%Y-%m-%d_%H_%M_%S' in file_sync.py`
- Solution: Remove any extra folders within the FTP remote folder

# Logging
- Setting up logging
    - Create folder `sudo mkdir /var/log/uwsgi`
    - Change owner `sudo chown -R vtsil:vtsil uwsgi`
    - Enable in .ini file
    - `sudo vim /home/vtsil/vtsil-data-viz-api/vtsil-web-server.ini`
    - Add the following line `logto = /var/log/uwsgi/%n.log`
- Interacting with logs
    - View logs using `sudo vim /var/log/uwsgi/vtsil-web-server.log`
    - New logs generated whenevr server is running
    - To reset long log, just remove or rename and new file will be generated
    - Remove: `sudo rm /var/log/uwsgi/vtsil-web-server.log`
    - Rename: `sudo mv /var/log/uwsgi/vtsil-web-server.log /var/log/uwsgi/new_name.log`
    - Restart server: `sudo systemctl restart vtsilwebserver.service`
- Printing to logs
    - Add following lines to app code
    - `sudo vim /home/vtsil/vtsil-data-viz-api/app.py`
         ```
        from __future__ import print_function
        import sys
        ```
    - Use standard print statements print()

# JavaScript Troubleshooting
- May need to empty cache if changes made to HTML/ JS are not being applied
- `Open Inspect > Right Click Refesh Button > Empty cache and hard reload`

# HTML Troubleshooting
- Adding comments
- `<!-- Write your comments here -->`
- Be sure that imported libraries don't have naming conflcts

# Chrome DevTools
- See tutorial: https://developer.chrome.com/docs/devtools/javascript/ 
- Use source tab for debugging Javascript
- Use elements tab for HTML
- Identifying problems
    - Use toolbar on the right while in Sources tab
    - Breakpoints
        - Add event listener breakpoints
            - Can pause on mouse click or another event
            - Resume til paused on correct line
        - Can click line number of source code to stop on that line
    - Scope pane shows variable definitions
        - Check that variables have the values expected
    - Add expressions to watch using watch pane
        - Watch the type of a certain variable `typeof variable_name`

# Error messages solved
`Connection error: ('Unable to connect to any servers', {'10.0.2.4': error(None, "Tried connecting to [('10.0.2.4', 9042)]. Last error: timed out")})`
- Possible reasons
    - Cassandra is not properly running
    - Try using cqlsh from a different server

# General Troubleshooting

**Application**  
Javascript  
- Error: `Uncaught TypeError: d3.select is not a function at dashboard.js:41`
    - Issue: Variable being loaded does not have the correct function
    - Solution: Two libraries were being named d3, so the second was overriding. One had to be renamed. See code changes for details.
- Error: `Uncaught (in promise) Error: 500 INTERNAL SERVER ERROR at c (d3-fetch.v1.min.js:formatted:37)`
    - Issue: Something in the python code is not working

Python
- Checking time for code to run
- Make sure to include parenthesis in functions
    ```
    from datetime import datetime, timedelta

    t0=datetime.now()
    function
    t1=datetime.now()
    print((t1 - t0).total_seconds())
    ```

- Error 
    ```
    File "./app.py", line 153, in streaming
        {'ts': results[sid_list[0]][0]['ts'],
    IndexError: list index out of range
    ```
    - Issue: Query to Cassandra is not valid
    - Solution: Had to query specific second that the minute level data was saving to (ex. 2021-05-04 09:00:07 instead of 2021-05-04 09:00:00)
    - When you run into query errors:
        - Try querying directly using cqlsh to see if data exists there (vtsil.sensor_data_by_minute and vtsil.psd_by_minute)
        - Try timezone offsets when querying or use +00 for UTC
        - Add print statements to code and check the time at each step
- Error `ImportError: No module named datavizapi`
    - Path does not include the appropriate files
    - Add parent directory that contains all desired files to .bashrc (See python file paths below)
        `export PYTHONPATH=/home/vtsil/vtsil-data-viz-api`

- Error: Files are not saved correctly: file signature not found
    - Files are corrupt
    - May be trying to access/ download a file before it is written fully
    - Add time.sleep(# of seconds) to code

**Zookeeper**
- Ensure /etc/host file is correct `sudo vim /etc/host`
- Match files with install/ set-up instructions `sudo vim /opt/zookeeper/conf/zoo.cfg`

**Kafka**
- If data folders are incorrect:
    - Back-up, copy, and rename
    - `sudo mkdir /mnt/sdc1/kafka`
    - `sudo cp -r /opt/kafka/data /mnt/sdc1/kafka/logs`
    - `sudo mv /opt/kafka/data /opt/kafka/data_backup`
    - `sudo chown -R vtsil:vtsil /mnt/sdc1/kafka`
    - `sudo ln -s /mnt/sdc1/kafka/logs /opt/kafka/data`
- If you cannot connect to broker
    - Example errors:
        ```
        ERROR [Broker id=0] Received LeaderAndIsrRequest with correlation id 1 from controller 2 epoch 80 for partition rawData_258-10 (last update controller epoch 80) but cannot become follower since the new leader -1 is unavailable. (state.change.logger)

        WARN [Consumer clientId=consumer-1, groupId=console-consumer-7499] Connection to node -1 (localhost/127.0.0.1:9092) could not be established. Broker may not be available. (org.apache.kafka.clients.NetworkClient)
^CProcessed a total of 0 messages
        ```
    - Changed /etc/hosts file to remove 127.0.0.1 because all nodes have same IP. Assigned localhost to explicit IP
    - See code changes documentation
    - Restart service
    - Try using reset kafka ansible file


**Python**
- Path issues
    - Check active path
        ```
        python
        import sys
        sys.path
        ctrl-d
        ```
    - Edit ~/.bashrc to add paths `sudo vim ~/.bashrc`
        - `export PYTHONPATH=/home/vtsil/vtsil-data-viz-api`
- Outdated .pyc files can cause issues

**Installations**
- sudo apt not working
    - `sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys <Paste key from error>`
