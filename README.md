# Real-Time Processing and Visualization of High-Volume Smart Infrastructure Data Using Open-Source Technologies

Natasha Vipond  (vipondn@vt.edu)  
Virginia Tech

Rodrigo Sarlo (sarlo@vt.edu)  
Virginia Tech

Zhiwu Xie  (zhiwuxie@vt.edu)  
Virginia Tech

May 19, 2022

## Repository Overview:

The code and documentation in this repository is designed to enable the reproduction and/or adaption of a novel open-source framework for processing, storing, and visualizing high-volume data in near real-time. This framework was concieved to address data management challenges that have emerged due to the increasing ubiquity of smart infrastructure and concurrent advances in sensing and communication technologies. As sensors are deployed more widely and higher sampling rates are feasible, managing the massive scale of real-time data collected by these systems has become fundamental to providing relevant and timely information to decision-makers. This framework provides a solution to these data management challenges. 

Examples where this framework could be implemented include smart cities, digital twins, cyber-physical systems, structural health monitoring (SHM) systems, and internet of things (IoT) applications. In the present implementation, the framework is tailored to vibration-based SHM of a 5-story smart building on the campus of Virginia Tech, but the design is intended to be adaptable for other applications. Distributed computing technologies are employed to accelerate the processing, storage, and visualization of data. Vibration data is processed in parallel using a publish-subscribe messaging queue and then inserted into a NoSQL database that stores heterogeneous data across several nodes. A REST-based web application allows interaction with this stored data via customizable visualization interfaces.

This repositiory was developed for the thesis "Real-Time Processing and Visualization of High-Volume Smart Infrastructure Data Using Open-Source Technologies" completed by Natasha Vipond in May 2022. Much of the code in this repositiory is adapted from the work done by Abhinav Kumar for his thesis completed in May 2019 "SensAnalysis: A Big Data Platform for Vibration-Sensor Data Analysis". Reading these related theses may add clarity to the framework design and implementation. They are available through the [Virginia Tech ETD database](https://vtechworks.lib.vt.edu/handle/10919/5534).

### [Documentation](https://code.vt.edu/vibes-lab/viz-processing-framework/-/tree/master/documentation)

The purpose of the documentation is to guide users through the set-up of this framework and to provide the necessary instructions to maintain, adapt, and use the services deployed in the cluster and the web applications they support. If you have access to the VT network, then you can use the code as written by skipping to the "Use" documentation.

- [Server Documentation](https://code.vt.edu/vibes-lab/viz-processing-framework/-/tree/master/documentation/server-documentation)  
    - [Set-up](https://code.vt.edu/vibes-lab/viz-processing-framework/-/tree/master/documentation/server-documentation/setup): Steps outlining the set-up required to begin maintaining and using the framework
        - [Setting up a virtual server to test the framework before implementation on physical machines (optional)](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/server-documentation/setup/virtual-server-configuration.md)
        - [Installing and configuring the required services for the data processing/ visualization server](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/server-documentation/setup/software-installation-configuration.md)
        - [Configuring an FTP server to transfer data between a data acquisition server and the data processing/ visualization server](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/server-documentation/setup/ftp_server_configuration.md)
    - [Maintenance](https://code.vt.edu/vibes-lab/viz-processing-framework/-/tree/master/documentation/server-documentation/maintenance): Instructions for how to deploy each portion of the framework and troubleshoot as neeeded
        - [Acquiring data and troubleshooting data acquisition issues](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/server-documentation/maintenance/data-acquisition.md)
        - [How to use the major services (Kafka and Cassandra)](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/server-documentation/maintenance/services.md)
        - [Tips for login and use of the virtual server](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/server-documentation/maintenance/virtual-server-workflow.md)
        - [Miscellaneous troubleshooting and tips](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/server-documentation/maintenance/troubleshooting.md) 
    - [Use](https://code.vt.edu/vibes-lab/viz-processing-framework/-/tree/master/documentation/server-documentation/use)
        - [Standard workflow to prepare the framework to begin using the applications](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/server-documentation/use/standard-workflow.md)
        - If all set-up is complete and you just want to use the applications:
            1. Ensure you are connected to the Virginia Tech Network via wifi or VPN 
            2. Use a web browser (Chrome, Edge, Firefox, Safari, etc) to navigate to the IP address of the web application found [here](https://docs.google.com/document/d/1PsLSvwPjGgCwYN3Wn7zjgbqAVcwfCrzwz6u3j6kDIjU/edit?usp=sharing)
- [Basic Instructions](https://code.vt.edu/vibes-lab/viz-processing-framework/-/tree/master/documentation/basic-instructions): Basics of how to use the software and languages leveraged within the framework
    - [Command line](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/basic-instructions/command-line-basics.md)
    - [Git](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/basic-instructions/git-basics.md)
    - [Python](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/basic-instructions/python.md)
    - [Javascript](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/basic-instructions/javascript_basics.md)
    - [HTML](https://code.vt.edu/vibes-lab/viz-processing-framework/-/blob/master/documentation/basic-instructions/html_basics.md)


### [Code](https://code.vt.edu/vibes-lab/viz-processing-framework/-/tree/master/code)
- [vtsil-data-viz-api](https://code.vt.edu/vibes-lab/viz-processing-framework/-/tree/master/code/vtsil-data-viz-api)
    - Contains the code needed for the visualization applications developed for the implementation of this framework in Goodwin Hall
- [housekeeping](https://code.vt.edu/vibes-lab/viz-processing-framework/-/tree/master/code/housekeeping)  
    - Contains instructions and relevant code used to set-up new nodes and run processing tasks
