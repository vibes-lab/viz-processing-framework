from __future__ import print_function, division
import sys
from flask import (
    Flask, jsonify, request, after_this_request,
    render_template, make_response)
#import datavizapi.sim_cass_ops as db_op
import datavizapi.cassandra_operations as db_op
from datavizapi.utils.time_utils import (
    editedTime, formatTime, parseTime, currTime, roundToMinute)
import datavizapi.constants as constants
import time
import math
import pytz
import numpy as np
from datetime import datetime, timedelta
from scipy import signal, integrate
from datavizapi import AppConfig
from datetime import datetime, timedelta
from cStringIO import StringIO as IO
import gzip
import functools
import json
from datavizapi.utils import commons
from collections import defaultdict

app = Flask(__name__, static_url_path='', static_folder='web/', template_folder='templates')
config = AppConfig().getConfig()
timezone = constants.APP_TZ
utc_tz=pytz.UTC

# Exports data
@app.route('/api/export')
def dataExport():
    from_ts = request.args.get('f')
    to_ts = request.args.get('t')
    daq_names = request.args.getlist('daqname')
    fname = request.args.get('fname')
    sensor_objs = db_op.getSensorInfoAll()
    sids = []
    for obj in sensor_objs:
        if obj.daq_name in daq_names:
            sids.append(obj.id)
    commons.produceToKafka(
        config['kafka']['file_download']['topic'],
        json.dumps({'fname': fname, 'sids': sids,
                    'from_ts': from_ts, 'to_ts': to_ts}))
    return make_response(jsonify({}))


def gzipped(f):
    """
    taken from: http://flask.pocoo.org/snippets/122/
    """
    @functools.wraps(f)
    def view_func(*args, **kwargs):
        @after_this_request
        def zipper(response):
            accept_encoding = request.headers.get('Accept-Encoding', '')

            if 'gzip' not in accept_encoding.lower():
                return response

            response.direct_passthrough = False

            if (response.status_code < 200 or
                    response.status_code >= 300 or
                    'Content-Encoding' in response.headers):
                return response
            gzip_buffer = IO()
            gzip_file = gzip.GzipFile(mode='wb',
                                      fileobj=gzip_buffer)
            gzip_file.write(response.data)
            gzip_file.close()

            response.data = gzip_buffer.getvalue()
            response.headers['Content-Encoding'] = 'gzip'
            response.headers['Vary'] = 'Accept-Encoding'
            response.headers['Content-Length'] = len(response.data)

            return response

        return f(*args, **kwargs)

    return view_func

# Static images for the home page

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/building')
def building():
    return render_template('building.html')


@app.route('/explore')
def explore():
    return render_template('explore.html')


@app.route('/floorwise')
def floorwise():
    return render_template('floorwise.html')

@app.route('/oma')
def oma_results():
    return render_template('oma.html')

@app.route('/fft')
def fft():
    return render_template('fft.html')

#Editable Parameters for building streaming

STREAMING_INCR = 1 # Number of seconds to include in each query
save_period=1 # Number of seconds of data saved per file
time_incr= 1 # Time to increase by for each new query
cookie_limit=10 # Number of seconds until cookies expire
fail_limit=5 # Number of failed attempts before skipping to next file
time_offset = 20 # Delay in seconds from real-time to start automatic streaming at


# Uses cookie values to set a timestamp when streaming
def getCookieValue(st, start_ts, incr=STREAMING_INCR):
    ct=editedTime(currTime(),seconds=time_offset)
    # If no user input has been given
    if (start_ts is None):
        # If there are no cookies use the current time minus the time_offset
        if (st is None):
            st_parsed=ct
            st = formatTime(st_parsed, timezone, constants.RES_DATE_FORMAT)
        # If there are cookies use the next time increment
        else:
            st_parsed = parseTime(st, timezone, constants.RES_DATE_FORMAT)
            st_parsed = editedTime(st_parsed, seconds=incr)
            st = formatTime(st_parsed, timezone, constants.RES_DATE_FORMAT)
    # If there is a user input but no cookies, use the user input directly
    elif (st is None) or (request.cookies.get('lst')=='0'):
        st = formatTime(start_ts, timezone, constants.RES_DATE_FORMAT)
    # If there is a user input and cookies, use the next time increment
    else:
        st_parsed = parseTime(st, timezone, constants.RES_DATE_FORMAT)
        st_parsed = editedTime(st_parsed, seconds=incr)
        st = formatTime(st_parsed, timezone, constants.RES_DATE_FORMAT)
    return st


# Streams data for the building page
@app.route('/api/stream', methods=('get',))
@gzipped
def streaming(): 
    start_ts = request.args.get('t')

    # Check if user input for start time has been given
    try:
        start_ts=parseTime(start_ts, timezone,constants.RES_DATE_FORMAT)
        lst='1'
    except ValueError:
        start_ts=None
        lst='0'
    # lst variable distinguishes whether this is the first query after the start_ts variable was input by a user

    incr=time_incr
    #incr = int(request.args.get('ns'))

    # If this is the first query or the previous was successful, move to next query
    if (request.cookies.get('fc')=='0') or (request.cookies.get('fc') is None):
        incr=incr
        fc=0
    # If query failed, try again for up to (fail_limit) iterations
    elif (int(request.cookies.get('fc'))<fail_limit):
        incr=0
        fc=int(request.cookies.get('fc'))
    # If query fails (fail_limit) times, skip to next data file and restart attempts
    else:
        incr=save_period
        fc=0
    
    from_d_str = getCookieValue(request.cookies.get('st'), start_ts, incr=incr)
    from_d = parseTime(from_d_str, timezone, constants.RES_DATE_FORMAT)
    to_d = editedTime(from_d, seconds=STREAMING_INCR)

    print('ts: ', from_d)
    
    response = {'v1': [], 'v2': []}

    try:
        results = db_op.fetchLatestPSD(from_d, to_d=to_d)
        sid_list = results.keys()

        # for v1
        value = {}
        for sid, val in results.items():
            if len(val) > 0:
                value[str(sid)] = val[0]['total_power']
        response['v1'].append(
            {'ts': results[sid_list[0]][0]['ts'],
            'value': value})

        # for v2
        for sid, v in results.items():
            if len(v) > 0:
                temp = {"id": str(sid), "data": []}
                num_freq = len(v[0]['power_dist']) / 2
                si = 0
                while si < num_freq:
                    temp['data'].append({"f": float(si), "p": v[0]['power_dist'][si]})
                    si += 1
                response['v2'].append(temp)

        resp = make_response(jsonify(response))
        fc=0
        resp.set_cookie('fc', str(fc), max_age=cookie_limit)

    except IndexError, e:
        resp = make_response(jsonify(response))
        print('ERROR: ', e)
        new_fc=fc+1
        resp.set_cookie('fc', str(new_fc),max_age=cookie_limit)

    resp.set_cookie('lst', lst, max_age=cookie_limit)
    resp.set_cookie('st', from_d_str, max_age=cookie_limit)

    return resp


# Extracts data for sensor hover interaction on floor/ floorwise page
@app.route('/api/psd/<sensor_id>', methods=('get',))
def getSensorPSD(sensor_id):
    ts = request.args.get('d')
    if ts is None:
        return make_response(
            jsonify({'msg': 'error', 'reason': 'invalid/empty timestamp'}))

    from_ts = parseTime(ts, timezone,
                        constants.RES_DATE_FORMAT)
    results = db_op.fetchPSDAsync(
        from_ts, from_ts, get_avg_power=False, get_power_dist=True)
    response = {'data': []}

    for idx, data in enumerate(results[int(sensor_id)][0]['power_dist']):
        response['data'].append([idx, data])
    return make_response(jsonify(response))


# Finds the max and min frequency to include without going beyond the range of valid frequencies
def calcFreqExtent(min_in, max_in, max_all, context):
    # Checks if frequency is below zero
    if (min_in-context>=0):
        min_freq=min_in-context
    else:
        min_freq=0
    # Checks if frequency is above the absolute max
    if (max_in+context<=max_all):
        max_freq=max_in+context
    else:
        max_freq=max_all
    freq=[min_freq, max_freq]
    return freq

# Calculates the avaerage FFT for a specified sensor and time range at a specified frequency resolution 
def calculateFFT(from_ts, to_ts, sid, df):
    try:
        sid_to_data_map=defaultdict(list) # Initialize
        
        raw_data = db_op.fetchSensorByIDAsync(from_ts, to_ts, sid) # Fetch raw sensor data by ID and time range

        # Filter data by sensor ID and reformat into one large list
        for sid, objects in raw_data.items():
            fs = None
            for obj in objects:
                sid_to_data_map[sid].extend(obj['data'])
                if fs is None:
                    fs = len(obj['data'])
            
            # Calculate required samples and windows for desired resolution
            N=len(sid_to_data_map[sid]) # Number of samples available
            dt=1/fs # Time resolution (sec)
            T=1/df # Minimum window period (sec)
            total_t=N/fs # Total amount of time collected over (sec)
            nw=int(math.floor(total_t/T))  # Number of windows possible without overlap (rounded down)
            n=int(math.floor(N/nw)) # Number of samples per window (actual, rounded down)
            n_min=fs*T # Number of samples per window (minimum to achieve desired df)

            fft_freq = [ x*(fs/n) for x in range(n//2)] # Frequency labels for data
            detrend_data=signal.detrend(sid_to_data_map[sid]) # Detrend the data
            full_fft=[0]*int(fs//df) # Initialize
            
            # Sum the FFT for each window
            if (N >= fs):
                for i in range(0, nw):
                    ki=i*n
                    kf=(i+1)*n
                    data_sect=detrend_data[ki:kf]
                    fft_comp=np.fft.fft(data_sect)
                    fft_amp=np.abs(fft_comp/(n//2))
                    new_fft=fft_amp[0:(n//2)]
                    full_fft=[x + y for x, y in zip(full_fft, new_fft)]

                valid_fft=[i for i in full_fft if i != 0] # Remove extra list items
                avg_fft=[fft/nw for fft in valid_fft] # Divide by number of windows to make sum the average
    except Exception, e:
        print(repr(e))    
        avg_fft=0

    return avg_fft


# API to calculate and send the response for the baseline FFT of a specified interval
@app.route('/api/fft/avg', methods=('get',))
def getAvgFFT():
    #try:
        # Use get method to extract arguments
        df= float(request.args.get('df'))# Resolution of streamed signal (match to BL)
        df=1/4 # Current resultion used for BL averaging, can override input
        bl= int(request.args.get('bl')) # Length (days) to use for baseline FFT
        ts=request.args.get('ts') # Current timestamp (string date/ time)
        sid=int(request.args.get('sid')) # Selected sensor ID

        # Specify max and min frequency if user input exists
        try:
            min_in=int(request.args.get('min')) # input for max freq (Hz)
            max_in=int(request.args.get('max')) # input for min freq (Hz)
            ct=request.args.get('ct')
            if (ct!='undefined'):
                context=int(request.args.get('ct')) # input for extra amount to show beyond specified range for context
            else:
                context=0
            calcExtent=1 
        except Exception, e:
            calcExtent=0
            extent=[0,-1] # If inputs are undefined, use full frequency range

        current_ts = parseTime(ts, timezone,constants.RES_DATE_FORMAT)
        
        to_ts=editedTime(current_ts,days=-4) # Use 3 days prior so days can be added
        from_ts= editedTime(to_ts,days=-bl)

        # Pull FFT from Cassandra table of FFT averaged by day

        results=db_op.fetchFFTBaseline(from_ts, to_ts, sid)
        
        # Check if query was successful (len greater than 0)
        try:
            N=len(results[sid][0]['fft'])
        except Exception, e:
            print(e)
    
        f_nyquist=N*df
        
        # Calculate the indices to use for the upper and lower frequency bounds
        if (calcExtent):
            extent=calcFreqExtent(min_in, max_in, f_nyquist, context)
            extent=[int(x//df) for x in extent]
            extent[1]=extent[1]+1

        # Initialize variables
        full_fft=[0]*60000
        max_fft=0
        n_days=len(results[sid])

        # Sum the FFT for every day in the selected range
        for i in range(0, n_days):
            new_fft=results[sid][i]['fft'][extent[0]:extent[1]]
            full_fft=[x + y for x, y in zip(full_fft, new_fft)]
            # Calculate the maximum fft value
            if (calcExtent):
                if (extent[0]*df<3 and extent[1]*df> 5):
                    max_fft=max(max_fft, max(new_fft[int(3/df):extent[1]])) # Don't include the first 3 Hz in max freq
                else:
                    max_fft=max(max_fft, max(new_fft))
            else:
                max_fft=max(max_fft, max(new_fft[int(3/df):extent[1]])) # Don't include the first 3 Hz in max freq
        
        # Remove all extra list entires and divide by number of days to make the sum the average
        valid_fft=[i for i in full_fft if i != 0]
        avg_fft=[fft/n_days for fft in valid_fft]

        # Set the x values (frequency labels) for the associated magnitudes
        fft_freq=[x*df for x in range(N)][extent[0]:extent[1]]

        # Option to add code here that matches the baseline frequency resolution to the streamed resolution

        # Calculate the RMS of the baseline FFT
        N_rms=len(avg_fft)
        rms_A=[0]*N_rms
        for j in range(N_rms):
            rms_A[j]=abs(avg_fft[j]/N_rms)**2
        rms=math.sqrt(sum(rms_A))

        xy=map(list,zip(fft_freq,avg_fft))
        
        response = {'fft': xy, 'max_fft': max_fft, 'avg_rms': rms}

    #except Exception, e:
    #    response = {'fft': []}
    #    print('ERROR: ', e)

        return make_response(jsonify(response))


# Calculates FFT to stream
@app.route('/api/stream-fft', methods=('get',))
def streamFFT():
        try:
            min_in=int(request.args.get('min'))
            max_in=int(request.args.get('max'))
            ct=request.args.get('ct')
            if (ct!='undefined'):
                context=int(request.args.get('ct')) # input for extra amount to show beyond specified range for context
            else:
                context=0
            calcExtent=1
        except Exception,e:
            calcExtent=0
            extent=[0,-1]

    #try:
        ts_str = request.args.get('t')
        incr = int(request.args.get('ns'))
        sid = int(request.args.get('sid'))
        df= float(request.args.get('df'))
        T=1/df


        start_ts=parseTime(ts_str, timezone,constants.RES_DATE_FORMAT)
        end_ts= editedTime(start_ts,seconds=T)

        new_ts = editedTime(start_ts, seconds=incr)
        new_ts_str = formatTime(new_ts, timezone, constants.RES_DATE_FORMAT)

        fft=calculateFFT(start_ts, end_ts, sid, df)

        N=len(fft)
        f_nyquist=N*df

        if (calcExtent):
            extent=calcFreqExtent(min_in, max_in, f_nyquist, context)
            extent=[int(x//df) for x in extent]
            extent[1]=extent[1]+1

        filt_fft=fft[extent[0]:extent[1]]
        full_freq=[x*df for x in range(N)]
        filt_freq=full_freq[extent[0]:extent[1]]
        
        xy=map(list,zip(filt_freq, filt_fft))
        
        response = {'fft': xy, 'ts':ts_str, 'new_ts': new_ts_str}
        
    #except Exception, e:
    #    response = {'fft': [], 'ts':ts_str, 'new_ts': ts_str}
    #    print('ERROR: ', e)

        print(ts_str)

        return make_response(jsonify(response))


# Calculates the MSE for a specified sensor and time range
@app.route('/api/mse', methods=('get',))
@gzipped
def calcMSE():
        bl= int(request.args.get('bl')) # Length (days) to use for baseline FFT
        sid = int(request.args.get('sid'))
        from_ts_str = request.args.get('fts')
        to_ts_str=request.args.get('tts')
        incr = int(request.args.get('ns'))
        df=float(request.args.get('df'))
        dft=1 # Set as equal to the freq res of the fft tables (not a user input from dashboard)

        try:
            min_freq=int(request.args.get('min'))
            max_freq=int(request.args.get('max'))+1
        except Exception,e:
            min_freq=0
            max_freq=-1

        # Adjust frequencies to match # of samples
        min_freq=int(min_freq/dft)
        if (max_freq>0):
            max_freq=int(max_freq/dft)

        from_ts_bl= datetime(2022, 3, 16, 0, 0, 0)
        to_ts_bl= datetime(2022, 3, 19, 0, 0, 0)
        
        # Calculate Baseline FFT
        
        bl_results=db_op.fetchFFTBaseline(from_ts_bl, to_ts_bl, sid, 0)

        N_bl=len(bl_results[sid][0]['fft'])

        f_nyquist=N_bl*df

        full_fft=[0]*60000
        max_fft=0

        n_days=len(bl_results[sid])

        for i in range(0, n_days):
            new_fft=bl_results[sid][i]['fft'][min_freq:max_freq]
            full_fft=[x + y for x, y in zip(full_fft, new_fft)]

        valid_fft=[i for i in full_fft if i != 0]
        avg_fft=[fft/n_days for fft in valid_fft]
        
        # Get FFT for every second in time period

        from_ts=parseTime(from_ts_str, timezone,constants.RES_DATE_FORMAT)
        to_ts= parseTime(to_ts_str, timezone,constants.RES_DATE_FORMAT)

        # Round starting time to minute since datepicker is in minutes
        from_ts=roundToMinute(from_ts)
        to_ts=roundToMinute(to_ts)
        
        response = {'mse': []}

        # Query FFT tables for sensor and range
        results=db_op.fetchFftTsSort(from_ts, to_ts, sid, 1)

        ts_list=sorted([ts for ts in results.keys()])
        n_ts=print(len(ts_list))

        # Perform MS calculation for every time in the range
        for ts in ts_list:
            full_fft=results[ts][0]['fft']
            fft=full_fft[min_freq:max_freq]
            Nfft=len(fft)
            mse_i=[0]*Nfft
            for i in range(Nfft):
                mse_i[i]=(fft[i]-avg_fft[i])**2
            mse=(1/Nfft)*(sum(mse_i))
            response['mse'].append([ts,mse])

        resp = make_response(jsonify(response))

        return resp

# Calculates RMS for a specified sensor and time range
@app.route('/api/rms', methods=('get',))
@gzipped
def calcRMS():
    #try:
        sid = int(request.args.get('sid'))
        from_ts_str = request.args.get('fts')
        to_ts_str=request.args.get('tts')
        incr = int(request.args.get('ns'))
        print('incr: ', incr)
        df=float(request.args.get('df'))
        T=int(1/df)
        dft=1 # Set as equal to the freq res of the fft tables (not a user input from dashboard)

        print('df: ', df)
        print('T: ', T)

        try:
            min_freq=int(request.args.get('min'))
            max_freq=int(request.args.get('max'))+1
            if (min_freq<3 and max_freq>5):
                filt_freq=3
            else:
                filt_freq=min_freq
        except Exception,e:
            min_freq=0
            filt_freq=3
            max_freq=-1

        print('min_freq: ', min_freq)
        print('max_freq: ', max_freq)
        print('filt_freq: ',filt_freq)

        # Adjust frequencies to match # of samples, remove later bc all FFT are 1 sec
        min_freq=int(min_freq/dft)
        if (max_freq>0):
            max_freq=int(max_freq/dft)
        filt_freq=int(filt_freq/dft)

        print('min_freq: ', min_freq)
        print('max_freq: ', max_freq)
        print('filt_freq: ',filt_freq)

        from_ts=parseTime(from_ts_str, timezone,constants.RES_DATE_FORMAT)
        to_ts= parseTime(to_ts_str, timezone,constants.RES_DATE_FORMAT)
       
        from_ts=roundToMinute(from_ts)
        to_ts=roundToMinute(to_ts)

        response = {'rms': [], 'max_fft': [], 'avg_rms': []}
        
        sec=1

        results=db_op.fetchFftTsSort(from_ts, to_ts, sid, sec)
        #results=db_op.fetchFftTsSort(from_ts_adj, to_ts, sid, sec)  # Use only if averaging RMS based on T
        
        # Calculate RMS for each time
    
        all_rms=[]
        full_rms=[]

        ts_list=sorted([ts for ts in results.keys()])
    
        max_fft=0

        for ts in ts_list:
            full_fft=results[ts][0]['fft']
            max_fft=max(max_fft, max(full_fft[filt_freq: max_freq]))
            fft=full_fft[min_freq:max_freq]
            Nfft=len(fft)
            rms_A=[0]*Nfft
            for i in range(Nfft):
                rms_A[i]=abs(fft[i]/Nfft)**2
            rms=math.sqrt(sum(rms_A))
            all_rms.append(rms)
            response['rms'].append([ts,rms])
            full_rms.append(rms)
        
        # Code to average over increment
        if (df>1):
            response['rms']=[]
            for i in range(n_ts):
                k1=i*incr
                k2=(i+1)*incr-1
                adj_ts=ts_list[k1]
                adj_rms=full_rms[k1:k2]
                avg=sum(adj_rms) / len(adj_rms)
                response['rms'].append([adj_ts,avg])

        # Code to do moving average of RMS based on T if desired, must also uncomment results query 
        #n_ts=int((len(ts_list)-T/sec)/incr)
        #if (df>1):
        #    response['rms']=[]
        #    for i in range(n_ts):
        #        k1=i*incr
        #        print(k1)
        #        k2=i*incr+T-1
        #        print(k2)
        #        adj_ts=ts_list[k1+T]
        #        print(adj_ts)
        #        adj_rms=full_rms[k1:k2]
        #        avg=sum(adj_rms) / len(adj_rms)
        #        response['rms'].append([adj_ts,avg])


        # Calculate the average RMS for the range

        avg_rms=sum(all_rms)/len(all_rms)
        response['avg_rms'].append(avg_rms)

        response['max_fft'].append(max_fft)

    #except Exception, e:
    #    response = {'rms': [], 'max_fft': [], 'avg_rms': []}
    #    print('ERROR: ', e)
        
        resp = make_response(jsonify(response))

        return resp


# Calculates RMS and FFT to stream together
@app.route('/api/stream-rms-fft', methods=('get',))
def streamRmsFft():
        try:
            min_freq=int(request.args.get('min'))
            max_freq=int(request.args.get('max'))+1
            ct=request.args.get('ct')
            if (ct!='undefined'):
                context=int(request.args.get('ct'))
            else:
                context=0
            calcExtent=1
        except Exception,e:
            calcExtent=0
            extent=[0,-1]
            min_freq=0
            filt_freq=min_freq
            max_freq=-1

    #try:
        ts_str = request.args.get('t')
        incr = int(request.args.get('ns'))
        sid = int(request.args.get('sid'))
        init= int(request.args.get('init')) 
        df= float(request.args.get('df'))
        T=1/df;

        start_ts=parseTime(ts_str, timezone,constants.RES_DATE_FORMAT)
        
        end_ts= editedTime(start_ts,seconds=T)
        new_ts = editedTime(start_ts, seconds=incr)
        new_ts_str = formatTime(new_ts, timezone, constants.RES_DATE_FORMAT)

        # Calculate FFT

        fft=calculateFFT(start_ts, end_ts, sid, df)

        N=len(fft)
        f_nyquist=N*df

        if (calcExtent):
            extent=calcFreqExtent(min_freq, max_freq, f_nyquist, context)
            extent=[int(x//df) for x in extent]
            extent[1]=extent[1]+1

        filt_fft=fft[extent[0]:extent[1]]
        full_freq=[x*df for x in range(N)]
        filt_freq=full_freq[extent[0]:extent[1]]
        xy=map(list,zip(filt_freq, filt_fft))

        response = {'fft': xy, 'ts':ts_str, 'new_ts': new_ts_str, 'rms': []}

        # Calculate RMS
        
        trend_length=300
        
        # Calculate RMS for last 5 min during the initial call
        if (init==True):
            init_start=editedTime(start_ts,seconds=-trend_length)
            init_end=start_ts
            st_list = commons.splitRangeInSpecSeconds(init_start, init_end, incr)

            for st in st_list:
                et=editedTime(st, seconds=incr)

                # Calculate RMS

                results=db_op.fetchFftTsSort(st, et, sid, 1)

                # Calculate RMS for each second

                all_rms=[]

                ts_list=sorted([ts for ts in results.keys()])

                for ts in ts_list:
                    full_fft=results[ts][0]['fft']
                    fft=full_fft[min_freq:max_freq]
                    Nfft=len(fft)
                    rms_A=[0]*Nfft
                    for i in range(Nfft):
                        rms_A[i]=abs(fft[i]/Nfft)**2
                    rms=math.sqrt(sum(rms_A))
                    all_rms.append(rms)

                if (len(all_rms)>0):
                    avg=sum(all_rms) / len(all_rms)
                    response['rms'].append([st,avg])
        
        # Calculate only the latest RMS on subsequent calls
        else:
            et=editedTime(start_ts, seconds=incr)
            
            results=db_op.fetchFftTsSort(start_ts, et, sid, 1)

            all_rms=[]

            ts_list=sorted([ts for ts in results.keys()])

            for ts in ts_list:
                full_fft=results[ts][0]['fft']
                fft=full_fft[min_freq:max_freq]
                Nfft=len(fft)
                rms_A=[0]*Nfft
                for i in range(Nfft):
                    rms_A[i]=abs(fft[i]/Nfft)**2
                rms=math.sqrt(sum(rms_A))
                all_rms.append(rms)

            if (len(all_rms)>0):
                avg=sum(all_rms) / len(all_rms)
                response['rms'].append([start_ts,avg])

        resp = make_response(jsonify(response))

        return resp


# Currently unused floor code
@app.route('/api/floor/sensors/<floor_num>', methods=('get',))
@gzipped
def floorSensors(floor_num):
    print('Floor sensor called')
    ts_from = request.args.get('from')
    if (ts_from is None):
        return make_response(
            jsonify({'msg': 'error', 'reason': 'invalid/empty timestamp'}))
    ts_from = parseTime(ts_from, timezone,
                        constants.RES_DATE_FORMAT)
    sensors = db_op.getSensorsByFloor(floor_num)
    sids = map(lambda x: x['sid'], sensors)

    results = db_op.fetchPSDAsync(
        ts_from, ts_to, get_power_dist=False)
    sids = [sid for sid in results.keys() if sid in sids]
    response = defaultdict(list)
    for sid in sids:
        results[sid] = sorted(results[sid], key=lambda x: x['ts'])
        for v in results[sid]:
            response[sid].append(
                [formatTime(v['ts'], timezone, constants.RES_DATE_FORMAT),
                 v['total_power']])

    return make_response(jsonify(response))

# Extracts PSD for floor page
@app.route('/api/floor/psd/<floor_num>', methods=('get',))
@gzipped
def floorPSD(floor_num):
    print('Floor PSD called')
    ts_from = request.args.get('from')
    ts_to = request.args.get('to')
    if (ts_from is None) or (ts_to is None):
        return make_response(
            jsonify({'msg': 'error', 'reason': 'invalid/empty timestamp'}))
    ts_from = parseTime(ts_from, timezone,
                        constants.RES_DATE_FORMAT)
    ts_to = parseTime(ts_to, timezone,
                      constants.RES_DATE_FORMAT)
    sensors = db_op.getSensorsByFloor(floor_num)
    sids = map(lambda x: x['sid'], sensors)
    results = db_op.fetchPSDAsync(
        ts_from, ts_to, get_power_dist=False)
    sids = [sid for sid in results.keys() if sid in sids]
    response = defaultdict(list)
    for sid in sids:
        results[sid] = sorted(results[sid], key=lambda x: x['ts'])
        for v in results[sid]:
            response[sid].append(
                [formatTime(v['ts'], timezone, constants.RES_DATE_FORMAT),
                 v['total_power']])

    return make_response(jsonify(response))

# Extracts sensor info and formats
@app.route('/api/sensor_info', methods=('get',))
def getSensorInfo():
    sensor_objs = db_op.getSensorInfoAll()
    resp_dict = {}
    for obj in sensor_objs:
        if (obj.x_pos != '') or (obj.floor_num != ''):
            resp_dict[str(obj.id)] = {
                'daq_name': obj.daq_name,
                'floor': obj.floor_num,
                'orientation': obj.orientation,
                'sensitivity': obj.sensitivity,
                'serial': obj.serial_num,
                'name': obj.name,
                'x': obj.x_pos,
                'y': obj.y_pos,
                'z': obj.z_pos
            }
    return jsonify(resp_dict)


# Query PSD for explore/ sensor page
@app.route('/api/explore/<sensor_name>', methods=('get',))
def getExplorationForSensor(sensor_name):
    from_time_str = request.args.get('from')
    to_time_str = request.args.get('to')

    sid = db_op.sensorNameToIdMap()[sensor_name]
    from_time = parseTime(from_time_str, timezone, constants.RES_DATE_FORMAT)
    to_time = parseTime(to_time_str, timezone, constants.RES_DATE_FORMAT)

    if((sid is None) or (from_time > to_time)):
        return jsonify({'msg': 'error'})

    psd = db_op.fetchPSDAsyncById(
        from_time, to_time, sid, get_power_dist=False)

    response = {'raw': [], 'psd': []}

    if sid not in psd:
        return make_response(jsonify(response))

    psd[sid] = sorted(psd[sid], key=lambda x: x['ts'])

    for data in psd[sid]:
        response['psd'].append([data['ts'], data['total_power']])

    return make_response(jsonify(response))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8000)
