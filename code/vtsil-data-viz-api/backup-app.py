from __future__ import print_function
import sys
from flask import (
    Flask, jsonify, request, after_this_request,
    render_template, make_response)
import datavizapi.cassandra_operations as db_op
from datavizapi.utils.time_utils import (
    editedTime, formatTime, parseTime, currTime)
import datavizapi.constants as constants
import time
from datetime import datetime, timedelta
from datavizapi import AppConfig
from cStringIO import StringIO as IO
import gzip
import functools
import json
from datavizapi.utils import commons
from collections import defaultdict

app = Flask(__name__, static_url_path='', static_folder='web/', template_folder='templates')
config = AppConfig().getConfig()
timezone = constants.APP_TZ

# Editable parameters
STREAMING_INCR = 1 # Number of seconds to include in each query
save_period=1 # Number of seconds of data saved per file
time_incr= 1 # Time to increase by for each new query
cookie_limit=10 # Number of seconds until cookies expire
fail_limit=5 # Number of failed attempts before skipping to next file
time_offset = 2*save_period # Delay in seconds from real-time to start automatic streaming at

@app.route('/api/export')
def dataExport():
    from_ts = request.args.get('f')
    to_ts = request.args.get('t')
    daq_names = request.args.getlist('daqname')
    fname = request.args.get('fname')
    sensor_objs = db_op.getSensorInfoAll()
    sids = []
    for obj in sensor_objs:
        if obj.daq_name in daq_names:
            sids.append(obj.id)
    commons.produceToKafka(
        config['kafka']['file_download']['topic'],
        json.dumps({'fname': fname, 'sids': sids,
                    'from_ts': from_ts, 'to_ts': to_ts}))
    return make_response(jsonify({}))


def gzipped(f):
    """
    taken from: http://flask.pocoo.org/snippets/122/
    """
    @functools.wraps(f)
    def view_func(*args, **kwargs):
        @after_this_request
        def zipper(response):
            accept_encoding = request.headers.get('Accept-Encoding', '')

            if 'gzip' not in accept_encoding.lower():
                return response

            response.direct_passthrough = False

            if (response.status_code < 200 or
                    response.status_code >= 300 or
                    'Content-Encoding' in response.headers):
                return response
            gzip_buffer = IO()
            gzip_file = gzip.GzipFile(mode='wb',
                                      fileobj=gzip_buffer)
            gzip_file.write(response.data)
            gzip_file.close()

            response.data = gzip_buffer.getvalue()
            response.headers['Content-Encoding'] = 'gzip'
            response.headers['Vary'] = 'Accept-Encoding'
            response.headers['Content-Length'] = len(response.data)

            return response

        return f(*args, **kwargs)

    return view_func


def getCookieValue(st, start_ts, incr=STREAMING_INCR):
    ct=editedTime(currTime(),seconds=time_offset) 
    # If no user input has been given
    if (start_ts is None):
        # If there are no cookies use the current time minus the time_offset
        if (st is None):
            st_parsed=ct
            st = formatTime(st_parsed, timezone, constants.RES_DATE_FORMAT)
        # If there are cookies use the next time increment
        else:
            st_parsed = parseTime(st, timezone, constants.RES_DATE_FORMAT)
            st_parsed = editedTime(st_parsed, seconds=incr)
            st = formatTime(st_parsed, timezone, constants.RES_DATE_FORMAT)
    # If there is a user input but no cookies, use the user input directly
    elif (st is None) or (request.cookies.get('lst')=='0'):
        st = formatTime(start_ts, timezone, constants.RES_DATE_FORMAT)
    # If there is a user input and cookies, use the next time increment
    else:
        st_parsed = parseTime(st, timezone, constants.RES_DATE_FORMAT)
        st_parsed = editedTime(st_parsed, seconds=incr)
        st = formatTime(st_parsed, timezone, constants.RES_DATE_FORMAT)
    return st


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/building')
def building():
    return render_template('building.html')


@app.route('/explore')
def explore():
    return render_template('explore.html')


@app.route('/floorwise')
def floorwise():
    return render_template('floorwise.html')


@app.route('/oma')
def oma_results():
    return render_template('oma.html')


@app.route('/api/stream', methods=('get',))
@gzipped
def streaming():
    
    t0=datetime.now()
    
    start_ts = request.args.get('t')
    
    # Check if user input for start time has been given
    try:
        start_ts=parseTime(start_ts, timezone,constants.RES_DATE_FORMAT)
        lst='1'
    except ValueError:
        start_ts=None
        lst='0'
    # lst variable distinguishes whether this is the first query after the start_ts variable was input by a user

    incr=time_incr
    #incr = int(request.args.get('ns'))
    
    # If this is the first query or the previous was successful, move to next query
    if (request.cookies.get('fc')=='0') or (request.cookies.get('fc') is None):
        incr=incr
        fc=0
    # If query failed, try again for up to (fail_limit) iterations
    elif (int(request.cookies.get('fc'))<fail_limit):
        incr=0
        fc=int(request.cookies.get('fc'))
    # If query fails (fail_limit) times, skip to next data file and restart attempts
    else:
        incr=save_period
        fc=0
    
    from_d_str = getCookieValue(request.cookies.get('st'), start_ts, incr=incr)
    from_d = parseTime(from_d_str, timezone, constants.RES_DATE_FORMAT)
    to_d = editedTime(from_d, seconds=STREAMING_INCR)
    
    print('lst: ', lst)
    print('ts: ',from_d)
    
    response = {'v1': [], 'v2': []}

    try: 
        results = db_op.fetchLatestPSD(from_d, to_d=to_d)
        sid_list = results.keys()

        # for v1
        value = {}
        for sid, val in results.items():
            if len(val) > 0:
                value[str(sid)] = val[0]['total_power']
        response['v1'].append(
            {'ts': results[sid_list[0]][0]['ts'],
            'value': value})
        
        # for v2
        for sid, v in results.items():
            if len(v) > 0:
                temp = {"id": str(sid), "data": []}
                num_freq = len(v[0]['power_dist']) / 2
                si = 0
                while si < num_freq:
                    temp['data'].append({"f": float(si), "p": v[0]['power_dist'][si]})
                    si += 1
                response['v2'].append(temp)
        
        resp = make_response(jsonify(response))
        fc=0
        resp.set_cookie('fc', str(fc), max_age=cookie_limit)

    except IndexError, e:
        resp = make_response(jsonify(response))
        #resp=make_response(jsonify({'msg': 'error', 'reason': 'invalid/empty timestamp'}))
        print('ERROR: ', e)
        # Update the fail count
        new_fc=fc+1
        resp.set_cookie('fc', str(new_fc),max_age=cookie_limit)

    resp.set_cookie('lst', lst, max_age=cookie_limit)
    resp.set_cookie('st', from_d_str, max_age=cookie_limit)

    t1=datetime.now()
    print('Total time: ',(t1-t0).total_seconds())

    return resp


@app.route('/api/psd/<sensor_id>', methods=('get',))
def getSensorPSD(sensor_id):
    ts = request.args.get('d')
    if ts is None:
        return make_response(
            jsonify({'msg': 'error', 'reason': 'invalid/empty timestamp'}))

    from_ts = parseTime(ts, timezone,
                        constants.RES_DATE_FORMAT)
    results = db_op.fetchPSDAsync(
        from_ts, from_ts, get_avg_power=False, get_power_dist=True)
    response = {'data': []}

    for idx, data in enumerate(results[int(sensor_id)][0]['power_dist']):
        response['data'].append([idx, data])
    return make_response(jsonify(response))


@app.route('/api/floor/psd/<floor_num>', methods=('get',))
@gzipped
def floorPSD(floor_num):
    ts_from = request.args.get('from')
    ts_to = request.args.get('to')
    if (ts_from is None) or (ts_to is None):
        return make_response(
            jsonify({'msg': 'error', 'reason': 'invalid/empty timestamp'}))
    ts_from = parseTime(ts_from, timezone,
                        constants.RES_DATE_FORMAT)
    ts_to = parseTime(ts_to, timezone,
                      constants.RES_DATE_FORMAT)
    sensors = db_op.getSensorsByFloor(floor_num)
    sids = map(lambda x: x['sid'], sensors)
    results = db_op.fetchPSDAsync(
        ts_from, ts_to, get_power_dist=False)
    sids = [sid for sid in results.keys() if sid in sids]
    response = defaultdict(list)
    for sid in sids:
        results[sid] = sorted(results[sid], key=lambda x: x['ts'])
        for v in results[sid]:
            response[sid].append(
                [formatTime(v['ts'], timezone, constants.RES_DATE_FORMAT),
                 v['total_power']])

    return make_response(jsonify(response))


@app.route('/api/sensor_info', methods=('get',))
def getSensorInfo():
    sensor_objs = db_op.getSensorInfoAll()
    resp_dict = {}
    for obj in sensor_objs:
        if (obj.x_pos != '') or (obj.floor_num != ''):
            resp_dict[str(obj.id)] = {
                'daq_name': obj.daq_name,
                'floor': obj.floor_num,
                'orientation': obj.orientation,
                'sensitivity': obj.sensitivity,
                'serial': obj.serial_num,
                'name': obj.name,
                'x': obj.x_pos,
                'y': obj.y_pos,
                'z': obj.z_pos
            }
    return jsonify(resp_dict)


@app.route('/api/explore/<sensor_name>', methods=('get',))
def getExplorationForSensor(sensor_name):
    from_time_str = request.args.get('from')
    to_time_str = request.args.get('to')

    sid = db_op.sensorNameToIdMap()[sensor_name]
    from_time = parseTime(from_time_str, timezone, constants.RES_DATE_FORMAT)
    to_time = parseTime(to_time_str, timezone, constants.RES_DATE_FORMAT)

    if((sid is None) or (from_time > to_time)):
        return jsonify({'msg': 'error'})

    psd = db_op.fetchPSDAsyncById(
        from_time, to_time, sid, get_power_dist=False)

    response = {'raw': [], 'psd': []}

    if sid not in psd:
        return make_response(jsonify(response))

    psd[sid] = sorted(psd[sid], key=lambda x: x['ts'])

    for data in psd[sid]:
        response['psd'].append([data['ts'], data['total_power']])

    return make_response(jsonify(response))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8000)
