# Common operations used in processing scripts and app

import time_utils
from confluent_kafka import Producer
from datavizapi import AppConfig

config = AppConfig().getConfig()

def splitRangeInDays(start_time, end_time):
    dates = []
    d = time_utils.roundToDay(start_time)
    if start_time == end_time:
        dates.append(d)
        return dates
    while d < end_time:
        dates.append(d)
        d = time_utils.editedTime(d, days=1)
    return dates

def splitRangeInHours(start_time, end_time):
    dates = []
    d = time_utils.roundToHour(start_time)
    if start_time == end_time:
        dates.append(d)
        return dates
    while d < end_time:
        dates.append(d)
        d = time_utils.editedTime(d, seconds=3600)
    return dates


def splitRangeInMinutes(start_time, end_time):
    dates = []
    d = time_utils.roundToMinute(start_time)
    if start_time == end_time:
        dates.append(d)
        return dates
    while d < end_time:
        dates.append(d)
        d = time_utils.editedTime(d, seconds=60)
    return dates


def splitRangeInSeconds(start_time, end_time):
    dates = []
    d = start_time
    if start_time == end_time:
        dates.append(d)
        return dates
    while d < end_time:
        dates.append(d)
        d = time_utils.editedTime(d, seconds=1)
    return dates

def splitRangeInSpecSeconds(start_time, end_time, incr):
    dates = []
    d = start_time
    if start_time == end_time:
        dates.append(d)
        return dates
    while d < end_time:
        dates.append(d)
        d = time_utils.editedTime(d, seconds=incr)
    return dates

def produceToKafka(topic, message):
    p = Producer({'bootstrap.servers': ",".join(config['kafka']['servers'])})
    p.produce(topic, message, partition=0)
    p.poll(1)
