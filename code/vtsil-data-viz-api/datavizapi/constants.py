# Constants for application code
DATE_FORMAT = '%Y-%m-%d %H:%M:%S.%f'
RES_DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
FILENAME_FORMAT = '%Y-%m-%d_%H_%M_%S'
HOUR_FORMAT = '%Y-%m-%d %H:00:00'
APP_TZ = 'US/Eastern' # Timzone where app is being accessed
#APP_TZ = 'US/Central' # Timzone where app is being accessed
DATA_TZ='US/Eastern' # Timezone data is being saved from
NUM_SENS=224 # Number of available sensors from the DAQ output (physical channels - channels without sensors)
ACQ_LEN=60 # Acquisition length, in seconds
