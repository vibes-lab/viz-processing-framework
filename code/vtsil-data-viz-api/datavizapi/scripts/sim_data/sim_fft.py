from __future__ import print_function, division
import sys
from flask import (
    Flask, jsonify, request, after_this_request,
    render_template, make_response)
import datavizapi.sim_cass_ops as db_op
#import datavizapi.cassandra_operations as db_op
from datavizapi.utils.time_utils import (
    editedTime, formatTime, parseTime, currTime)
import datavizapi.constants as constants
import time
import math
import pytz
import numpy as np
from datetime import datetime, timedelta
from scipy import signal, integrate
from datavizapi import AppConfig
from datetime import datetime, timedelta
from cStringIO import StringIO as IO
import gzip
import functools
import json
from datavizapi.utils import commons
from collections import defaultdict

timezone = constants.APP_TZ
utc_tz=pytz.UTC

future_results=[]

def calculateFFT(start_ts, sid, incr, df, duration):
    try:
        end_ts= editedTime(start_ts,seconds=duration)
        new_ts = editedTime(start_ts, seconds=incr)
        
        sid_to_data_map=defaultdict(list)
        
        #raw_data = db_op.fetchSensorDataAsync(start_ts, end_ts)
        raw_data=db_op.fetchSensorByIDAsync(start_ts, end_ts, sid)

        #print(raw_data)
        
        for sid, objects in raw_data.items():
            print(sid)
            fs = None
            for obj in objects:
                sid_to_data_map[sid].extend(obj['data'])
                if fs is None:
                    fs = len(obj['data'])
            
            # Calculate required samples and windows for desired resolution
            N=len(sid_to_data_map[sid]) # Number of samples available
            print('N: ', N)
            dt=1/fs # Time resolution (sec)
            T=1/df # Minimum window period (sec)
            total_t=N/fs # Total amount of time collected over (sec)
            nw=int(math.floor(total_t/T))  # Number of windows possible without overlap (rounded down)
            #nw=max(1, nw)
            n=int(math.floor(N/nw)) # Number of samples per window (actual, rounded down)
            #print('n: ', n)
            n_min=fs*T # Number of samples per window (minimum to achieve desired df)

            fft_freq = [ x*(fs/n) for x in range(n//2)]
            detrend_data=signal.detrend(sid_to_data_map[sid])
            full_fft=[0]*int(fs//df)
            if (N >= fs):
                for i in range(0, nw):
                    ki=i*n
                    kf=(i+1)*n
                    data_sect=detrend_data[ki:kf]
                    fft_comp=np.fft.fft(data_sect)
                    fft_amp=np.abs(fft_comp/(n//2))
                    new_fft=fft_amp[0:(n//2)]
                    full_fft=[x + y for x, y in zip(full_fft, new_fft)]

                valid_fft=[i for i in full_fft if i != 0]
                avg_fft=[fft/nw for fft in valid_fft]

                future_results.append(db_op.insertFFTAsync(sid, list(avg_fft), start_ts))

                for res in future_results:
                    res.result()

            else:
                error=int(q)
    except Exception, e:
        print(repr(e))    
        new_ts=start_ts

    return new_ts


df=1/(5) # Frequency resolution
duration=5 # 5 min duration (sec)
incr=5 # 5 sec sliding window
sid=17 # MUST CHANGE THIS TO THE SENSOR YOU WANT

start_time=datetime(2022, 3,11, 16, 0, 0)
start_time_utc=utc_tz.localize(start_time)
print('start_time: ',start_time_utc)
end_time=datetime(2022, 3, 11, 16, 30, 0)
end_time_utc=utc_tz.localize(end_time)
print('end_time: ',end_time_utc)
start_ts=start_time_utc

while (start_ts < end_time_utc): 
    print(start_ts)
    new_ts=calculateFFT(start_ts, sid, incr, df, duration)
    start_ts=new_ts
    time.sleep(1)
