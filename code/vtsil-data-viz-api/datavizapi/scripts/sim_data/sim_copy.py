# Copy unshifted data into simulated data Cass table

from __future__ import print_function, division
import os
import time
import json
import math
from datetime import datetime, timedelta
import ftp_operations as f_ops
import datavizapi.sim_cass_ops as db_op
#import datavizapi.cassandra_operations as db_op
from datavizapi.utils.time_utils import (roundToHour, editedTime, formatTime, parseTime, currTime)
import datavizapi.constants as constants
from scipy import signal, integrate
import numpy as np
import pytz
from collections import defaultdict
from datavizapi import AppConfig
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt

config = AppConfig().getConfig()

FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']
data_tz=pytz.timezone(constants.DATA_TZ)
utc_tz=pytz.UTC

future_results=[]
sid_to_data_map = defaultdict(list)

def calculate_and_save_fft(from_ts, to_ts, sid, df, fs):
        
        #Clear all large variables    
        future_results[:] = []
        raw_data=None
        sid_to_data_map.clear()
        fft_freq=None
        full_fft=None
        detrend_data=None
        valid_fft=None
        avg_fft=None
        new_fft=None
        avg_fft=None
        data_sect=None
        fft_comp=None
        fft_amp=None
        success=False

        t0=datetime.now()

        try:
            raw_data = db_op.fetchSensorByIDAsync(from_ts, to_ts, sid)
        except Exception, e:
            print(repr(e))
            return success

        t1=datetime.now()
        print('Query: ',(t1 - t0).total_seconds())

        for sid, objects in raw_data.items():
            print(sid)
            fs = None
            for obj in objects:
                sid_to_data_map[sid].extend(obj['data'])
                if fs is None:
                    fs = len(obj['data'])
            
            # Import data and set params

            raw_td=sid_to_data_map[sid] # raw time domain accel signal
            td_data=signal.detrend(raw_td)
            td_data=list(td_data)

            # Split data back into seconds

            total_samp=len(td_data)
            print(total_samp)
            total_time=total_samp//fs
            
            for q in range(total_time):
                mod_ts=from_ts+timedelta(seconds=q)
                mod_ts=mod_ts.strftime(FILENAME_DATE_FORMAT)
                new_data=td_data[q*int(fs):(q+1)*int(fs)]
                
                #print(new_data)
                
                #print(len(new_data))
                #print(mod_ts)

                future_results.append(db_op.insertSimSensorData(sid, mod_ts, new_data))

                for res in future_results:
                    res.result()
                
            success=True

        t2=datetime.now()
        print('FFT: ',(t2 - t1).total_seconds())

        return success

fs=256 # Sampling rate in Hz
T=5*60 # Period in seconds
df=1/T # Frequency resolution
sid=249 # Selected sensor sid

start_time=datetime(2022, 3, 28, 16, 00, 00)
start_time_utc=utc_tz.localize(start_time)
print('start_time: ',start_time_utc)
end_time=datetime(2022, 3, 28, 17, 55, 50)
end_time_utc=utc_tz.localize(end_time)
print('end_time: ',end_time_utc)
current_time=start_time_utc

while (current_time < end_time_utc):
    from_ts=current_time
    print('from_ts: ', from_ts)
    to_ts=current_time+timedelta(seconds=T)
    print('to_ts: ', to_ts)
    success=calculate_and_save_fft(from_ts, to_ts, sid, df, fs)
    if (success):
        current_time=to_ts
    time.sleep(1)
    #break # REMOVE THIS BEFORE ACTUALLY RUNNING
