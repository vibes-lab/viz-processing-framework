# Sync data from directory containing data to be used in the simulated damage scenario

import os
import time
import json
import ftp_operations as f_ops
from datetime import datetime, timedelta
from confluent_kafka import Producer
from datavizapi import AppConfig

config = AppConfig().getConfig()

p = Producer({'bootstrap.servers': ",".join(config['kafka']['servers'])})

SCRIPT_BASE_PATH = os.path.dirname(os.path.realpath(__file__))

def parseSampleRateFromConfig(fname):
    sample_rate = None
    with open(fname, 'r') as f:
        lines = f.readlines()
        sample_rate_line = \
            filter(lambda x: x.startswith('real sample rate'), lines)[0]
        sample_rate = sample_rate_line.split('=')[-1].strip(' \n\r')
    return sample_rate


def putNewfilenames(data, sample_rate):
    if len(data) == 0:
        return
    data = map(lambda x: x[-1], data)
    for d in data:
        payload = {
            "sample_rate": sample_rate,
            "file_name": d
        }
        p.produce(
            config['kafka']['consumer_h5']['topic'],
            json.dumps(payload))
    p.poll(0)

last_config=None

ftp = f_ops.connectAndGetFTP()
#remote_dir,num_dirs=f_ops.getRemoteDir(ftp,None,None)
remote_dir='sim_data'
print(remote_dir)
ftp.cwd(remote_dir)
config_fname, last_config = f_ops.fetchConfigFile(ftp,None)
sample_rate = parseSampleRateFromConfig(config_fname)
print(config_fname)


while True:
    last_date = f_ops.getLastReadDate()
    #print(last_date)
    ftp = f_ops.connectAndGetFTP()
    try:
        ftp.cwd(remote_dir)
        records = f_ops.getFileMetadata(ftp)
        records = f_ops.filterUnfetchedRecords(last_date, records)
        f_ops.updateLastReadDate(records)
        ftp.quit()
        ftp.close()
        #print(records)
        putNewfilenames(records, sample_rate)
        f_ops.timeOffsetList(records)
        time.sleep(0.005)
    except Exception, e:
        print(e)
        ftp.quit()
        ftp.close()
