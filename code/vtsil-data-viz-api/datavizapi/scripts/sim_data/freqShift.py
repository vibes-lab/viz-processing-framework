# Initial code to shift frequencies, was modified for use with Cassandra in other scripts

# -*- coding: utf-8 -*-

import scipy.signal as signal
import numpy as np
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt

#Import Data and set params
data = np.genfromtxt('/home/vtsil/vtsil-data-viz-api/datavizapi/scripts/raw_data_249.csv', delimiter=',')
fs = 256;
N = len(data)
k = np.arange(0,N) 
k_half = np.arange(0,int(N/2)+1) #Note that this is one sample more that half!
t = k/fs

#Preprocess
td_data=signal.detrend(data)
print('td_data: ',len(td_data))


#FFT
fft_spec=np.around(np.fft.fft(td_data),2)

print('fft_spec: ',len(fft_spec))

f = k*fs/N
mag=np.abs(fft_spec)

print('mag:', len(mag))

"""
Enforce a consistent value for a zero complex number, for some reason it
varies between 0j,-0j, -0-0j, etc., all which give different phase values!
"""
for i in k:
    if mag[i] == 0:
        fft_spec[i] = 0j

phase = np.angle(fft_spec)

#Half Spectrum
fft_halfspec = fft_spec[k_half]

print('fft_halfspec: ', len(fft_halfspec))

mag_half=np.abs(fft_halfspec)
phase_half=np.angle(fft_halfspec)
f_half = k_half*fs/N

print('k_half: ', k_half)
print('f_half: ', f_half)

#Shift
f_shift = 0 #shift in Hz
k_shift = int(np.round(f_shift*N/fs)) #shift in indeces

fft_halfspec_shift = np.roll(fft_halfspec, k_shift)
fft_halfspec_shift[0]=0j #force zero frequency to be zero magnitude and phase
fft_halfspec_shift[int(N/2)]=0j #force nyquist freq to be zero magnitude and phase
mag_half_shift=np.abs(fft_halfspec_shift)
phase_half_shift=np.angle(fft_halfspec_shift)

#Reconstruct

fft_flipped = np.flip(np.conjugate(fft_halfspec_shift[np.arange(1,int(N/2))]))
fft_spec_shift = np.concatenate((fft_halfspec_shift,fft_flipped))
td_data_shift = np.real(np.fft.ifft(fft_spec_shift))


mag_shift=np.abs(fft_spec_shift)
phase_shift = np.angle(fft_spec_shift)

print('Modified: ',td_data_shift)
print('Original: ', td_data)



"""
# Plotting
plt.figure(1), plt.plot(t,td_data)
plt.plot(t,td_data_shift)
plt.title('Time series')

plt.figure(2), plt.plot(f_half,mag_half)
plt.plot(f_half,mag_half_shift)
plt.title('Half Spectrum Mag')
plt.figure(3), plt.plot(f_half,phase_half,'-o')
plt.plot(f_half,phase_half_shift,'-o')
plt.title('Half Spectrum Phase')

plt.figure(4)
plt.plot(f,mag)
plt.plot(f,mag_shift)
plt.title('Full Spectrum Mag')

plt.figure(5)
plt.plot(f,phase)
plt.plot(f,phase_shift)
plt.title('Full spectrum Phase')
"""

#Test
fft_spec_test=np.around(np.fft.fft(td_data_shift),2)
mag_test=np.abs(fft_spec_test)

"""
Enforce a consistent value for a zero complex number, for some reason it
varies between 0j,-0j, -0-0j, etc., all which give different phase values!
"""
for i in k:
    if mag_test[i] == 0:
        fft_spec_test[i] = 0j

phase_test = np.angle(fft_spec_test)
"""
plt.figure(4), plt.plot(f,mag_test)
plt.figure(5), plt.plot(f,phase_test,'-o')

# One-sided spectrum
mag_1=(mag/(N//2))[0:(N//2)]
phase_1=(phase/(N//2))[0:(N//2)]
# Two-sided spectrum
mag_2=np.concatenate((mag_1, np.flip(mag_1)))
phase_2=np.concatenate((phase_1, -np.flip(phase_1)))
Xre, Xim = pol2cart(phase_2, mag_2)
X= Xre+ 1j*Xim
mod_td_data=np.fft.ifft(X)

"""
