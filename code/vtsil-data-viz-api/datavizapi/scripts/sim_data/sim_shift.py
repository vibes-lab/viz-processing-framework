# Shift data by specified amount and add to simulated data table

from __future__ import print_function, division
import os
import time
import json
import math
from datetime import datetime, timedelta
import ftp_operations as f_ops
import datavizapi.sim_cass_ops as db_op
#import datavizapi.cassandra_operations as db_op
from datavizapi.utils.time_utils import (roundToHour, editedTime, formatTime, parseTime, currTime)
import datavizapi.constants as constants
from scipy import signal, integrate
import numpy as np
import pytz
from collections import defaultdict
from datavizapi import AppConfig
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt

config = AppConfig().getConfig()

FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']
data_tz=pytz.timezone(constants.DATA_TZ)
utc_tz=pytz.UTC

future_results=[]
sid_to_data_map = defaultdict(list)

def calculate_and_save_fft(from_ts, to_ts, sid, df, fs):
        
        #Clear all large variables    
        future_results[:] = []
        raw_data=None
        sid_to_data_map.clear()
        fft_freq=None
        full_fft=None
        detrend_data=None
        valid_fft=None
        avg_fft=None
        new_fft=None
        avg_fft=None
        data_sect=None
        fft_comp=None
        fft_amp=None
        success=False

        t0=datetime.now()

        try:
            raw_data = db_op.fetchSensorByIDAsync(from_ts, to_ts, sid)
        except Exception, e:
            print(repr(e))
            return success

        t1=datetime.now()
        print('Query: ',(t1 - t0).total_seconds())

        #print(raw_data)

        for sid, objects in raw_data.items():
            print(sid)
            fs = None
            for obj in objects:
                sid_to_data_map[sid].extend(obj['data'])
                if fs is None:
                    fs = len(obj['data'])
            
            # Import data and set params

            raw_td=sid_to_data_map[sid] # raw time domain accel signal
            N=len(raw_td) # Number of samples available            
            qf=fs/N # Actual frequency resolution
            k = np.arange(0,N)
            k_half = np.arange(0,int(N/2)+1) #Note that this is one sample more than half!
            t = k/fs    

            print(N)

            #Preprocess
            td_data=signal.detrend(raw_td)

            #FFT
            fft_spec=np.around(np.fft.fft(td_data),2)
            f = k*fs/N
            mag=np.abs(fft_spec)

            
            # Enforce a consistent value for a zero complex number, for some reason it 
            # varies between 0j,-0j, -0-0j, etc., all which give different phase values!
            
            for i in k:
                if mag[i] == 0:
                    fft_spec[i] = 0j

            phase = np.angle(fft_spec)

            #Half Spectrum
            fft_halfspec = fft_spec[k_half]
            mag_half=np.abs(fft_halfspec)
            phase_half=np.angle(fft_halfspec)
            f_half = k_half*fs/N

            #Shift
            f_shift = 0.5 #shift in Hz
            k_shift = int(np.round(f_shift*N/fs)) #shift in indeces

            fft_halfspec_shift = np.roll(fft_halfspec, k_shift)
            fft_halfspec_shift[0]=0j #force zero frequency to be zero magnitude and phase
            fft_halfspec_shift[int(N/2)]=0j #force nyquist freq to be zero magnitude and phase
            mag_half_shift=np.abs(fft_halfspec_shift)
            phase_half_shift=np.angle(fft_halfspec_shift)

            #Reconstruct
            fft_flipped = np.flip(np.conjugate(fft_halfspec_shift[np.arange(1,int(N/2))]))
            fft_spec_shift = np.concatenate((fft_halfspec_shift,fft_flipped))
            td_data_shift = np.real(np.fft.ifft(fft_spec_shift))
            td_data_shift=list(td_data_shift)

            mag_shift=np.abs(fft_spec_shift)
            phase_shift = np.angle(fft_spec_shift)

            # Split data back into seconds

            total_samp=len(td_data_shift)
            total_time=total_samp//fs

            #ts=datetime.strptime(from_ts,FILENAME_DATE_FORMAT)

            for q in range(total_time):
                mod_ts=from_ts+timedelta(seconds=q)
                mod_ts=mod_ts.strftime(FILENAME_DATE_FORMAT)
                new_data=td_data_shift[q*int(fs):(q+1)*int(fs)]
                #print(len(new_data))
                #print(mod_ts)

                future_results.append(db_op.insertSimSensorData(sid, mod_ts, new_data))

                for res in future_results:
                    res.result()
                
            success=True
        t2=datetime.now()
        print('FFT: ',(t2 - t1).total_seconds())

        return success

fs=256 # Sampling rate in Hz
T=5*60 # Period in seconds
df=1/T # Frequency resolution
sid=249 # Selected sensor sid

start_time=datetime(2022, 3, 28, 17, 55, 50)
start_time_utc=utc_tz.localize(start_time)
print('start_time: ',start_time_utc)
end_time=datetime(2022, 3, 28, 20, 0, 0)
end_time_utc=utc_tz.localize(end_time)
print('end_time: ',end_time_utc)
current_time=start_time_utc

while (current_time < end_time_utc):
    from_ts=current_time
    print('from_ts: ', from_ts)
    to_ts=current_time+timedelta(seconds=T)
    print('to_ts: ', to_ts)
    success=calculate_and_save_fft(from_ts, to_ts, sid, df, fs)
    if (success):
        current_time=to_ts
    time.sleep(1)
    #break # REMOVE THIS BEFORE ACTUALLY RUNNING
