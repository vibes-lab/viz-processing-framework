# Calculates average hourly FFT

from __future__ import print_function, division
import os
import time
import json
import math
from datetime import datetime, timedelta
import ftp_operations as f_ops
import datavizapi.sim_cass_ops as db_op
#import datavizapi.cassandra_operations as db_op
from datavizapi.utils.time_utils import (roundToHour, editedTime, formatTime, parseTime, currTime)
import datavizapi.constants as constants
from scipy import signal, integrate
import numpy as np
import pytz
from collections import defaultdict
from datavizapi import AppConfig

config = AppConfig().getConfig()

FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']
data_tz=pytz.timezone(constants.DATA_TZ)
utc_tz=pytz.UTC

future_results=[]
sid_to_data_map = defaultdict(list)

def calculate_and_save_fft(from_ts, to_ts, duration, df):
        
        #Clear all large variables    
        future_results[:] = []
        raw_data=None
        sid_to_data_map.clear()
        fft_freq=None
        full_fft=None
        detrend_data=None
        valid_fft=None
        avg_fft=None
        new_fft=None
        avg_fft=None
        data_sect=None
        fft_comp=None
        fft_amp=None
        success=False

        t0=datetime.now()

        try:
            raw_data = db_op.fetchSensorDataByDay(from_ts, to_ts)
        except Exception, e:
            print(repr(e))
            return success

        t1=datetime.now()
        print('Query: ',(t1 - t0).total_seconds())
        

        for sid, objects in raw_data.items():
            fs = None
            for obj in objects:
                sid_to_data_map[sid].extend(obj['data'])
                if fs is None:
                    fs = len(obj['data'])
 
            # Calculate required samples and windows for desired resolution
            
            N=len(sid_to_data_map[sid]) # Number of samples available            
            dt=1/fs # Time resolution (sec)
            T=1/df # Minimum window period (sec)
            total_t=N/fs # Total amount of time collected over (sec)
            nw=int(math.floor(total_t/T))  # Number of windows possible without overlap (rounded down)
            n=int(math.floor(N/nw)) # Number of samples per window (actual, rounded down)
            n_min=fs*T # Number of samples per window] (minimum to achieve desired df)

            fft_freq = [ x*(fs/n) for x in range(n//2)]
            detrend_data=signal.detrend(sid_to_data_map[sid])
            full_fft=[0]*int(fs//df)
            if (N >= fs):
                for i in range(0, nw):
                    ki=i*n
                    kf=(i+1)*n
                    data_sect=detrend_data[ki:kf]
                    fft_comp=np.fft.fft(data_sect)
                    fft_amp=np.abs(fft_comp/(n//2))
                    new_fft=fft_amp[0:(n//2)]
                    full_fft=[x + y for x, y in zip(full_fft, new_fft)]

                valid_fft=[i for i in full_fft if i != 0]
                avg_fft=[fft/nw for fft in valid_fft]
                
                future_results.append(db_op.insertFFTByHour(sid, list(avg_fft), from_ts))
            
                for res in future_results:
                    res.result()
                
                success=True
            else:
                success=False

        t2=datetime.now()
        print('FFT: ',(t2 - t1).total_seconds())
        
        return success

freq_res=1/(60*5)

duration=60*60 # One hour duration (sec)

start_time=datetime(2022, 3, 20, 0, 0, 0)
start_time_utc=utc_tz.localize(start_time)
print('start_time: ',start_time_utc)
end_time=datetime(2022, 3, 22, 0, 0, 0)
end_time_utc=utc_tz.localize(end_time)
print('end_time: ',end_time_utc)
current_time=start_time_utc

while (current_time < end_time_utc):
    from_ts=current_time
    print('from_ts: ', from_ts)
    to_ts=current_time+timedelta(seconds=duration)
    print('to_ts: ', to_ts)
    success=calculate_and_save_fft(from_ts, to_ts, duration, freq_res)
    if (success):
        current_time=to_ts
    time.sleep(1)

