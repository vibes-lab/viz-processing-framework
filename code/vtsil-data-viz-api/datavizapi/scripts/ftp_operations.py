# Operations to interact with FTP server and basic operations used in processing scripts

import os
import time
from ftplib import FTP
from pytz import timezone
import pytz
from datetime import datetime, timedelta
from datavizapi import AppConfig

config = AppConfig().getConfig()

ftp_config = config['file_sync']

TEMP_FILE_PATH = ftp_config['temp_file_path']
CREDENTIALS_FILE = ftp_config['credentials_file_path']
DATE_TIME_FORMAT = ftp_config['file_name_format']
DESTINATION_DIR = ftp_config['destination_dir']
SCRIPT_BASE_PATH = os.path.dirname(os.path.realpath(__file__))


# Read password from credential file
def getUsernamePassword():
    lines = open(CREDENTIALS_FILE, 'r').readlines()
    lines = map(lambda l: l.strip('\n '), lines)
    return (lines[0], lines[1])

# Get timestamp for last read file
def getLastReadDate():
    resp = None
    try:
        with open(TEMP_FILE_PATH, 'r') as f:
            line = f.readlines()[0]
            line = line.strip('\n ')
            resp = datetime.strptime(line, DATE_TIME_FORMAT)
    except IOError:
        pass
    return resp

# Connect to FTP server and login
def connectAndGetFTP(host=ftp_config['ftp_remote_ip'], port=ftp_config['ftp_remote_port']):
    username, password = getUsernamePassword()
    ftp = FTP()
    ftp.set_debuglevel(0)
    ftp.connect(host, port)
    ftp.login(username, password)
    ftp.set_pasv(True)
    return ftp


# Get name of remote directory from FTP server
def getRemoteDir(ftp,latest_name,last_length):
    names = ftp.nlst()
    length=len(names)
    if (length>last_length):
        latest_time = None
        latest_name = None
        for name in names:
            time = ftp.voidcmd("MDTM " + name)
            if (latest_time is None) or (time > latest_time):
                latest_name = name
                latest_time = time
        last_length=length
    return latest_name, last_length


# Extract and format file metadata from remote dir
def getFileMetadata(ftp):
    this = getFileMetadata
    this.data = []

    def cb(d):
        getFileMetadata.data.append(d)
    ftp.retrlines('LIST', cb)

    this.data = map(lambda d: d.split(), this.data)
    this.data = map(lambda d: [d[0] + " " + d[1]] + d[2:], this.data)
    return this.data


# Fetch most recent configuration file from remote dir
def fetchConfigFile(ftp,last_config):
    this = fetchConfigFile
    this.files = []
    ftp.retrlines('LIST', lambda d: this.files.append(d))
    this.files = map(lambda x: x.split()[-1], this.files)
    config_fname=filter(lambda x: x.endswith('config.txt'), this.files)[-1]
    try:
        if (config_fname!=last_config):
            ofile = open(os.path.join(SCRIPT_BASE_PATH, config_fname), 'wb')
            ftp.retrbinary('RETR ' + config_fname, ofile.write)
            ofile.close()
            last_config=config_fname
    except NameError:
            ofile = open(os.path.join(SCRIPT_BASE_PATH, config_fname), 'wb')
            ftp.retrbinary('RETR ' + config_fname, ofile.write)
            ofile.close()
            last_config=config_fname
    return config_fname, last_config


# Filter records to only include files newer than .lastread
def filterUnfetchedRecords(last_date, records):
    records = filter(lambda x: not x[-1].endswith('txt'), records)
    if last_date is None:
        return records
    return filter(
        lambda r: datetime.strptime(r[-1][:-3], DATE_TIME_FORMAT) > last_date,
        records)

# Write file from FTP server to local server
def fetchFiles(ftp, records):
    for r in records:
        retries = 0
        while True:
            try:
                ofile = open(os.path.join(SCRIPT_BASE_PATH, r), 'wb')
                ftp.retrbinary('RETR ' + r, ofile.write)
                ofile.close()
                break
            except Exception:
                retries += 1
                if retries > 5:
                    break
                else:
                    time.sleep(0.2)
                pass

# Update the timestamp in .lastread
def updateLastReadDate(records):
    if len(records) == 0:
        return
    dates = map(
        lambda r: datetime.strptime(r[-1][:-3], DATE_TIME_FORMAT), records)
    dates = sorted(dates, reverse=True)
    with open(TEMP_FILE_PATH, 'w') as f:
        f.write(datetime.strftime(dates[0], DATE_TIME_FORMAT))

# Calculates time offset between time data is saved and time for a step to complete
def timeOffsetList(records):
    if len(records) == 0:
        return
    data=map(lambda x: x[-1], records)
    for d in data:
        file_name=d.rsplit( ".", 1 )[ 0 ]
        time=datetime.strptime(file_name, DATE_TIME_FORMAT)+timedelta(hours=4) # 5 during DST, 4 otherwise
        current=datetime.now()
        offset=current-time
        seconds=offset.total_seconds()
        print(str(seconds))

# Removes extension from filename, converts to datetime and compares to current time
def timeOffset(f_name):
    file_name=f_name.rsplit( ".", 1 )[ 0 ]
    time=datetime.strptime(file_name, DATE_TIME_FORMAT)+timedelta(hours=4) # 5 durign DST, 4 otherwise
    current=datetime.now()
    offset=current-time
    seconds=offset.total_seconds()
    print(str(seconds))


# Removes extension from filename, converts to datetime and compares to current time
def timeOffsetTest(f_name):
    file_name=f_name.rsplit( ".", 1 )[ 0 ]
    time=datetime.strptime(file_name, DATE_TIME_FORMAT)+timedelta(hours=4) # 5 durign DST, 4 otherwise
    current=datetime.now()
    offset=current-time
    seconds=offset.total_seconds()
    return str(seconds) 
