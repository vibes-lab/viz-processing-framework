from __future__ import print_function, division
import os
import time
import json
import math
import matplotlib 
matplotlib.use("agg")
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import datavizapi.cassandra_operations as db_op
import ftp_operations as f_ops
from datavizapi.utils.commons import splitRangeInDays, splitRangeInHours, splitRangeInMinutes, splitRangeInSeconds
import datavizapi.utils.time_utils as time_utils
import datavizapi.constants as constants
from scipy import signal, integrate
import numpy as np
from scipy.fftpack import fft
import pytz
from collections import defaultdict
from datavizapi import AppConfig

#FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']
data_tz=constants.DATA_TZ
utc_tz=pytz.UTC

sid_to_data_map = defaultdict(list)

def split_and_avg_fft(from_ts, to_ts):
        t0=datetime.now()
        
        date = time_utils.roundToDay(from_ts)
        print('date: ', date)

        try:
            raw_data =db_op.fetchFFTForDay(date)
        except Exception, e:
            print(repr(e))
            success=False
            return success

        t1=datetime.now()
        print('Query: ',(t1 - t0).total_seconds())

        future_results = []
        success=True

        for sid, objects in raw_data.items():
            print(sid)
            for obj in objects:
                sid_to_data_map[sid].extend(obj['fft'])
           
            # Calculate required samples and windows for desired resolution
            
            N=len(sid_to_data_map[sid]) # Number of samples available            
            n=512 # Number of samples per hour 0.25 freq res
            H=N//n
            fft_freq=[ x*0.25 for x in range(n)]
            #print(fft_freq)

            #print(N)

            full_fft=[0]*3000

            for i in range(0, H):
                #print(i*n)
                #print((i+1)*n)
                new_fft=sid_to_data_map[sid][i*n:(i+1)*n]
                #print('len new_fft: ',len(new_fft))
                full_fft=[x + y for x, y in zip(full_fft, new_fft)]

            valid_fft=[i for i in full_fft if i != 0]
            avg_fft=[fft/H for fft in valid_fft]

            future_results.append(db_op.insertFFTByDay(sid, list(avg_fft), to_ts))
            
            for res in future_results:
                res.result()
            
            #print('avg_fft: ',len(avg_fft))

                #future_results.append(db_op.insertFFTByHour(sid, list(avg_fft), to_ts))
            
                #for res in future_results:
                #    res.result()
            
            
            
            #save_name='fft_plot_'+str(sid)+'.png'
            #fig, ax = plt.subplots()
            #ax.plot(fft_freq, avg_fft)
            #plt.savefig(save_name)         
        #success=True

        t2=datetime.now()
        print('FFT: ',(t2 - t1).total_seconds())

        #save_name='fft_plot_'+str(sid)+'.png'
        #fig, ax = plt.subplots()
        #ax.plot(fft_freq, avg_fft)
        #plt.savefig(save_name)

            #save_name='fft_data_'+str(sid)+'.csv'
        #np.savetxt(save_name, avg_fft, delimiter=',')

        return success

def avg_day_fft(from_ts, to_ts):
    try:
        date = time_utils.roundToDay(from_ts)
        print('date: ', date)

        results=db_op.fetchFFTByHour(from_ts, to_ts, sid)
        num_samples=len(results[sid])
        
        #print(num_samples)

        full_fft=[0]*2000
        max_fft=0

        for i in range(0, num_samples):
            new_fft=results[sid][i]['fft']
            #print('fft_len: ',len(new_fft))
            full_fft=[x + y for x, y in zip(full_fft, new_fft)]
            max_fft=max(max_fft, max(new_fft))

        valid_fft=[i for i in full_fft if i != 0]

        avg_fft=[fft/num_samples for fft in valid_fft]
        
        #print('avg_len: ',len(avg_fft))
        success=True
    except Exception, e:
        print(repr(e))
        success=False
    return success

freq_res=0.25

duration=60*60*24 # One day duration (sec)
start_time=datetime(2022, 3,16,0, 0, 0)
start_time_utc=utc_tz.localize(start_time)
print('start_time: ',start_time_utc)
end_time=datetime(2022, 3, 20, 0, 0, 0)
end_time_utc=utc_tz.localize(end_time)
print('end_time: ',end_time_utc)
current_time=start_time_utc

while (current_time < end_time_utc):
    from_ts=current_time
    print('from_ts: ', from_ts)
    to_ts=current_time+timedelta(seconds=duration)
    print('to_ts: ', to_ts)
    #success=True
    #success=avg_day_fft(from_ts, to_ts)
    success= split_and_avg_fft(from_ts, to_ts)
    #print('success: ', success)
    if (success):
        current_time=to_ts
    time.sleep(1)
