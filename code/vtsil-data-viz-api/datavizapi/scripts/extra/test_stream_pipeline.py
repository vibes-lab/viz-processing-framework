import os
from datavizapi.scripts.h5_to_kafka import readH5, putRawDataInQueue

SCRIPT_BASE_PATH = os.path.dirname(os.path.realpath(__file__))

server = 'node0'
sample_rate = 1024
file_name = '2019-04-09_06_38_17.h5'

data = readH5(file_name, sample_rate)
putRawDataInQueue(file_name[-3], data, sample_rate)
