import sys
from multiprocessing import Pool,Process
import os
from datetime import datetime
import json

def fetch(arg):
    print("started {0}".format(arg))
    a,b=arg
    return os.popen('python /home/vtsil/vtsil-data-viz-api/datavizapi/scripts/fetch_data.py {0},{1}'.format(a,b)).read()


    N = 10
    p=Pool(N)
    strts = range(N)
    total = [N]*N
    ranges=zip(strts,total)
    response = {}
    print(datetime.now())
    map(lambda x: response.update(json.loads(x)), p.map(fetch, ranges))
    print(len(response))
    print(datetime.now())
