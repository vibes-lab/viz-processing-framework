import os
import time
import ftp_operations as f_ops
from datavizapi import AppConfig
from datetime import datetime, timedelta
from scipy import signal, integrate
import datavizapi.cassandra_operations as db_op
from datavizapi.utils.time_utils import (
    editedTime, formatTime, parseTime, currTime)
from datetime import datetime, timedelta
import pytz
from collections import defaultdict

config = AppConfig().getConfig()

FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']

sid_to_data_map = defaultdict(list)

def parseSampleRateFromConfig(fname):
    sample_rate = None
    with open(fname, 'r') as f:
        lines = f.readlines()
        sample_rate_line = \
            filter(lambda x: x.startswith('real sample rate'), lines)[0]
        sample_rate = sample_rate_line.split('=')[-1].strip(' \n\r')
    return sample_rate

def get_all_sensor_ids():
    objs = db_op.getSensorInfoAll()
    ids = [x.id for x in objs]
    return ids

def power_spectrum(input_arr, sampling_f=256.0,
                   scaling='density', window='hann'):
    window = signal.get_window(window, sampling_f)
    return signal.welch(
        input_arr, fs=sampling_f, scaling=scaling, window=window, nperseg=sampling_f)

def calculate_and_save_psd(from_ts, to_ts, duration):
    raw_data = db_op.fetchSensorDataAsync(from_ts, to_ts)
    future_results = []
    success=False
    num_sens=len(raw_data.items())
    #print(num_sens)
    for sid, objects in raw_data.items():
        sampling_freq = None
        for obj in objects:
            sid_to_data_map[sid].extend(obj['data'])
            if sampling_freq is None:
                sampling_freq = len(obj['data'])
        if len(sid_to_data_map[sid]) < duration * sampling_freq:
            continue
        if len(sid_to_data_map[sid]) > duration * sampling_freq:
            sid_to_data_map[sid] = sid_to_data_map[sid][sampling_freq:]
        if len(sid_to_data_map[sid]) == duration * sampling_freq:
            freqs, power = power_spectrum(sid_to_data_map[sid], sampling_f=sampling_freq)
            average_power = integrate.simps(power)
            future_results.append(db_op.insertPSDAsync(sid, average_power, list(power), to_ts))
            success=True
        else:
            return success
    for res in future_results:
        res.result()
    return success, num_sens

time_length=input('Acquisition length (seconds): ')
start_time = editedTime(f_ops.getLastReadDate(),seconds=time_length)
local_tz = pytz.timezone('US/Eastern')
#start_time = datetime(2021, 7, 27, 15, 40, 44)
start_time_local = local_tz.localize(start_time)
start_time_utc = start_time_local.astimezone(pytz.utc)
#print(start_time_utc)
start_time_utc=start_time_utc +timedelta(seconds=time_length)
#print(start_time_utc)

while True:
    s = datetime.now()
    success, num_sens = calculate_and_save_psd(start_time_utc, start_time_utc, 1)
    if success:
        #print(num_sens)
        try:
            if num_sens == last_num:
                start_local = start_time_utc.astimezone(local_tz)
                #print('Sens: ', num_sens)
                print(start_local)
                print((datetime.now() - s).total_seconds())
                #f_ops.timeOffset(formatTime(start_local))
                start_time_utc = start_time_utc + timedelta(seconds=1)
            else:
                last_num=num_sens
                time.sleep(0.05)
        except Exception:
            last_num=num_sens
            time.sleep(0.05)
