import os
import time
import json
from datetime import datetime, timedelta
from confluent_kafka import Consumer
import datavizapi.cassandra_operations as db_op
import ftp_operations as f_ops
from datavizapi.utils.time_utils import (
    editedTime, formatTime, parseTime, currTime)
import datavizapi.constants as constants
from scipy import signal, integrate
import numpy as np
from scipy.fftpack import fft
import pytz
from collections import defaultdict
from datavizapi import AppConfig

config = AppConfig().getConfig()

kafka_config = config['kafka']

FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']
data_tz=constants.DATA_TZ

c = Consumer({
    'bootstrap.servers': ",".join(config['kafka']['servers']),
    'group.id': config['kafka']['consumer_fft']['group'],
    'auto.offset.reset': config['kafka']['consumer_fft']['offset']})

c.subscribe([config['kafka']['consumer_fft']['topic']])

sid_to_data_map = defaultdict(list)

def power_spectrum(input_arr, sampling_f=256.0,
                   scaling='density', window='hann'):
    window = signal.get_window(window, sampling_f)
    return signal.welch(
        input_arr, fs=sampling_f, scaling=scaling, window=window, nperseg=sampling_f)


def calculate_and_save_fft_psd(from_ts, to_ts, duration,sid):
        raw_data=db_op.fetchSensorByIDAsync(from_ts, to_ts, sid)
        future_results = []
        future_results_raw = []
        future_results_psd=[]
        success=False
        #print(raw_data)
        check1=str(raw_data.items())
        check2="0"
        for sid, objects in raw_data.items():
            sampling_freq = None
            for obj in objects:
                sid_to_data_map[sid].extend(obj['data'])
                if sampling_freq is None:
                    sampling_freq = len(obj['data'])
            
            check2=str(len(sid_to_data_map[sid]))
            
            if len(sid_to_data_map[sid]) < duration * sampling_freq:
                continue
            if len(sid_to_data_map[sid]) > duration * sampling_freq:
                sid_to_data_map[sid] = sid_to_data_map[sid][sampling_freq:]
            if len(sid_to_data_map[sid]) == duration * sampling_freq:
                # Add detrended FFT
                detrend_data=signal.detrend(sid_to_data_map[sid])
                y_detrend=fft(detrend_data)
                fft_detrend=2.0/sampling_freq * np.abs(y_detrend[0:sampling_freq//2])
                future_results.append(db_op.insertFFTAsync(sid, list(fft_detrend), to_ts))
                # Add raw FFT
                yf = fft(sid_to_data_map[sid])
                fft_y=2.0/sampling_freq * np.abs(yf[0:sampling_freq//2])
                future_results_raw.append(db_op.insertFFTRaw(sid, list(fft_y), to_ts))
                # Add raw PSD
                freqs, power = power_spectrum(sid_to_data_map[sid], sampling_f=sampling_freq)
                average_power = integrate.simps(power)
                future_results_psd.append(db_op.insertPSDAsync(sid, average_power, list(power), to_ts))
                success=True
            else:
                return success, check1, check2
        for res in future_results:
            res.result()
        for res in future_results_raw:
            res.result()
        for res in future_results_psd:
            res.result()
        return success, check1, check2

#full_sens=constants.NUM_SENS
#time_length= constants.ACQ_LEN # Number of seconds for acquisition
duration= 1 # Number of seconds for raw data (sec)

while True:
    msg = c.poll(0)
    if msg is None:
        continue
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        continue
    msg = json.loads(msg.value())
    sid = msg['sid']
    ts=msg['ts']
    start_time_local = parseTime(ts, data_tz, FILENAME_DATE_FORMAT)
    start_time_utc= start_time_local.astimezone(pytz.utc)
    success, check1, check2 = calculate_and_save_fft_psd(start_time_utc, start_time_utc, duration, sid)
    if success:
        continue
        #print(start_time_local)
    else:
        error_str='FAILED: '+start_time_utc.strftime(FILENAME_DATE_FORMAT)+', raw_data.items: '+check1+ ', sid: '+sid+' , num_samp: '+check2+ '\n'
        print(error_str)
        with open("log_fft.txt", "a") as myfile:
                    myfile.write(error_str)
