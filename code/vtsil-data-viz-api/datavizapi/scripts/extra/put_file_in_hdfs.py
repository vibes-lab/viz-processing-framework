import os
import json
import time
from datetime import datetime, timedelta
from confluent_kafka import Consumer, Producer
import ftp_operations as f_ops
from datavizapi import AppConfig

config = AppConfig().getConfig()
SCRIPT_BASE_PATH = os.path.dirname(os.path.realpath(__file__))

p = Producer({
    'bootstrap.servers': ','.join(config['kafka']['servers']),
    'default.topic.config': {'acks': '1'},
    'retries': config['kafka']['producer_hdfs']['retries'],
    'max.in.flight.requests.per.connection': config['kafka']['producer_hdfs']['max_in_flight']})

c = Consumer({
    'bootstrap.servers': ','.join(config['kafka']['servers']),
    'group.id': config['kafka']['consumer_h5']['group'],
    'auto.offset.reset': config['kafka']['consumer_h5']['offset']})

c.subscribe([config['kafka']['consumer_h5']['topic']])


def fetchFileFTP(fname):
    ftp = f_ops.connectAndGetFTP()
    try:
        remote_dir,num_dirs=f_ops.getRemoteDir(ftp,remote_dir,num_dirs)
        ftp.cwd(remote_dir)
    except Exception: # Exception for initial run of code when variables are first defined
        remote_dir,num_dirs=f_ops.getRemoteDir(ftp,None,None)
        ftp.cwd(remote_dir)
    f_ops.fetchFiles(ftp, [fname])
    ftp.quit()
    ftp.close()


while True:
    #t0=datetime.now()
    msg = c.poll(0)
    if msg is None:
        continue
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        continue
    msg = json.loads(msg.value())
    fname = msg['file_name']
    sample_rate = msg['sample_rate']
    time.sleep(10) #Uncomment this when doing second level file saving
    try:
        fetchFileFTP(fname)
        os.system("hdfs dfs -copyFromLocal {0}/{1} /user/vtsil/testfiles/".format(
            SCRIPT_BASE_PATH, fname))

        print(fname)
        payload = {
            'file_name': fname,
            'sample_rate': sample_rate
        }
        os.system("rm {0}/{1}".format(SCRIPT_BASE_PATH, fname))
        p.produce(
            config['kafka']['consumer_hdfs']['topic'],
            json.dumps(payload))
        f_ops.timeOffset(fname)
    except Exception, e:
        print(e)
    p.poll(0)
    
    #t1=datetime.now()
    #print((t1 - t0).total_seconds())
