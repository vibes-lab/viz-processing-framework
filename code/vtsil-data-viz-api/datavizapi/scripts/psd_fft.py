# Calculate PSD, total power, and FFT and add to correspodning Cassandra tables

from __future__ import print_function, division
import os
import time
import json
from confluent_kafka import Consumer
import ftp_operations as f_ops
import datavizapi.cassandra_operations as db_op
from datavizapi.utils.time_utils import (roundToHour, editedTime, formatTime, parseTime, currTime)
import datavizapi.constants as constants
from scipy import signal, integrate
import numpy as np
import pytz
from datavizapi import AppConfig

config = AppConfig().getConfig()

kafka_config = config['kafka']

FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']
data_tz=constants.DATA_TZ

c = Consumer({
    'bootstrap.servers': ",".join(config['kafka']['servers']),
    'group.id': config['kafka']['consumer_fft']['group'],
    'auto.offset.reset': config['kafka']['consumer_fft']['offset']})

c.subscribe([config['kafka']['consumer_fft']['topic']])

future_results_fft=[]
future_results_psd=[]

# Calculates PSD
def power_spectrum(input_arr, sampling_f=256.0, scaling='density', window='hann'):
    window = signal.get_window(window, sampling_f)
    return signal.welch(
        input_arr, fs=sampling_f, scaling=scaling, window=window, nperseg=sampling_f)

def calculate_and_save_fft(raw_data, ts, sid):
        
        #Clear all large variables    
        future_results_fft[:]=[]
        future_results_psd[:]=[]
        freqs=None
        power=None
        fft_comp=None
        fft=None
        success=False
       
        data=signal.detrend(raw_data)

        ts = parseTime(ts, data_tz, FILENAME_DATE_FORMAT)
        ts= ts.astimezone(pytz.utc)

        # Calculate sample rate and detrend
        fs=len(data)

        # Calculate and add raw FFT to Cass
        fft_comp=np.fft.fft(data)
        fft=np.abs(fft_comp/(fs//2))[0:(fs//2)]
        future_results_fft.append(db_op.insertFFTAsync(sid, list(fft), ts))

        # Calculate and add PSD to Cass
        freqs, power = power_spectrum(data, sampling_f=fs)
        average_power = integrate.simps(power)
        future_results_psd.append(db_op.insertPSDAsync(sid, average_power, list(power), ts))
        
        for res in future_results_fft:
            res.result()
        for res in future_results_psd:
            res.result()
        
        return

while True:
    # Poll topic and continuously calculate PSD and FFT and add to Cassandra
    msg=c.poll(0)
    if msg is None:
        continue
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        continue
    msg = json.loads(msg.value())
    sid = msg['sid']
    ts=msg['ts']
    raw_data=msg['d']
    calculate_and_save_fft(raw_data, ts, sid)
    print(ts)
