# Flatten HDF5 file, split data into 1 secon increments, and add to topics based on SID

import h5py
import os
import json
import time
from datetime import datetime, timedelta
from confluent_kafka import Consumer, Producer
import datavizapi.cassandra_operations as db_op
import ftp_operations as f_ops
from datavizapi import AppConfig

config = AppConfig().getConfig()
SCRIPT_BASE_PATH = os.path.dirname(os.path.realpath(__file__))
SENSOR_DATE_TIME_FORMAT = '%Y-%m-%d %H:%M:%S:%f'
FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']
daq_name_id_map = {}

p = Producer({
    'bootstrap.servers': ",".join(config['kafka']['servers']),
    'default.topic.config': {'acks': '1'},
    'retries': config['kafka']['producer_h5']['retries'],
    'max.in.flight.requests.per.connection': 1,
    'linger.ms': config['kafka']['producer_h5']['linger_ms']})

c = Consumer({
    'bootstrap.servers': ','.join(config['kafka']['servers']),
    'group.id': config['kafka']['consumer_h5']['group'],
    'auto.offset.reset': config['kafka']['consumer_h5']['offset']})

c.subscribe([config['kafka']['consumer_h5']['topic']])

# Get DAQ name based on SID
def daqNameIdMap():
    if daq_name_id_map:
        return daq_name_id_map
    sensor_info_objects = db_op.getSensorInfoAll()
    for obj in sensor_info_objects:
        daq_name_id_map[obj.daq_name] = obj.id
    return daq_name_id_map

# Connect to FTP server and fetch file
def fetchFileFTP(fname):
    ftp = f_ops.connectAndGetFTP()
    try:
        remote_dir,num_dirs=f_ops.getRemoteDir(ftp,remote_dir,num_dirs)
        ftp.cwd(remote_dir)
    except Exception: # Exception for initial run of code when variables are first defined
        remote_dir,num_dirs=f_ops.getRemoteDir(ftp,None,None)
        ftp.cwd(remote_dir)
    f_ops.fetchFiles(ftp, [fname])
    ftp.quit()
    ftp.close()

# Flatten HDF5 file
def readH5(fname, sample_rate):
    f = h5py.File(SCRIPT_BASE_PATH + '/' + fname, 'r')
    data = f['/Data']
    keys = data.keys()
    flattened_data = map(lambda k: (k, data.get(k)[()].flatten()), keys)
    f.close()
    return flattened_data

# Split data into 1 second increments, and add to topics based on SID
def putRawDataInQueue(ts, data, sample_rate):
    total_samples=data[0][1].size
    save_int=total_samples/int(sample_rate)
    for k, v in data:
        sid = daqNameIdMap()[k[5:]]
        data = v.tolist()
        ts_dt=datetime.strptime(ts,FILENAME_DATE_FORMAT)
        for q in range(save_int):
            mod_ts=ts_dt+timedelta(seconds=q)
            mod_ts=mod_ts.strftime(FILENAME_DATE_FORMAT)
            new_data=data[q*int(sample_rate):(q+1)*int(sample_rate)]
            payload = {
                "ts": mod_ts,
                "daq_name": k[5:],
                "d": new_data,
                "f": sample_rate,
                "sid": sid
            }
            p.produce(
                "rawData_" + str(sid),
                value=json.dumps(payload))
            p.poll(0)


while True:
    msg = c.poll(0)
    if msg is None:
        continue
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        continue
    msg = json.loads(msg.value())
    fname = msg['file_name']
    sample_rate = msg['sample_rate']
    ts = fname[:-3]
    time.sleep(10) # Sleep time should be proportional to record length (record length/6 is a good benchmark)
    try:
        fetchFileFTP(fname) 
        data = readH5(fname, sample_rate)
        putRawDataInQueue(ts.split('/')[-1], data, sample_rate)
        print(fname)
        os.system("rm {0}/{1}".format(SCRIPT_BASE_PATH, fname)) # Remove file after data is added to topic
        #f_ops.timeOffset(fname)
    except Exception, e:
        print(e)
    p.poll(0)
