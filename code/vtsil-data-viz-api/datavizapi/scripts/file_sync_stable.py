# Polls FTP server for new files and adds the filenames to incomingFiles topic

import os
import time
import json
import ftp_operations as f_ops
from datetime import datetime, timedelta
from confluent_kafka import Producer
from datavizapi import AppConfig

config = AppConfig().getConfig()

p = Producer({'bootstrap.servers': ",".join(config['kafka']['servers'])})

SCRIPT_BASE_PATH = os.path.dirname(os.path.realpath(__file__))

# Extracts sample rate from most recent config file in the data folder

def parseSampleRateFromConfig(fname):
    sample_rate = None
    with open(fname, 'r') as f:
        lines = f.readlines()
        sample_rate_line = \
            filter(lambda x: x.startswith('real sample rate'), lines)[0]
        sample_rate = sample_rate_line.split('=')[-1].strip(' \n\r')
    return sample_rate

# Adds filenames and sample rate to topic

def putNewfilenames(data, sample_rate):
    if len(data) == 0:
        return
    data = map(lambda x: x[-1], data)
    for d in data:
        payload = {
            "sample_rate": sample_rate,
            "file_name": d
        }
        p.produce(
            config['kafka']['consumer_h5']['topic'],
            json.dumps(payload))
    p.poll(0)

last_config=None

# Connects to FTP server and finds remote directory where files are being saved
ftp = f_ops.connectAndGetFTP()
remote_dir,num_dirs=f_ops.getRemoteDir(ftp,None,None)
ftp.cwd(remote_dir)
config_fname, last_config = f_ops.fetchConfigFile(ftp,None)
sample_rate = parseSampleRateFromConfig(config_fname)

while True:
    last_date = f_ops.getLastReadDate()
    ftp = f_ops.connectAndGetFTP()
    try:
        last_num=num_dirs
        remote_dir,num_dirs=f_ops.getRemoteDir(ftp,remote_dir,num_dirs)
        ftp.cwd(remote_dir)
        # Reset config settings if new directory was made on FTP server
        if (num_dirs != last_num):
            config_fname, last_config = f_ops.fetchConfigFile(ftp,None)
            sample_rate = parseSampleRateFromConfig(config_fname)
        else:
            config_fname, last_config = f_ops.fetchConfigFile(ftp,last_config)
        records = f_ops.getFileMetadata(ftp) # Get metadata for all files in remote dir
        records = f_ops.filterUnfetchedRecords(last_date, records) # Filter files to include newer than .lastread
        f_ops.updateLastReadDate(records) # Update .lastread
        ftp.quit() 
        ftp.close()
        putNewfilenames(records, sample_rate) # Add filenames to kafka
        f_ops.timeOffsetList(records)
        time.sleep(0.005)
    except Exception, e:
        print(e)
        ftp.quit()
        ftp.close()
