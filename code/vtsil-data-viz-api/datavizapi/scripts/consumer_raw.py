# Code to add raw acceleration data to Cassandra table

from confluent_kafka import Consumer
from confluent_kafka import Producer
import datavizapi.cassandra_operations as db_op
import ftp_operations as f_ops
from datetime import datetime, timedelta
from datavizapi import AppConfig
import json

config = AppConfig().getConfig()
kafka_config = config['kafka']

p = Producer({
    'bootstrap.servers': ",".join(config['kafka']['servers']),
    'default.topic.config': {'acks': '1'},
    'retries': config['kafka']['producer_fft']['retries'],
    'max.in.flight.requests.per.connection': 1,
    'linger.ms': config['kafka']['producer_fft']['linger_ms']})

c = Consumer({
    'bootstrap.servers': ",".join(kafka_config['servers']),
    'group.id': kafka_config['consumer_psd']['group'],
    'auto.offset.reset': kafka_config['consumer_psd']['offset']})

c.subscribe([kafka_config['consumer_psd']['topic']])


while True:
    # Read message from rawData topics
    msg = c.poll(0)
    if msg is None:
        continue
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        continue
    msg = json.loads(msg.value())
    sid = msg['sid']
    ts=msg['ts']
    print(sid)
    print(ts)
    
    # Insert data into Cassandra
    db_op.insertSensorData(sid, ts, msg['d'])
    
    # Write data to topic to calculate FFT, PSD, total power
    payload = {
        "ts": ts,
        "sid": sid,
        "d": msg['d']
            }
    p.produce(
        "fftData_" + str(sid),
        value=json.dumps(payload))
    p.poll(0)
