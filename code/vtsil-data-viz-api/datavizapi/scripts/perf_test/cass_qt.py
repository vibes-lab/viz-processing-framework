import sys
import time
from datetime import datetime, timedelta
sys.path.append('/home/vtsil/vtsil-data-viz-api')
import datavizapi.cassandra_operations as db_op


if __name__ == '__main__':
    start = datetime(2022, 3, 30, 5, 0, 0)
    res_file = open('cassandra_q_time_sync.csv', 'w')
    res_file.write("Time Length (seconds), Query Time (seconds)\n")
    #tlengths= [1, 2, 5, 10, 30, 60, 300, 600]
    tlengths= [1800, 3600, 18000, 36000, 86400]
    #tlengths = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096]
    for tl in tlengths:
        i=1
        for i in range(10):
            s = start
            e = s + timedelta(seconds=tl)
            st = datetime.now()
            resp = db_op.fetchSensorData(s, e)
            et = datetime.now()
            time_taken = (et - st).total_seconds()
            print(tl, time_taken)
            res_file.write("{0},{1}\n".format(tl, time_taken))
            resp=None
            sid_list=None
            time.sleep(5)
