from confluent_kafka import Consumer
from confluent_kafka import Producer
import datavizapi.cassandra_operations as db_op
import ftp_operations as f_ops
from datetime import datetime, timedelta
from datavizapi import AppConfig
import json

config = AppConfig().getConfig()
kafka_config = config['kafka']

FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']

p = Producer({
    'bootstrap.servers': ",".join(config['kafka']['servers']),
    'default.topic.config': {'acks': '1'},
    'retries': config['kafka']['producer_fft']['retries'],
    'max.in.flight.requests.per.connection': 1,
    'linger.ms': config['kafka']['producer_fft']['linger_ms']})

c = Consumer({
    'bootstrap.servers': ",".join(kafka_config['servers']),
    'group.id': kafka_config['consumer_psd']['group'],
    'auto.offset.reset': kafka_config['consumer_psd']['offset']})

c.subscribe([kafka_config['consumer_psd']['topic']])



while True:
    t0=datetime.now()
    msg = c.poll(0)
    if msg is None:
        continue
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        continue
    msg = json.loads(msg.value())
    last_time = msg['last_time']
    lt_dt=datetime.strptime(last_time, FILENAME_DATE_FORMAT)
    k_cs=str((t0-lt_dt).total_seconds())
    sid = msg['sid']
    ts=msg['ts']
    print(sid, ts)
    #print(ts)
    db_op.insertSensorData(sid, ts, msg['d'])
    t1=datetime.now()
    op_time=(t1 - t0).total_seconds()
    time_str=t1.strftime(FILENAME_DATE_FORMAT)
    #seconds = f_ops.timeOffsetTest(ts)
    payload = {
        "ts": ts,
        "sid": sid,
        "d": msg['d'],
        "fs_k": msg['fs_k'],
        "fs_ot": msg['fs_ot'],
        "k_cs": k_cs,
        "k_ot": msg['k_ot'],    
        "cs_ot": op_time,
        "last_time": time_str
        }
    p.produce(
        "fftData_" + str(sid),
        value=json.dumps(payload))
    p.poll(0)
    #t1=datetime.now()
    #op_time=(t1 - t0).total_seconds()
    #seconds, file_name = timeOffsetTest(ts)
    #res_file.write("{0},{1}\n".format(file_name, seconds, op_time))
