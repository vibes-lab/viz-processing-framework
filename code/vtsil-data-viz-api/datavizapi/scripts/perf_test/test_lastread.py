import os
import time
from ftplib import FTP
from pytz import timezone
import pytz
from datetime import datetime, timedelta
from datavizapi import AppConfig

config = AppConfig().getConfig()

ftp_config = config['file_sync']

TEMP_FILE_PATH = ftp_config['temp_file_path']
CREDENTIALS_FILE = ftp_config['credentials_file_path']
DATE_TIME_FORMAT = ftp_config['file_name_format']
DESTINATION_DIR = ftp_config['destination_dir']
SCRIPT_BASE_PATH = os.path.dirname(os.path.realpath(__file__))

date=datetime.now()+timedelta(hours=-4,seconds=30)
with open(TEMP_FILE_PATH, 'w') as f:
        f.write(datetime.strftime(date, DATE_TIME_FORMAT))
