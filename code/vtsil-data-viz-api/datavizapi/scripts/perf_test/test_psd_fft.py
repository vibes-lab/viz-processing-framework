from __future__ import print_function, division
import os
import time
import json
from confluent_kafka import Consumer, Producer
import ftp_operations as f_ops
import datavizapi.cassandra_operations as db_op
from datavizapi.utils.time_utils import (roundToHour, editedTime, formatTime, parseTime, currTime)
import datavizapi.constants as constants
from scipy import signal, integrate
import numpy as np
import pytz
from datetime import datetime, timedelta
from datavizapi import AppConfig

config = AppConfig().getConfig()

kafka_config = config['kafka']

FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']
data_tz=constants.DATA_TZ

p = Producer({'bootstrap.servers': ",".join(config['kafka']['servers'])})

c = Consumer({
    'bootstrap.servers': ",".join(config['kafka']['servers']),
    'group.id': config['kafka']['consumer_fft']['group'],
    'auto.offset.reset': config['kafka']['consumer_fft']['offset']})

c.subscribe([config['kafka']['consumer_fft']['topic']])

future_results_fft=[]
future_results_psd=[]


def power_spectrum(input_arr, sampling_f=256.0, scaling='density', window='hann'):
    window = signal.get_window(window, sampling_f)
    return signal.welch(
        input_arr, fs=sampling_f, scaling=scaling, window=window, nperseg=sampling_f)

def calculate_and_save_fft(raw_data, ts_str, sid, t0):
        
        #Clear all large variables    
        future_results_fft[:]=[]
        future_results_psd[:]=[]
        freqs=None
        power=None
        fft_comp=None
        fft=None
        success=False
       
        data=signal.detrend(raw_data)
        
        ts = parseTime(ts_str, data_tz, FILENAME_DATE_FORMAT)
        ts= ts.astimezone(pytz.utc)

        # Calculate smaple rate and detrend
        fs=len(data)

        # Calculate and add raw FFT
        fft_comp=np.fft.fft(data)
        fft=np.abs(fft_comp/(fs//2))[0:(fs//2)]
        future_results_fft.append(db_op.insertFFTAsync(sid, list(fft), ts))

        for res in future_results_fft:
            res.result()

        t1=datetime.now()
        fft_ot=(t1 - t0).total_seconds()
        #fft_d = f_ops.timeOffsetTest(ts_str)

        # Calculate and add PSD
        freqs, power = power_spectrum(data, sampling_f=fs)
        average_power = integrate.simps(power)
        future_results_psd.append(db_op.insertPSDAsync(sid, average_power, list(power), ts))
        
        for res in future_results_psd:
            res.result()
        
        t2=datetime.now()
        psd_ot=(t2 - t1).total_seconds()
        #psd_d = f_ops.timeOffsetTest(ts_str)

        return fft_ot, psd_ot

while True:
    t0=datetime.now()
    msg=c.poll(0)
    if msg is None:
        continue
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        continue
    msg = json.loads(msg.value())
    last_time = msg['last_time']
    lt_dt=datetime.strptime(last_time, FILENAME_DATE_FORMAT)
    cs_fft=str((t0-lt_dt).total_seconds())
    sid = msg['sid']
    ts=msg['ts']
    raw_data=msg['d']
    fft_ot, psd_ot = calculate_and_save_fft(raw_data, ts, sid, t0)
    t1=datetime.now()
    time_str=t1.strftime(FILENAME_DATE_FORMAT)
    #psd_ot=(t1 - t0).total_seconds()
    #psd_d = timeOffsetTest(ts)
    payload = {
        "ts": ts,
        "sid": sid,
        "fs_k": msg['fs_k'],
        "fs_ot": msg['fs_ot'],
        "k_cs": msg['k_cs'],
        "k_ot": msg['k_ot'],
        "cs_fft": cs_fft,
        "cs_ot": msg['cs_ot'],
        #"fft_d": fft_d,
        "fft_ot": fft_ot,
        #"psd_d": psd_d,
        "psd_ot": psd_ot
        }
    p.produce(
            config['kafka']['consumer_write']['topic'],
            json.dumps(payload))
    p.poll(0)
    print(ts)
