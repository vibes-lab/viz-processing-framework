import os
import time
import json
import ftp_operations as f_ops
from datetime import datetime, timedelta
from confluent_kafka import Producer
from datavizapi import AppConfig

config = AppConfig().getConfig()

p = Producer({'bootstrap.servers': ",".join(config['kafka']['servers'])})

ftp_config = config['file_sync']

SCRIPT_BASE_PATH = os.path.dirname(os.path.realpath(__file__))
SENSOR_DATE_TIME_FORMAT = '%Y-%m-%d %H:%M:%S:%f'
FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']
DATE_TIME_FORMAT = ftp_config['file_name_format']

def parseSampleRateFromConfig(fname):
    sample_rate = None
    with open(fname, 'r') as f:
        lines = f.readlines()
        sample_rate_line = \
            filter(lambda x: x.startswith('real sample rate'), lines)[0]
        sample_rate = sample_rate_line.split('=')[-1].strip(' \n\r')
    return sample_rate


def putNewfilenames(data, sample_rate, t0, num):
    if len(data) == 0:
        return num
    data = map(lambda x: x[-1], data)
    for d in data:
        print(d)
        file_name=d.rsplit( ".", 1 )[ 0 ]
        #time=datetime.strptime(file_name, DATE_TIME_FORMAT)+timedelta(hours=4) # 5 during DST, 4 otherwise
        current=datetime.now()
        #seconds=(current-time).total_seconds()
        op_time=(current-t0).total_seconds()
        time_str=current.strftime(FILENAME_DATE_FORMAT)
        num+=1
        payload = {
            "sample_rate": sample_rate,
            "file_name": d,
            "last_time": time_str,
            "fs_ot": str(op_time)
        }
        p.produce(
            config['kafka']['consumer_h5']['topic'],
            json.dumps(payload))
    p.poll(0)
    print(num)
    return num

last_config=None

ftp = f_ops.connectAndGetFTP()
remote_dir,num_dirs=f_ops.getRemoteDir(ftp,None,None)
#remote_dir='test_data'
print(remote_dir)
ftp.cwd(remote_dir)
config_fname, last_config = f_ops.fetchConfigFile(ftp,None)
sample_rate = parseSampleRateFromConfig(config_fname)
num=0

while (num<50):
    t0=datetime.now()
    last_date = f_ops.getLastReadDate()
    ftp = f_ops.connectAndGetFTP()
    try:
        ftp.cwd(remote_dir)
        records = f_ops.getFileMetadata(ftp)
        records = f_ops.filterUnfetchedRecords(last_date, records)
        f_ops.updateLastReadDate(records)
        ftp.quit()
        ftp.close()
        num=putNewfilenames(records, sample_rate, t0, num)
        time.sleep(0.005)
    except Exception, e:
        print(e)
        ftp.quit()
        ftp.close()
