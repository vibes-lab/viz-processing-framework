from confluent_kafka import Consumer
from confluent_kafka import Producer
import datavizapi.cassandra_operations as db_op
import ftp_operations as f_ops
from datetime import datetime, timedelta
from datavizapi import AppConfig
import json

config = AppConfig().getConfig()
kafka_config = config['kafka']

c = Consumer({
    'bootstrap.servers': ",".join(kafka_config['servers']),
    'group.id': kafka_config['consumer_write']['group'],
    'auto.offset.reset': kafka_config['consumer_write']['offset']})

c.subscribe([kafka_config['consumer_write']['topic']])

res_file = open('process_times.csv', 'w')
res_file.write("ts, sid, fs_k, fs_ot, k_cs, k_ot, cs_fft, cs_ot, fft_ot, psd_ot \n")

while True:
    msg = c.poll(0)
    if msg is None:
        continue
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        continue
    msg = json.loads(msg.value())
    sid = msg['sid']
    ts=msg['ts']
    fs_k=msg['fs_k']
    fs_ot=msg['fs_ot']
    k_cs=msg['k_cs']
    k_ot=msg['k_ot']
    cs_fft=msg['cs_fft']
    cs_ot=msg['cs_ot']
    fft_ot=msg['fft_ot']
    psd_ot=msg['psd_ot']
    print(ts)
    res_file.write("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}\n".format(ts, sid, fs_k, fs_ot, k_cs, k_ot, cs_fft, cs_ot, fft_ot, psd_ot))
