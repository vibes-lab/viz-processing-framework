# Average every second of sensor data for the preceding hour and add the averaged value to Cass

from __future__ import print_function, division
import os
import time
import json
import math
from datetime import datetime, timedelta
import ftp_operations as f_ops
import datavizapi.cassandra_operations as db_op
from datavizapi.utils.time_utils import (roundToHour, editedTime, formatTime, parseTime, currTime)
import datavizapi.constants as constants
from scipy import signal, integrate
import numpy as np
import pytz
from collections import defaultdict
from datavizapi import AppConfig

config = AppConfig().getConfig()

FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']
data_tz=pytz.timezone(constants.DATA_TZ)
utc_tz=pytz.UTC

future_results=[]
sid_to_data_map = defaultdict(list)

def calculate_and_save_fft(from_ts, to_ts, duration, df):
        
        #Clear all large variables    
        future_results[:] = []
        raw_data=None
        sid_to_data_map.clear()
        fft_freq=None
        full_fft=None
        detrend_data=None
        valid_fft=None
        avg_fft=None
        new_fft=None
        avg_fft=None
        data_sect=None
        fft_comp=None
        fft_amp=None
        success=False

        t0=datetime.now()

        # Fetch raw sensor data from the hour from Cass
        try:
            raw_data = db_op.fetchSensorDataByDay(from_ts, to_ts)
        except Exception, e:
            print(repr(e))
            return success

        t1=datetime.now()
        print('Query: ',(t1 - t0).total_seconds())
        
        # COmbined data into one large time series grouped by SID
        for sid, objects in raw_data.items():
            fs = None
            for obj in objects:
                sid_to_data_map[sid].extend(obj['data'])
                if fs is None:
                    fs = len(obj['data'])
 
            # Calculate required samples and windows for desired resolution
            
            N=len(sid_to_data_map[sid]) # Number of samples available            
            dt=1/fs # Time resolution (sec)
            T=1/df # Minimum window period (sec)
            total_t=N/fs # Total amount of time collected over (sec)
            nw=int(math.floor(total_t/T))  # Number of windows possible without overlap (rounded down)
            n=int(math.floor(N/nw)) # Number of samples per window (actual, rounded down)
            n_min=fs*T # Number of samples per window] (minimum to achieve desired df)


            # Calculate the FFT for every window in the range
            fft_freq = [ x*(fs/n) for x in range(n//2)]
            detrend_data=signal.detrend(sid_to_data_map[sid])
            full_fft=[0]*int(fs//df)
            if (N >= fs):
                for i in range(0, nw):
                    ki=i*n
                    kf=(i+1)*n
                    data_sect=detrend_data[ki:kf]
                    fft_comp=np.fft.fft(data_sect)
                    fft_amp=np.abs(fft_comp/(n//2))
                    new_fft=fft_amp[0:(n//2)]
                    full_fft=[x + y for x, y in zip(full_fft, new_fft)]

                valid_fft=[i for i in full_fft if i != 0]
                avg_fft=[fft/nw for fft in valid_fft]
                
                # Add averaged FFT to hourly FFT table
                future_results.append(db_op.insertFFTByHour(sid, list(avg_fft), from_ts))
            
                for res in future_results:
                    res.result()
                
                success=True
            else:
                success=False

        t2=datetime.now()
        print('FFT: ',(t2 - t1).total_seconds())
        
        return success

freq_res=0.25

duration=60*60 # One hour duration (sec)

#start_time=datetime(2022, 2, 3,0, 0, 0)
#end_time=datetime(2022, 2, 1, 0, 0, 0)
#end_time_utc=utc_tz.localize(end_time)
#print('end_time: ',end_time_utc)

start_time = editedTime(f_ops.getLastReadDate(), seconds=0)
print('lastread:', start_time)
start_time_local = data_tz.localize(start_time)
start_time_utc = start_time_local.astimezone(pytz.utc)
start_time_hour=roundToHour(start_time_utc)
current_time=start_time_hour
print('start_time: ', current_time)

time.sleep(2*duration)

retry=0
total_diff=0

while True:
    t0=datetime.now()
    from_ts=current_time
    to_ts=current_time+timedelta(seconds=duration)
    print('from_ts: ', from_ts)
    print('to_ts: ', to_ts)
    success=calculate_and_save_fft(from_ts, to_ts, duration, freq_res)
    if (success):
        current_time=to_ts
        retry=0
        t1=datetime.now()
        diff=(t1 - t0).total_seconds()
        sleep_time=3599-diff
        time.sleep(sleep_time)
    else:
        retry+=1
        print('Retry: ', retry)
        t1=datetime.now()
        diff=(t1 - t0).total_seconds()
        total_diff=total_diff+diff+1
        if (retry>=60):
            current_time=to_ts
            retry=0
            sleep_time=3599-total_diff
            time.sleep(sleep_time)
    time.sleep(1)
