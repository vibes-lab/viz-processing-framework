# Average all the hourly FFT to get a daily FFT

from __future__ import print_function, division
import os
import time
import json
import math
import matplotlib 
matplotlib.use("agg")
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import datavizapi.cassandra_operations as db_op
import ftp_operations as f_ops
from datavizapi.utils.commons import splitRangeInDays, splitRangeInHours, splitRangeInMinutes, splitRangeInSeconds
import datavizapi.utils.time_utils as time_utils
import datavizapi.constants as constants
from scipy import signal, integrate
import numpy as np
from scipy.fftpack import fft
import pytz
from collections import defaultdict
from datavizapi import AppConfig

config = AppConfig().getConfig()

FILENAME_DATE_FORMAT = config['file_sync']['file_name_format']
data_tz=pytz.timezone(constants.DATA_TZ)
utc_tz=pytz.UTC

sid_to_data_map = defaultdict(list)

def split_and_avg_fft(from_ts, to_ts):
        t0=datetime.now()
        
        success=False
        date = time_utils.roundToDay(from_ts)

        try:
            raw_data =db_op.fetchFFTForDay(date)
        except Exception, e:
            print(repr(e))
            return success

        t1=datetime.now()
        print('Query: ',(t1 - t0).total_seconds())

        future_results = []

        # Split data by SID
        for sid, objects in raw_data.items():
            for obj in objects:
                sid_to_data_map[sid].extend(obj['fft'])
           
            # Calculate required samples and windows for desired resolution
            
            N=len(sid_to_data_map[sid]) # Number of samples available            
            if (N>0):
                n=512 # Number of samples per hour 0.25 freq res
                H=N//n
                fft_freq=[ x*0.25 for x in range(n)]

                full_fft=[0]*3000

                # Calculate average FFT
                for i in range(0, H):
                    new_fft=sid_to_data_map[sid][i*n:(i+1)*n]
                    full_fft=[x + y for x, y in zip(full_fft, new_fft)]

                valid_fft=[i for i in full_fft if i != 0]
                avg_fft=[fft/H for fft in valid_fft]

                future_results.append(db_op.insertFFTByDay(sid, list(avg_fft), to_ts))
            
                for res in future_results:
                    res.result()

                success=True

        t2=datetime.now()
        print('FFT: ',(t2 - t1).total_seconds())

        return success

freq_res=0.25

duration=60*60*24 # One day duration (sec)

start_time = time_utils.editedTime(f_ops.getLastReadDate(), seconds=0)
print('lastread:', start_time)
start_time_local = data_tz.localize(start_time)
start_time_utc = start_time_local.astimezone(pytz.utc)
start_time_hour=time_utils.roundToDay(start_time_utc)
current_time=start_time_hour
print('start_time: ', current_time)

time.sleep(1.6*duration) # Offset to allow hourly data from previous day to complete

retry=0
total_diff=0

# Run once every 24 hours (sleep the remaining time)
while True:
    t0=datetime.now()
    from_ts=current_time
    to_ts=current_time+timedelta(seconds=duration)
    print('from_ts: ', from_ts)
    print('to_ts: ', to_ts)
    success= split_and_avg_fft(from_ts, to_ts)
    if (success):
        current_time=to_ts
        retry=0
        t1=datetime.now()
        diff=(t1 - t0).total_seconds()
        sleep_time=duration-1-diff
        time.sleep(sleep_time)
    else:
        retry+=1
        print('Retry: ', retry)
        t1=datetime.now()
        diff=(t1 - t0).total_seconds()
        total_diff=total_diff+diff+1
        if (retry>=60):
            current_time=to_ts
            retry=0
            sleep_time=duration-1-total_diff
            time.sleep(sleep_time)
    time.sleep(1)
