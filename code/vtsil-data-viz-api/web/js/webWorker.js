var sensor_info_api = '/api/sensor_info';


if( 'function' === typeof importScripts) {
   importScripts("https://d3js.org/d3-fetch.v1.min.js");
}

onmessage = function(event){
    let msg=event.data;
	// Update data for building page
    if(msg[0]==='updateData') {
	var start_time;
        var ns = msg[2];
        if(msg[1]) {
            start_time = msg[1];
        }
        var stream_api = `/api/stream?t=${start_time}&ns=${ns}`;
    	d3.json(stream_api).then(function(response) {
    		postMessage(['data', response]);
    	});
    }
	// Post sensor info message
    if(msg[0]==='sensorInfo') {
	d3.json(sensor_info_api).then(function(response) {
    		postMessage(['sensorInfo', response]);
    	});
    }
	// Update data for FFT page
    if(msg[0]==='updateFFTData') {
	var start_time= msg[1];
	var ns = msg[2];
	var sid = msg[3];
	var df = msg[4];
        var min = msg[5];
	var max =msg[6];
	var context= msg[7];
	var mode= msg[8];
	var init = msg[9]
	if (mode==true){
		// Historical mode
		if(sid && start_time){
			var fft_stream_api = `/api/stream-fft?t=${start_time}&ns=${ns}&sid=${sid}&df=${df}&min=${min}&max=${max}&ct=${context}`;
       			d3.json(fft_stream_api).then(function(response) {
            			postMessage(['data', response]);
        		});
		}
	} else {
		// Real-time mode
		if(sid && start_time){
			var fft_rms_stream_api = `/api/stream-rms-fft?t=${start_time}&ns=${ns}&sid=${sid}&df=${df}&min=${min}&max=${max}&ct=${context}&init=${init}`;
				d3.json(fft_rms_stream_api).then(function(response) {
						postMessage(['data', response]);
				});
		}
	}
    }

};
