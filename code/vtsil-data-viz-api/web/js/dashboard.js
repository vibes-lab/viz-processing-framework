; var Dashboard = (function(mod, Commons, DashboardInteraction, DashboardFloorViz, DashboardSpectrumViz, DashboardTimeseriesViz, DOCUMENT, WINDOW) {
	var is_first = true;

// Updates date based on data from python and updates visualzations                                                                           
	Commons.web_worker.onmessage = function(event) {
	    var t0 = performance.now()
	    var msg = event.data;
	    if(msg[0]==='data') {
		if (typeof msg[1]['v1'][0] !== 'undefined'){
			var date = new Date(msg[1]['v1'][0]['ts']);
	    		DashboardInteraction.latest_time = Commons.formatDate(date);
	    		DashboardTimeseriesViz.updateData(msg[1]['v1'][0]);
	    		DashboardFloorViz.initData(msg[1]['v1'][0]);
	    		DashboardSpectrumViz.initData(msg[1]['v2']);
	        	if(is_first===true){
	        		DashboardFloorViz.draw();
	        		DashboardSpectrumViz.draw();
	            		DashboardTimeseriesViz.draw();
	            		is_first=false;
	        	}else {
	        		DashboardFloorViz.draw();
	        		DashboardSpectrumViz.draw();
	            	// DashboardTimeseriesViz.update();
	            	DashboardTimeseriesViz.draw();
	        	}
		}
	    }

	    if(msg[0]==='sensorInfo') {
	        Commons.processSensorInfo(msg);
	    }
	};

// Specifies details about how the dashboard updates
	mod.update = function() {
		if(!DashboardInteraction.is_hovered)
			// Change the third argument to adjust ns variable (increment)
			Commons.web_worker.postMessage(['updateData', mod.start_time, 1]);
	    	// Change the second argument in WINDOW.setTimeout to adjust how frequenctly page updates (in milliseconds)
		WINDOW.setTimeout(mod.update, 1000);
	}

	return mod;

})(Dashboard || {}, Commons, DashboardInteraction, DashboardFloorViz, DashboardSpectrumViz, DashboardTimeseriesViz, document, window);

d3.select('#start_stream')
	.on('click', function() {
		Dashboard.start_time = d3.select('#start_time')._groups[0][0].value;
    	DashboardTimeseriesViz.initData({});
    	DashboardFloorViz.initData([]);
    	DashboardSpectrumViz.initData([]);
    	window.clearTimeout();
    	Dashboard.update();
	});

function dashboardInit() {
    DashboardFloorViz.initView();
    DashboardSpectrumViz.initView();
    DashboardTimeseriesViz.initView();
    Commons.updateScreenDimensions(window.innerHeight, window.innerWidth);
    Commons.web_worker.postMessage(['sensorInfo']);
    Dashboard.update();
}

// Loads when page is visited
window.addEventListener("DOMContentLoaded", function() {
    // DashboardFloorViz.initView();
    // DashboardSpectrumViz.initView();
    // DashboardTimeseriesViz.initView();
    // Commons.updateScreenDimensions(window.innerHeight, window.innerWidth);
    // DashboardInteraction.init(Commons.W, Commons.H);
    // Commons.web_worker.postMessage(['sensorInfo']);
    // Dashboard.update();
    dashboardInit();
});
