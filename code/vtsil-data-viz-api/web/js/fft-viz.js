// Frequnecy domain dashboard
; var FFTViz = (function(mod, Commons, DOCUMENT, WINDOW) {
	var floor={padding: {l:30,b:30,t:30,r:30}},
		fft={padding: {l:60,b:40,t:15,r:15}},
		rms={padding: {l:70,b:40,t:15,r:15}},
		p = Math.max(0, d3.precisionRound(0.0, 1.0) - 1);

	// All actions to initialize the view
	mod.initView = function() {
        	var coll = document.getElementsByClassName("collapsible");
		for (var i = 0; i < coll.length; i++) {
  			coll[i].addEventListener("click", function() {
				this.classList.toggle("active-col");
				var content = this.nextElementSibling;
				if (content.style.display === "block") {
      				content.style.display = "none";
    			} else {
      				content.style.display = "block";
    			}
  			});
		}

		$('.start_datetime').datetimepicker({
			weekStart: 0,
			todayBtn:  0,
			autoclose: 1,
			todayHighlight: 0,
			startView: 2,
			forceParse: 1,
			showMeridian: 1
		}).on("changeDate", function() { mod.startTimeChange()});

		$('.start_datetime').attr('data-date', Commons.currDate());

		$('.end_datetime').datetimepicker({
			weekStart: 0,
			todayBtn:  0,
			autoclose: 1,
			todayHighlight: 0,
			startView: 2,
			forceParse: 1,
			showMeridian: 1
        	}).on("changeDate", function() { mod.endTimeChange()});

        	Commons.web_worker.postMessage(['sensorInfo']);
        	setFloorNumber();
		
		d3.select('#play_pause').property('value', "\u23F8");
            	mod.playToggle=true;

		is_clicked=new Array(300).fill(0);
		
		floor.svg = d3.select('#floor').append('svg')
			.attr('width', "100%")
			.attr('height', '100%')
			.attr('stroke', 'white');

		fft.svg = d3.select('#floor-sensor-fft').append('svg')
			.attr('width', "100%")
			.attr('height', '100%')
			.attr('stroke', 'white');

		rms.svg = d3.select('#rms').append('svg')
			.attr('width', "100%")
			.attr('height', '100%')
			.attr('stroke', 'white');
		
		let dimensions = Commons.getDimensions("floor");

		floor.w = Math.floor(dimensions[0]);
		floor.h = floor.w*0.9;
		floor.x = Math.floor(dimensions[2]);
		floor.y = Math.floor(dimensions[3]);

        	dimensions = Commons.getDimensions("floor-sensor-fft");

		fft.w = Math.floor(dimensions[0]);
		fft.h = floor.h;
		fft.x = Math.floor(dimensions[2]);
		fft.y = Math.floor(dimensions[3]);
		fft.svg.append('g').attr('class', 'paths');
		fft.svg.append('g').attr('class', 'axis x');
		fft.svg.append('g').attr('class', 'axis y');
		fft.svg.append('g').attr('class', 'axis-label label-y').append('text');
		fft.svg.append('g').attr('class', 'axis-label label-x').append('text');
		fft.svg.append('g').attr('class', 'title').append('text');

		dimensions = Commons.getDimensions("rms");

		rms.w = Math.floor(dimensions[0]);
		rms.h = Math.floor(dimensions[1])*0.9;
		rms.x = Math.floor(dimensions[2]);
		rms.y = Math.floor(dimensions[3]);
		rms.svg.append('g').attr('class', 'legend');
		rms.svg.append('g').attr('class', 'axis label legend-label');
		rms.svg.append('g').attr('class', 'paths');
		rms.svg.append('g').attr('class', 'axis x');
		rms.svg.append('g').attr('class', 'axis y');
		rms.svg.append('g').attr('class', 'axis-label label-y').append('text');
		rms.svg.append('g').attr('class', 'axis-label label-x').append('text');
		rms.svg.append('g').attr('class', 'title').append('text');

		floor.svg.append('g').attr('class', 'paths');
		floor.svg.append('g').attr('class', 'circles');
		floor.svg.append('g').attr('class', 'title').append('text');

		mod.drawFloor(Commons.floormap_outer, Commons.floormap_inner);
		
		mod.trendModeSelected();
		mod.modeSelected();
		settingsChange();
		mod.rms_stream=[];
		mod.speed=1000; // Refresh rate for api calls (determines how often page updates) in ms
		mod.update();
    };

	// Posts message to start webworker actions
	mod.update = function() {
		Commons.web_worker.postMessage(['updateFFTData', mod.start_time, mod.incr, mod.sid, mod.df, mod.min_freq, mod.max_freq,mod.freq_context,mod.mode, mod.init]);
	WINDOW.setTimeout(mod.update, mod.speed);
    }

	// Function to update plots or data with each webworker message received
	Commons.web_worker.onmessage = function(event) {   
		var msg = event.data;
		if(msg[0]==='data') {
			// Historical mode
			if (mod.mode==true){
				// Only run if not paused
				if (mod.playToggle==true){
					// Only run if variables are done being updated
					if (mod.updating!==true){
						d3.select('.loader').classed('hidden', true);
						mod.start_time=msg[1]['new_ts'];
						//console.log('app st:', mod.start_time)
						// Only update if required variables are defined
						if(mod.sid && mod.start_time && mod.end_time && mod.max_fft){
							mod.drawFFT({id: mod.sid, fft: msg[1]['fft']});
							mod.drawRMS(mod.trend_data);
						}
					} else {
						d3.select('.loader').classed('hidden', false);
					}
				// If paused
				} else {
					if (mod.updating!==true){
										d3.select('.loader').classed('hidden', true);
										mod.start_time=msg[1]['ts'];
										// Only update if required variables are defined
										if(mod.sid && mod.start_time && mod.end_time && mod.max_fft){
												mod.drawFFT({id: mod.sid, fft: msg[1]['fft']});
												mod.drawRMS(mod.trend_data);
										}
								} else {
										d3.select('.loader').classed('hidden', false);
					}
				}
			// Real-time mode
			} else {
				if (mod.playToggle==true){
					// Only run if variables are done being updated
					if (mod.updating!==true){
						d3.select('.loader').classed('hidden', true);
						// Only update if required variables are defined
						if(mod.sid && mod.start_time){
							if (mod.init==true){
								mod.rms_stream=msg[1]['rms']
								mod.init=0;
								mod.start_time=msg[1]['ts'];
							} else {
								var new_rms=msg[1]['rms'][0]
								mod.rms_stream.push(new_rms) // Add new rms to end
								mod.rms_stream.shift()
								mod.start_time=msg[1]['new_ts'];
							}
							var new_fft=msg[1]['fft']
							var new_max=d3.max(new_fft, d=>d[1])
							mod.max_fft=Math.max(mod.max_fft,new_max)
							mod.drawFFT({id: mod.sid, fft: new_fft});
							mod.streamRMS(mod.rms_stream);
						}
					} else {
											d3.select('.loader').classed('hidden', false);
					}
				} else {
					if (mod.updating!==true){
							d3.select('.loader').classed('hidden', true);
							mod.start_time=msg[1]['ts'];
							// Only update if required variables are defined
							if(mod.sid && mod.start_time){
									mod.drawFFT({id: mod.sid, fft: msg[1]['fft']});
									mod.streamRMS(mod.rms_data);
							}
					} else {
							d3.select('.loader').classed('hidden', false);
					}
				}
			}
			// Makes sure that updated time doesn't get overwritten by time that was loading during selection
			if (mod.time_update==true){
				var ts=0;
				var start=new Date
				while (ts < mod.speed*1.5){
					mod.start_time=mod.updated_time
					end= new Date
					ts = end - start;
				}
				mod.time_update=false;
			}
		}
		if(msg[0]==='sensorInfo') {
				Commons.processSensorInfo(msg);
		}
	}

	// Draws outline of Goodwin Hall floor
	mod.drawFloor = function(floor_plan_outer, floor_plan_inner) {
		var settings = floor;

	    var x_scale = d3.scaleLinear()
			.domain(d3.extent(floor_plan_outer.flat(), d=>parseFloat(d[0])))
			.range([settings.padding.l, settings.w-settings.padding.r]);

	    var y_scale = d3.scaleLinear()
			.domain(d3.extent(floor_plan_outer.flat(), d=>parseFloat(d[1])))
			.range([settings.h-settings.padding.b, settings.padding.t]);

	    var line = d3.line()
			.x(d=>x_scale(d[0]))
			.y(d=>y_scale(d[1]));

	    var inner = settings.svg.select('g.paths').selectAll('path').data(floor_plan_inner);
	    var outer = settings.svg.select('g.paths').selectAll('path').data(floor_plan_outer);

	    inner.enter()
			.append('path')
			.attr('d', line);
	    outer.enter()
			.append('path')
			.attr('d', line);
	};

	// Adds circles at sensor locations within the floorplan
	mod.drawSensors = function(floor_plan_outer, sensor_locations) {
		var settings = floor;

		var circles = settings.svg.select('g.circles')
			.selectAll('circle')
			.data(sensor_locations, d=>d[0]);

		var x_scale = d3.scaleLinear()
			.domain(d3.extent(floor_plan_outer.flat(), d=>parseFloat(d[0])))
			.range([settings.padding.l, settings.w-settings.padding.r]);

		var y_scale = d3.scaleLinear()
			.domain(d3.extent(floor_plan_outer.flat(), d=>parseFloat(d[1])))
			.range([settings.h-settings.padding.b, settings.padding.t]);

		circles.enter()
			.append('circle')
				.attr('id', d=>('name'+d[0]))
				.attr('stroke', 'none')
				.attr('opacity', 1) 
				.attr('cx', d=>x_scale(d[2]))
				.attr('cy', d=>y_scale(d[3]))
				.on('mouseover', d=>onSensorOver(d))
				.on('mouseout',d=>onSensorOut(d))
				.on('click', d=>onSensorGlyphClicked(d[0]))
			.merge(circles)
				.transition()
				.attr('r', 4)
				.attr('fill', d3.rgb(253, 133, 58));
		circles.exit().remove();
	}

	// Plots the FFT for the current time, baseline FFT for the time range, and lines showing selected frequency range
	mod.drawFFT = function(data) {
			var settings = fft;

			// Set up path forumalas
			var min_data=[[mod.min_freq-0.0001,0],[mod.min_freq,0], [mod.min_freq, mod.max_fft]]
			var max_data=[[mod.max_freq,0], [mod.max_freq, mod.max_fft]]		

			if (mod.x_scale == undefined) {
				mod.x_scale = d3.scaleLinear()
					.domain(d3.extent(data.fft, d=>d[0]))
					.range([settings.padding.l, settings.w-settings.padding.r]);
			}

			if (mod.y_scale == undefined) {
				mod.y_scale = d3.scaleLinear()
                                        .domain([0,mod.max_fft])
                                        .range([settings.h-settings.padding.b, settings.padding.t]);
			}

			var line = d3.line()
				.x(d=>mod.x_scale(d[0]))
				.y(d=>mod.y_scale(d[1]));
            
			// Path for current FFT
			var fftlines = settings.svg.select('g.paths').selectAll('path').data([data.fft], d=>d[2]);
            		fftlines.enter()
				.append('path')
					.attr('d', line)
					.attr('fill', 'none')
					.attr('stroke', d3.rgb(33, 150, 243));
            		fftlines.exit().remove();
		
			// Only plot if BL data exists
			if(mod.baselineFFT){
				// Plot for baseline FFT
				var baseline = settings.svg.select('g.paths').selectAll('path').data([mod.baselineFFT], d=>d[2]);
            			baseline.enter()
					.append('path')
						.attr('d', line)
						.attr('fill', 'none')
						.attr('stroke', d3.rgb(255, 0, 0));
			}
            if(mod.max_freq && mod.min_freq){
				// Line for minimum frequency selected
				var minline= settings.svg.select('g.paths').selectAll('path').data([min_data], d=>d[2]);
				minline.enter()
					.append('path')
						.attr('d', line)
						.attr('fill', 'none')
						.attr('stroke', 'white')
						.attr('stroke-dasharray','1 4');

				// Line for maximum frequency selected
				var maxline= settings.svg.select('g.paths').selectAll('path').data([max_data], d=>d[2]);
				maxline.enter()
					.append('path')
						.attr('d', line)
						.attr('fill', 'none')
						.attr('stroke', 'white')
						.attr('stroke-dasharray','1 4');
			}
		
			// Get power of maximum plot value (to adjust axis labels)
			var pow=-Math.floor(Math.log10(mod.max_fft))

			// Format axes
			var x_axis = d3.axisBottom(mod.x_scale)
			var y_axis = d3.axisLeft(mod.y_scale)      
				.tickFormat(x => `${(x*Math.pow(10,pow)).toFixed(2)}`)	

			settings.svg.select('g.x')
				.attr('transform', 'translate('+(0)+","+(settings.h-settings.padding.b)+')')
					.call(x_axis);

		 	settings.svg.select('g.y')
				.attr('transform', 'translate('+(settings.padding.l)+","+(0)+')')
					.call(y_axis);

			// Format axis labels
			settings.svg.select('g.label-y').select('text')
				.attr("transform", "rotate(-90)")
				.attr("y", 2)
				.attr("x", 0-(settings.h/2))
				.attr("dy", "1em")
				.attr('text-anchor', 'middle')
				.text('FFT Amplitude (10-'+pow+')');

			settings.svg.select('g.label-x').select('text')
				.attr('transform','translate('+(settings.w/2)+","+(settings.h-10)+")")
				.attr('text-anchor', 'middle')
				.text('Frequency');

			var daq_txt=mod.start_time;

			// Format titles
			settings.svg.select('g.title').select('text')
				.attr('transform','translate('+(3*settings.w/4)+","+(settings.padding.t)+")")
				.attr("text-anchor", "middle")
				.style("font-size", "16px")
				.attr('stroke', 'white')
				.text(daq_txt);
	};
	
	// Plots RMS variation over time, average for the time period, and a line showing the current time
	mod.drawRMS = function(rms_data) {
		if(mod.sid && mod.start_time && mod.end_time){
			var settings = rms;
			// Colors for plots
			var bl_color=d3.rgb(255, 0, 0)
			var r_color=d3.rgb(33, 150, 243)
			var st_color=d3.rgb(240, 240, 240)

			// Set up formulas for paths
			var min_ts=new Date(d3.min(rms_data, d=>d[0]));
			var max_ts=new Date(d3.max(rms_data, d=>d[0]));
			
			var max_rms=d3.max(rms_data, d=>d[1])
			var min_rms=d3.min(rms_data, d=>d[1])
			
			// Cannot plot multiple two-point lines using d3 paths, must add extra arbitrary point to get plot to work
			var time_data=[[mod.start_time,min_rms-0.00000000000000000000000000001],[mod.start_time,min_rms],[mod.start_time,max_rms]]
			var x_scale = d3.scaleTime()
				.domain([min_ts, max_ts])
				.range([settings.padding.l, settings.w-settings.padding.r]);
		
			var y_scale = d3.scaleLinear()
				.domain(d3.extent(rms_data, d=>d[1]))
				.range([settings.h-settings.padding.b, settings.padding.t]);
          	
			var x_inverse_scale = d3.scaleLinear()
				.range([min_ts, max_ts])
				.domain([settings.padding.l, settings.w - settings.padding.r]);

			var line = d3.line()
				.x(d=>x_scale(new Date(d[0])))
				.y(d=>y_scale(d[1]));
			
			// Line for current time being streamed
			var startline = settings.svg.select('g.paths').selectAll('path').data([time_data], d=>d[2]);
			startline.enter()
				.append('path')
					.attr('d', line)
					.attr('fill', 'none')
					.attr('stroke', st_color);
			startline.exit().remove();
			
			// If RMS mode is selected
			if (mod.trend_mode){
                mode="RMS"
				var base_data=[[min_ts,mod.rms_bl],[max_ts, mod.rms_bl]]
				// Line for baseline RMS
				var baseline = settings.svg.select('g.paths').selectAll('path').data([base_data], d=>d[2]);
				baseline.enter()
					.append('path')
						.attr('d', line)
						.attr('fill', 'none')
						.attr('stroke', bl_color);
			} else {
				mode="MSE"
            }
			
			// Streamed RMS/ MSE path over time
			var rmslines = settings.svg.select('g.paths').selectAll('path').data([rms_data], d=>d[2]);	
			rmslines.enter()
				.append('path')
					.attr('d', line)
					.attr('fill', 'none')
					.attr('stroke', r_color)
				.on('click', function(d) {
					d = {'id': d};
					var position=[d3.event.x-settings.x, d3.event.y-settings.y];
					mod.updated_time=Commons.formatDateStr(x_inverse_scale(position[0]));
					mod.start_time=mod.updated_time;
					mod.time_update=true;
					console.log('mod.updated_time: ', mod.updated_time)
		    	});
       	    
			// Number of seconds and minutes in rms range
			n_sec=(max_ts - min_ts) / 1000
			n_min=n_sec/60

			if (n_min >= 10){
				t_format=d3.timeFormat("%I:%M %p")
			} else {
				t_format=d3.timeFormat("%I:%M:%S %p")
			}

			var pow=-Math.floor(Math.log10(max_rms))

			// Format axes
            var y_axis = d3.axisLeft(y_scale)
                .ticks(6)
				.tickFormat(x => `${(x*Math.pow(10,pow)).toFixed(2)}`)

			var x_axis = d3.axisBottom(x_scale)
				.ticks(15)
				.tickFormat(t_format);

            settings.svg.select('g.x')
                .attr('transform', 'translate('+(0)+","+(settings.h-settings.padding.b)+')')
			.call(x_axis)
			.selectAll("text")  
				.style("text-anchor", "end")
			.attr("transform", "rotate(-40)");

			settings.svg.select('g.y')
				.attr('transform', 'translate('+(settings.padding.l)+","+(0)+')')
				.call(y_axis);

			// Format axis labels
			settings.svg.select('g.label-y').select('text')
				.attr("transform", "rotate(-90)")
				.attr("y", 2)
				.attr("x", 0-(settings.h/2))
				.attr("dy", "1em")
				.attr('text-anchor', 'middle')
				.text(mode+' (10-'+pow+')');

			settings.svg.select('g.label-x').select('text')
				.attr('transform','translate('+(settings.w/2)+","+(settings.h+15)+")")
				.attr('text-anchor', 'middle')
				.text('Time');

			// Add legend
			var size =5

			settings.svg.select('g.legend-label').append("text")
				.attr("x", 220)
				.attr("y", size+2)
				.text("Current")
				.attr("alignment-baseline","middle")
				.style("font-width", "lighter")
				.attr('fill', 'white')
				.attr('stroke-width', 0.25);
			
			settings.svg.select('g.legend-label').append("text")
				.attr("x", 420)
				.attr("y", size+2)
				.text("Baseline")
				.attr("alignment-baseline","middle")
				.style("font-width", "lighter")
				.attr('fill', 'white')
				.attr('stroke-width', 0.25);

			settings.svg.select('g.legend-label').append("text")
				.attr("x", 620)
				.attr("y", size+2)
				.text("Time")
				.attr("alignment-baseline","middle")
				.style("font-width", "lighter")
				.attr('fill', 'white')
				.attr('stroke-width', 0.25);

			settings.svg.append("circle")
				.attr("cx",200)
				.attr("cy",size)
				.attr("r", size)
				.style("fill", r_color)
				.attr('stroke', r_color);

			settings.svg.append("circle")
				.attr("cx",400)
				.attr("cy",size)
				.attr("r", size)
				.style("fill", bl_color)
				.attr('stroke', bl_color);

			settings.svg.append("circle")
				.attr("cx",600)
				.attr("cy",size)
				.attr("r", size)
				.style("fill", st_color)
				.attr('stroke', st_color);
		}
	};

	// Plots RMS/ mSE variation over time, average for the time period, and a line showing the current time
        mod.streamRMS = function(rms_data) {
            if (mod.trend_mode){
				mode="RMS"
			} else {
				mode="MSE"
			}
				
			var settings = rms;

			// Set up formulas for paths
			var bl_color=d3.rgb(255, 0, 0)
			var r_color=d3.rgb(33, 150, 243)

			var min_ts=new Date(d3.min(rms_data, d=>d[0]));
			var max_ts=new Date(d3.max(rms_data, d=>d[0]));

			var max_rms=d3.max(rms_data, d=>d[1])
			var min_rms=d3.min(rms_data, d=>d[1])
			var avg_rms=d3.mean(rms_data, d=>d[1])

			extent=[Math.min(min_rms, mod.rms_bl), Math.max(max_rms,mod.rms_bl)]

			var base_data=[[min_ts,avg_rms],[max_ts, avg_rms]]

			var x_scale = d3.scaleTime()
				.domain([min_ts, max_ts])
				.range([settings.padding.l, settings.w-settings.padding.r]);

			var y_scale = d3.scaleLinear()
				.domain(d3.extent(rms_data, d=>d[1]))
				.range([settings.h-settings.padding.b, settings.padding.t]);

			var x_inverse_scale = d3.scaleLinear()
				.range([min_ts, max_ts])
				.domain([settings.padding.l, settings.w - settings.padding.r]);

			var line = d3.line()
				.x(d=>x_scale(new Date(d[0])))
				.y(d=>y_scale(d[1]));

			// Baseline RMS/ MSE path
			var baseline = settings.svg.select('g.paths').selectAll('path').data([base_data], d=>d[2]);
			baseline.enter()
				.append('path')
					.attr('d', line)
					.attr('fill', 'none')
					.attr('stroke', bl_color);
			baseline.exit().remove();
			
			// Path for RMS/ MSE over time
			var rmslines = settings.svg.select('g.paths').selectAll('path').data([rms_data], d=>d[2]);
			rmslines.enter()
					.append('path')
							.attr('d', line)
							.attr('fill', 'none')
							.attr('stroke', r_color)
					.on('click', function(d) {
							d = {'id': d};
							var position=[d3.event.x-settings.x, d3.event.y-settings.y];
							mod.start_time=Commons.formatDateStr(x_inverse_scale(position[0]));
			});

			// Format axes
			n_sec=(max_ts - min_ts) / 1000
			n_round=Math.ceil((n_sec/6)/5)*5		
			var pow=-Math.floor(Math.log10(max_rms))

			var y_axis = d3.axisLeft(y_scale)
					.tickFormat(x => `${(x*Math.pow(10,pow)).toFixed(2)}`)

			var x_axis = d3.axisBottom(x_scale)
				.ticks(8)
				.tickFormat(d3.timeFormat("%I:%M:%S"));

			settings.svg.select('g.x')
				.attr('transform', 'translate('+(0)+","+(settings.h-settings.padding.b)+')')
				.call(x_axis)
				.selectAll("text")
				.style("text-anchor", "end")
				.attr("transform", "rotate(-40)");

			settings.svg.select('g.y')
				.attr('transform', 'translate('+(settings.padding.l)+","+(0)+')')
				.call(y_axis);

			// Format axis labels
			settings.svg.select('g.label-y').select('text')
				.attr("transform", "rotate(-90)")
				.attr("y", 2)
				.attr("x", 0-(settings.h/2))
				.attr("dy", "1em")
				.attr('text-anchor', 'middle')
				.text(mode+' (10-'+pow+')');

			settings.svg.select('g.label-x').select('text')
				.attr('transform','translate('+(settings.w/2)+","+(settings.h+15)+")")
				.attr('text-anchor', 'middle')
				.text('Time');
		
			// Add legend
			var size =5

			settings.svg.select('g.legend-label').append("text")
				.attr("x", 220)
				.attr("y", size+2)
				.text("Current")
				.attr("alignment-baseline","middle")
				.style("font-width", "lighter")
				.attr('fill', 'white')
				.attr('stroke-width', 0.25);

			settings.svg.select('g.legend-label').append("text")
				.attr("x", 420)
				.attr("y", size+2)
				.text("Baseline")
				.attr("alignment-baseline","middle")
				.style("font-width", "lighter")
				.attr('fill', 'white')
				.attr('stroke-width', 0.25);

			settings.svg.append("circle")
				.attr("cx",200)
				.attr("cy",size)
				.attr("r", size)
				.style("fill", bl_color)
				.attr('stroke', bl_color);

			settings.svg.append("circle")
				.attr("cx",400)
				.attr("cy",size)
				.attr("r", size)
				.style("fill", r_color)
				.attr('stroke', r_color);
        };

	// Graphically update selected sensor
	function updateSensorSelection(){
		if(is_clicked[mod.sid]){
                	is_clicked=new Array(300).fill(0);
                        resetHighlight();
                        document.getElementById('select-sensor-id').value = undefined;
                        document.getElementById('daq-select').innerHTML = '';
			mod.sid=undefined;
                } else {
			is_clicked=new Array(300).fill(0);
                        is_clicked[mod.sid]=true;
			resetHighlight();
                        highlightSensor(mod.sid, 0, 255, 0, 7, 1);
			updateSensorInfo(mod.sid, 'daq-select');
			updateData()
		}
	}

	// Update sensor ID and call all the APIs 
	function updateData() {
		console.log(mod.sid)
		// Historical mode
		if (mod.mode==true){
			if (mod.sid && mod.start_time && mod.end_time){
				// Start loader
				d3.select('.loader').classed('hidden', false);
						mod.updating=true;
				mod.speed=1000;
				// Clear variables
				mod.y_scale=undefined;
						mod.x_scale=undefined;
						mod.max_fft=undefined;
						mod.rms_bl=undefined;
						// Call python functions
						baselineFFT();
				if (mod.trend_mode){
					updateRMS();
				} else {
					updateMSE();
				}
			}
		// Real-time mode
		} else {
			if (mod.sid){
					// Start loader
					d3.select('.loader').classed('hidden', false);
					mod.updating=true;
					mod.init=1;
					mod.speed=1500;
					mod.rms_stream=[]
					mod.y_scale=undefined;
					mod.x_scale=undefined;
					mod.max_fft=undefined;
					mod.rms_bl=undefined;
					// Call python functions
					baselineFFT();
					mod.updating=false;
            }
		}
	}

	// Calls API to get baseline FFT and baseline RMS data
	function baselineFFT() {
		var api = "/api/fft/avg?bl="+mod.bl+"&ts="+mod.selected_time+"&sid="+mod.sid+"&df="+mod.df+"&min="+mod.min_freq+"&max="+mod.max_freq+"&ct="+mod.freq_context;
		d3.json(api).then(function(response) {
			if(response['msg']==='error') {
					d3.select('.alert').classed('hidden', false);
					return;
			} else {
					d3.select('.alert').classed('hidden', true);
			}
			mod.max_bl=response.max_fft
			mod.baselineFFT=response.fft;
			mod.max_fft=mod.max_bl*1.5;
		});
	}
	
	// Calls API to get MSE data
        function updateMSE() {
                from_ts=d3.select('#start_time').attr('value');
                to_ts=d3.select('#end_time').attr('value');
                var api = "/api/mse?sid="+mod.sid+"&bl="+mod.bl+"&fts="+from_ts+"&tts="+to_ts+"&ns="+mod.incr+"&df="+mod.df+"&min="+mod.min_freq+"&max="+mod.max_freq;
                d3.json(api).then(function(response) {
                        if(response['msg']==='error') {
                                        d3.select('.alert').classed('hidden', false);
                                        return;
                        } else {
                                        d3.select('.alert').classed('hidden', true);
                        }
                        mod.trend_data=response.mse;
			mod.updating=false;
		});
    }


	// Calls API to get rms data and the max FFT value for the selected frequency range
	function updateRMS() {
		from_ts=d3.select('#start_time').attr('value');
		to_ts=d3.select('#end_time').attr('value');
		var api = "/api/rms?sid="+mod.sid+"&fts="+from_ts+"&tts="+to_ts+"&ns="+mod.incr+"&df="+mod.df+"&min="+mod.min_freq+"&max="+mod.max_freq;
		d3.json(api).then(function(response) {
			if(response['msg']==='error') {
					d3.select('.alert').classed('hidden', false);
					return;
			} else {
					d3.select('.alert').classed('hidden', true);
			}
			mod.max_rms=response.max_fft[0]
			mod.rms_bl=response.avg_rms[0];
			mod.trend_data=response.rms;
			mod.updating=false;
        });
    }

	// Create array of sensor info for all DAQs 
	function loadSensorInfo() {
		all_daq_info=new Array(300).fill(0);
		var sids = Object.keys(Commons.sensor_info);
		var info=Commons.sensor_info
		for(var i=0; i<sids.length; i++) {
            		var daq_info=[]
			var dir=undefined
			daq_info[0]=sids[i]; //sid
			daq_info[1]=info[sids[i]]['daq_name']; // DAQ name
			orientation=info[sids[i]]['orientation'];
			switch(orientation) {
				case 'U':
					dir='Up';
					break;
				case 'D':
					dir='Down';
					break;
				case 'E':
					dir='East';
					break;
				case 'W':
					dir='West';
					break;
				case 'N':
					dir='North';
					break;
				case 'S':
					dir='South';
					break;
			}
			daq_info[2]=dir;
			daq_info[3]=info[sids[i]]['sensitivity'];
			all_daq_info[sids[i]]=daq_info;
		}
		return all_daq_info
	}

	// Updates dropdown list of sensors based on floor selected
	function updateSensorList(floor_num) {
		floor_sensors=Commons.SidAndDaqNamesForFloor(floor_num);
		var select = document.getElementById("select-sensor-id");
		var i, L = select.options.length - 1;
   		for(i = L; i >= 0; i--) {
      		select.remove(i);
   		}
		
		var default_sens = document.createElement("option");
		default_sens.textContent = 'Select Sensor';
		default_sens.value = undefined;
		select.appendChild(default_sens);

		// Add DAQ names for selected floor
		for(var i = 0; i < floor_sensors.length; i++) {
			var daq = floor_sensors[i][0];
			var sid= floor_sensors[i][1];
    			var el = document.createElement("option");
    			el.textContent = daq;
    			el.value = sid;
    			select.appendChild(el);
		}
	}

	// Called on mouseclick of sensor glyph (circle)
	function onSensorGlyphClicked(sid) {
		mod.sid=sid;
		document.getElementById('select-sensor-id').value = mod.sid;
		updateSensorSelection();
    	}

// mod functions

	// Update sensor on sensor dropdown selection
	mod.sensorDropdownSelected = function() {
	    var sensor_select = document.getElementById('select-sensor-id');
		var selected = sensor_select.options[sensor_select.selectedIndex];
		mod.sid = selected.value;
		updateSensorSelection();
    }

	// Updates when floor from floor dropdown is selected
	mod.floorSelected = function() {
	    var floor_select = document.getElementById('select-floor-number');
	    var selected = floor_select.options[floor_select.selectedIndex];
	    var floor_num = selected.value;
	    mod.drawSensors(Commons.floormap_outer, Commons.sensor_coords[floor_num-1]);
	    document.getElementById('daq-select').innerHTML = '';
	    updateSensorList(floor_num);
	    loadSensorInfo();
	}

	// Updates start_time variable to datetime selected from datetime picker
	mod.startTimeChange = function() {
		mod.selected_time=d3.select('#start_time').attr('value');
		mod.start_time=mod.selected_time;
		updateData();
    	}

	// Updates end_time variable to datetime selected from datetime picker
	mod.endTimeChange = function() {
                mod.end_time=d3.select('#end_time').attr('value');
		updateData();
    	}

	// Updates mode variable to selected radio button
	mod.modeSelected = function() {
		var mode_list = document.getElementsByName('mode')
		for(i = 0; i < mode_list.length; i++) {
                        if(mode_list[i].checked)
                                mode=mode_list[i].value
                }
		if(mode=="1"){
			// Historical
			mod.updating=true
			mod.mode=true
			document.getElementById("time-container").style.pointerEvents = "auto";
			document.getElementById("time-container").style.opacity = "1";
			mod.startTimeChange();
			mod.endTimeChange();
			updateData();
		} else {
			// Real-time
			mod.updating=true
			mod.mode=false
			document.getElementById("time-container").style.pointerEvents = "none";
			document.getElementById("time-container").style.opacity = "0.6";
			dt=new Date
			//dt.setMinutes( dt.getMinutes() - 5 ); // 5 minute delay
			dt.setSeconds( dt.getSeconds() - 10 ); // 10 sec delay
			mod.selected_time=Commons.formatDateStr(dt);
			mod.start_time= mod.selected_time;
			updateData();
			mod.speed=2000;
		}
	}

	// Updates trend_mode variable to selected radio button (MSE/ RMS)
	mod.trendModeSelected = function() {
                var mode_list = document.getElementsByName('trend_mode')
                for(i = 0; i < mode_list.length; i++) {
                        if(mode_list[i].checked)
                                mode=mode_list[i].value
                }
                if(mode=="1"){
                        // RMS
						mod.trend_mode=true
                } else {
						// MSE
                        mod.trend_mode=false
		}
		updateData();
    }


	// Updates sensor information textboxes
	function updateSensorInfo(sid,id) {
		info=loadSensorInfo();
		var daq_info = info[sid];
		text= '<strong>DAQ: </strong>' + daq_info[1] + '<br>' +
			'<strong>Orientation: </strong>' + daq_info[2] + '<br>' +
			'<strong>Sensitivity: </strong>' + daq_info[3]
		document.getElementById(id).innerHTML = text;
	}

	// Changes fill for selected sensor
	function highlightSensor(sid, r, g, b, radius, opacity) {
		var settings = floor;
		settings.svg.select('g.circles')
			.select('#name' + sid)
				.transition()
				.attr('fill', d3.rgb(r, g, b))
				.attr('opacity', opacity)
				.attr('r', radius);
    }
	
	// Returns fill for selected sensor to the default color
	function resetHighlight() {
		var settings = floor;
			settings.svg.select('g.circles')
				.selectAll('circle')
					.attr('fill', d3.rgb(253, 133, 58))
					.attr('opacity', 1)
					.attr('r', 4);
        }

	// Called on mouseover of sensor circle
	function onSensorOver(data) {
		sid=data[0];
		highlightSensor(sid, 0, 0, 255, 7, 1);
		updateSensorInfo(sid, 'daq-hover');
	}	

	// Called on mouseout of sensor circle
	function onSensorOut(data) {
		sid=data[0];
		if(is_clicked[sid]){
			highlightSensor(sid, 0, 255, 0, 7, 1);
			document.getElementById('daq-hover').innerHTML = '';
		} else {
			highlightSensor(sid, 253, 133, 58, 4, 1);
			document.getElementById('daq-hover').innerHTML = '';
		}
	}

	// Updates all settings in the collapsible settings menu upon settings selection
	function settingsChange(){
		// Set frequency settings
                min=document.getElementById("min_freq").value;
                max=document.getElementById("max_freq").value;
                context=document.getElementById("freq_context").value;
                if (min !=""){
                        mod.min_freq=min
                }
		else {
			mod.min_freq=undefined;
		}
                if (max !=""){
                        mod.max_freq=max
                }
		else  {
			mod.max_freq=undefined;
                }
                if (context !=""){
                        mod.freq_context=context
                }
		else {
			mod.freq_context=undefined;
		}

                // Set frequency resolution
                var df_list = document.getElementsByName('freq_res')
                // Checks if any radio buttons are toggled
                for(i = 0; i < df_list.length; i++) {
                        if(df_list[i].checked) {
                        // Sets baseline variable to number of seconds represented by the selected radio button
                                mod.df=df_list[i].value
								mod.df=eval(mod.df)
                        }
                }
		mod.T=1/mod.df
		document.getElementById('period').innerHTML = mod.T

                // Set baseline period
                var baseline_list = document.getElementsByName('baseline')
                for(i = 0; i < baseline_list.length; i++) {
                        if(baseline_list[i].checked)
                                mod.bl=baseline_list[i].value
                }
	
				// Set streaming increment
                var incr_list = document.getElementsByName('incr')
                // Checks if any radio buttons are toggled
                for(i = 0; i < incr_list.length; i++) {
                        if(incr_list[i].checked) {
                        // Sets incr variable to number of seconds represented by the selected radio button
                                mod.incr=incr_list[i].value
                        }
                }

                // Call apis and update plots with new settings
                updateData();
        }

	
	// Defines behavior on play_pause button click
	d3.select('#play_pause')
        .on('click', function() {
			console.log('Button click')
			if (mod.playToggle==true){
				mod.updated_time=mod.start_time;
				mod.time_update=true;
				console.log('play=true')
				d3.select('#play_pause').property('value', '\u23F5');
				//mod.playButton=false;
				mod.playToggle=false;
			} else {
				console.log('play=false')
				d3.select('#play_pause').property('value', "\u23F8");
				mod.playToggle=true;
				//mod.playButton=true;
			}
        });

	// Defines behavior on set time button click
	d3.select('#set_time')
        .on('click', function() {
		// Updates start_time variable to datetime selected from datetime picker
                mod.selected_time=d3.select('#start_time').attr('value');
                mod.start_time=mod.selected_time;
                mod.end_time=d3.select('#end_time').attr('value');
                updateData();
    });

	// Defines behavior on settings button click
 	d3.select('#settings_set')
		.on('click', function() {
			settingsChange();
    });

	// Defines behavior on settings button click
	d3.select('#freq_reset')
		.on('click', function() {
			document.getElementById("min_freq").value="";
			document.getElementById("max_freq").value="";
			document.getElementById("freq_context").value="";
	});

	return mod;

})(FFTViz || {}, Commons, document, window);

window.addEventListener("DOMContentLoaded", function() {
    FFTViz.initView();
});
