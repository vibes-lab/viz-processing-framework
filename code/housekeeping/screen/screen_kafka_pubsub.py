import os
import sys

if __name__ == '__main__':
    os.system('killall screen')
    n1=input("Number of script instances: ")
    
    for i in range(n1):
        os.system("screen -d -S fft_sec -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python psd_fft.py;'")
        os.system("screen -d -S consumer_raw -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python consumer_raw.py;'")
        os.system("screen -d -S hdfs_to_kafka -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python put_file_in_kafka.py;'")


    os.system("screen -d -S file_sync -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python file_sync_stable.py;'")
    os.system("screen -d -S fft_hour -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python fft_by_hour.py;'")
    os.system("screen -d -S fft_day -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python fft_by_day.py;'")
