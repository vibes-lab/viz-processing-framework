import os
import sys

if __name__ == '__main__':
    os.system('killall screen')
    n1=input("Number of script instances: ")
    
    for i in range(n1):
        os.system("screen -d -S consumer -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python consumer_sim.py;'")
        os.system("screen -d -S kafka_sim -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python kafka_sim.py;'")

    os.system("screen -d -S file_sync -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python file_sync_sim.py;'")
