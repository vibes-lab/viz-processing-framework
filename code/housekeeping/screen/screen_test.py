import os
import sys
import time

if __name__ == '__main__':
    os.system('killall screen')
    os.system('sudo rm /home/vtsil/vtsil-data-viz-api/datavizapi/scripts/process_times.csv')
    os.system("screen -d -S lr -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python test_lastread.py;'")
    n1=input("Number of script instances: ")

    for i in range(n1):
        os.system("screen -d -S fft -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python test_psd_fft.py;'")
        os.system("screen -d -S raw -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python test_raw.py;'")
        os.system("screen -d -S kafka -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python test_kafka.py;'")

    os.system("screen -d -S write -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python test_write.py;'")
os.system("screen -d -S file -m bash -c 'cd /home/vtsil/vtsil-data-viz-api/datavizapi/scripts; workon flask-app; python test_file_sync.py;'")
