#!/bin/bash
sudo mkdir -p /mnt/sdc1/kafka/logs
sudo mkdir -p /mnt/sdb1/cassandra/data
sudo mkdir -p /mnt/sdc1/cassandra/commitlog
sudo mkdir -p /mnt/sdc1/cassandra/hints
sudo mkdir -p /mnt/sda1/hdfs/datanode
sudo mkdir -p /mnt/sdb1/hdfs/namenode
sudo mkdir -p /opt/cassandra
sudo mkdir -p /opt/hadoop/datanode
sudo mkdir -p /opt/hadoop/namenode

sudo chown vtsil:vtsil -R /mnt/sda1/hdfs
sudo chown vtsil:vtsil -R /mnt/sdb1/hdfs
sudo chown vtsil:vtsil -R /opt/hadoop/datanode
sudo chown vtsil:vtsil -R /opt/hadoop/namenode
sudo chown cassandra:cassandra -R /mnt/sdc1/cassandra
sudo chown cassandra:cassandra -R /mnt/sdb1/cassandra
sudo chown cassandra:cassandra -R /opt/cassandra
sudo chown vtsil:vtsil -R /mnt/sdc1/kafka

sudo ln -sf /mnt/sda1/hdfs/datanode /opt/hadoop/
sudo ln -sf /mnt/sdb1/hdfs/namenode /opt/hadoop/

sudo ln -sf /mnt/sdc1/kafka/logs /opt/kafka/data

sudo ln -sf /mnt/sdb1/cassandra/data /opt/cassandra/data
sudo ln -sf /mnt/sdc1/cassandra/commitlog /opt/cassandra/commitlog
sudo ln -sf /mnt/sdc1/cassandra/hints /opt/cassandra/hints

