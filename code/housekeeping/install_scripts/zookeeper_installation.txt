mkdir /opt/zookeeper
sudo chown vtsil:vtsil -R /opt/zookeeper
mkdir /opt/zookeeper/data
mkdir /opt/zookeeper/logs
mkdir /var/log/zookeeper
sudo chown vtsil:vtsil -R /var/log/zookeeper
mkdir /opt/zookeeper/data/myid
echo "1">>/opt/zookeeper/data/myid #use "2" for zoo2 and "3" for zoo3
# add following lines in /etc/hosts
10.0.1.12 zoo1
10.0.1.13 zoo2
10.0.1.14 zoo3

cd /opt/zookeeper
wget https://www-us.apache.org/dist/zookeeper/stable/zookeeper-3.4.12.tar.gz
tar -zxf zookeeper-3.4.12.tar.gz -C ./

# add following lines in conf/zoo.cfg
tickTime=2000
initLimit=10
syncLimit=5
dataDir=/opt/zookeeper/data
dataLogDir=/opt/zookeeper/logs
clientPort=2181

server.1=zoo1:2888:3888
server.2=zoo2:2888:3888
server.3=zoo3:2888:3888

# to run zookeeper using systemd, follow these ste[s
sudo vi /etc/systemd/system/zookeeper.service
[Unit]
Description=Zookeeper Daemon
Documentation=http://zookeeper.apache.org
Requires=network.target
After=network.target

[Service]    
Type=forking
WorkingDirectory=/opt/zookeeper
User=vtsil
Group=vtsil
ExecStart=/opt/zookeeper/bin/zkServer.sh start /opt/zookeeper/conf/zoo.cfg
ExecStop=/opt/zookeeper/bin/zkServer.sh stop /opt/zookeeper/conf/zoo.cfg
ExecReload=/opt/zookeeper/bin/zkServer.sh restart /opt/zookeeper/conf/zoo.cfg
TimeoutSec=30
Restart=on-failure

[Install]
WantedBy=default.target

sudo systemctl enable zookeeper # so that zookeeper start at system restart

# to run zookeeper
sudo systemctl start zookeeper
sudo systemctl stop zookeeper
sudo systemctl status zookeeper
